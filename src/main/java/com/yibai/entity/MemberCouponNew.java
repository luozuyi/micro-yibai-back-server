package com.yibai.entity;

import java.math.BigDecimal;
import java.util.Date;

public class MemberCouponNew {
    /**
     * 主键id
     * */
    private Long id;

    /**
     * 优惠卷规则id
     * */
    private Long activityCouponId;

    /**
     * 会员名
     * */
    private String username;

    /**
     * 领取时间
     * */
    private Date createTime;

    /**
     * 生效时间
     * */
    private Date startTime;

    /**
     * 截至时间
     * */
    private Date endTime;

    /**
     * 优惠劵价值
     * */
    private BigDecimal couponValue;

    /**
     * 优惠券最低抵扣
     * */
    private BigDecimal couponLeastPrice;

    /**
     * 优惠券类型0：满减1：直接抵扣
     * */
    private String type;

    /**
     * 可使用平台状态: 0：全平台1：淘宝2: 天猫3: 其他
     * */
    private String categoryType;

    /**
     * 0:全类目13 服饰鞋包14 美容护理15 母婴用品16 3C数码类17 运动/户外18 家装家饰19 家居用品20 食品/保健21 珠宝/首饰22 游戏/话费23 生活服务24 汽车配件25 书籍音像26 玩乐/收藏27 万用百搭28 其他行业29 医药健康30 大家电
     * */
    private String shopType;

    /**
     * 优惠券状态0：待使用1：已使用2：已删除3：已过期4：冻结5：违约扣除
     * */
    private String status;

    /**
     * 是否可以叠加0：可以1：不可以
     * */
    private String isCanAdd;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getActivityCouponId() {
        return activityCouponId;
    }

    public void setActivityCouponId(Long activityCouponId) {
        this.activityCouponId = activityCouponId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public BigDecimal getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(BigDecimal couponValue) {
        this.couponValue = couponValue;
    }

    public BigDecimal getCouponLeastPrice() {
        return couponLeastPrice;
    }

    public void setCouponLeastPrice(BigDecimal couponLeastPrice) {
        this.couponLeastPrice = couponLeastPrice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType == null ? null : categoryType.trim();
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType == null ? null : shopType.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getIsCanAdd() {
        return isCanAdd;
    }

    public void setIsCanAdd(String isCanAdd) {
        this.isCanAdd = isCanAdd == null ? null : isCanAdd.trim();
    }
}