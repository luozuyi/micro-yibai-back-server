package com.yibai.entity;

import java.util.Date;

public class Question {
    /*提问建议ID*/
    private Long questionId;
    /*站点ID*/
    private Long websiteId;
    /*用户ID*/
    private Long memberId;
    /*商城类型ID*/
    private Long categoryId;
    /*类型交易状态：0.交易前    1.交易中   2.交易后   3.其他*/
    private Long categoryType;
    /*新建时间*/
    private Date startdate;
    /*标题*/
    private String title;
    /*提问正文*/
    private String startcontent;
    /*回答正文*/
    private String endcontent;
    /*回复时间*/
    private Date enddate;
    /*回答者ID*/
    private Long adminId;
    /*提问类型：0.提问 1.建议 2.评价  3.投诉*/
    private Long questionType;

    private Long browse;
    /*投诉或评价 内容*/
    private String assessContent;
    /*投诉或评价 业务员ID*/
    private Long assessAdminId;
    /*是否匿名， 0为非匿名， 1为匿名*/
    private Long assessDisplay;
    /*评价等级  1满意， 2一般，3不满意*/
    private Long assessRank;

    private String assessReply;

    private Integer emark;

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public Long getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(Long websiteId) {
        this.websiteId = websiteId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(Long categoryType) {
        this.categoryType = categoryType;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getStartcontent() {
        return startcontent;
    }

    public void setStartcontent(String startcontent) {
        this.startcontent = startcontent == null ? null : startcontent.trim();
    }

    public String getEndcontent() {
        return endcontent;
    }

    public void setEndcontent(String endcontent) {
        this.endcontent = endcontent == null ? null : endcontent.trim();
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public Long getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Long questionType) {
        this.questionType = questionType;
    }

    public Long getBrowse() {
        return browse;
    }

    public void setBrowse(Long browse) {
        this.browse = browse;
    }

    public String getAssessContent() {
        return assessContent;
    }

    public void setAssessContent(String assessContent) {
        this.assessContent = assessContent == null ? null : assessContent.trim();
    }

    public Long getAssessAdminId() {
        return assessAdminId;
    }

    public void setAssessAdminId(Long assessAdminId) {
        this.assessAdminId = assessAdminId;
    }

    public Long getAssessDisplay() {
        return assessDisplay;
    }

    public void setAssessDisplay(Long assessDisplay) {
        this.assessDisplay = assessDisplay;
    }

    public Long getAssessRank() {
        return assessRank;
    }

    public void setAssessRank(Long assessRank) {
        this.assessRank = assessRank;
    }

    public String getAssessReply() {
        return assessReply;
    }

    public void setAssessReply(String assessReply) {
        this.assessReply = assessReply == null ? null : assessReply.trim();
    }

    public Integer getEmark() {
        return emark;
    }

    public void setEmark(Integer emark) {
        this.emark = emark;
    }
}
