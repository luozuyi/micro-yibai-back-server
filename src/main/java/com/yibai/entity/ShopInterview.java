package com.yibai.entity;

import java.util.Date;

public class ShopInterview {
    /*主键ID*/
    private Long id;
    /*标题*/
    private String title;
    /*售价*/
    private Double price;
    /*等级*/
    private Integer rank;
    /*店铺名称*/
    private String producttitle;
    /*类型*/
    private String tag;
    /*好评率*/
    private Double praise;
    /*采访时间*/
    private Date date;
    /*标题图*/
    private String titleImg;
    /*信用等级*/
    private Integer prop2;
    /*详细信用等级*/
    private Integer prop3;
    /*摘要*/
    private String description;
    /*正文*/
    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getProducttitle() {
        return producttitle;
    }

    public void setProducttitle(String producttitle) {
        this.producttitle = producttitle == null ? null : producttitle.trim();
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag == null ? null : tag.trim();
    }

    public Double getPraise() {
        return praise;
    }

    public void setPraise(Double praise) {
        this.praise = praise;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitleImg() {
        return titleImg;
    }

    public void setTitleImg(String titleImg) {
        this.titleImg = titleImg == null ? null : titleImg.trim();
    }

    public Integer getProp2() {
        return prop2;
    }

    public void setProp2(Integer prop2) {
        this.prop2 = prop2;
    }

    public Integer getProp3() {
        return prop3;
    }

    public void setProp3(Integer prop3) {
        this.prop3 = prop3;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}