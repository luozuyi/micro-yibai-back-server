package com.yibai.entity;

import java.io.Serializable;
import java.util.Date;

public class HeadActive implements Serializable{
    private Integer id;

    private String idfa;

    private String imei;
   /*
   时间
    */
    private Date timestamp;
    /*
    转化来源
     */
    private String istoutiao;
   /*
   注册会员名
    */
    private String username;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdfa() {
        return idfa;
    }

    public void setIdfa(String idfa) {
        this.idfa = idfa == null ? null : idfa.trim();
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei == null ? null : imei.trim();
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getIstoutiao() {
        return istoutiao;
    }

    public void setIstoutiao(String istoutiao) {
        this.istoutiao = istoutiao == null ? null : istoutiao.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }
}