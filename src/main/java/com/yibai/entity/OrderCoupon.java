package com.yibai.entity;

public class OrderCoupon {
    /**
     * 主键id
     * */
    private Long id;

    /**
     * 优惠券ID
     * */
    private Long couponId;

    /**
     * 订单ID
     * */
    private Long orderId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }
}