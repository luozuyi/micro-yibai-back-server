package com.yibai.entity;

import java.io.Serializable;

public class SalesAdmin implements Serializable{
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private Long id;
    /**
     * 销售级别
     */
    private Integer level;
    /**
     *销售管理会员数量
     */
    private Integer num;
    /**
     * 级别对应的最大数量
     */
    private Integer levelnum;
    /**
     * 当天数量
     */
    private Integer todaynum;
    /**
     * 昨天数量
     */
    private Integer yesterdaynum;
    /**
     * 总数量
     */
    private Integer totalnum;
    /**
     * 今天备注数
     */
    private Integer todaynote;
    /**
     * 昨天备注数
     */
    private Integer yesterdaynote;
    /**
     * 总备注数
     */
    private Integer totalnote;
    /**
     * 限制数量
     */
    private Integer limitnum;
    /**
     * 组长ID
     */
    private Integer leader;
    /**
     * 当天查看数
     */
    private Integer dcheck;
    /**
     * 本周查看数
     */
    private Integer wcheck;
    /**
     * 当月查看数
     */
    private Integer mcheck;
    /**
     * 今日业绩
     */
    private Integer dachievement;
    /**
     * 本周业绩
     */
    private Integer wachievement;
    /**
     * 本月业绩
     */
    private Integer machievement;
    /**
     * 查看数量
     */
    private Integer checknum;
    /**
     * 总业绩
     */
    private Integer achievement;
    /**
     * 当天会员数
     */
    private Integer dmember;
    /**
     * 本周会员数
     */
    private Integer wmember;
    /**
     * 本月会员数
     */
    private Integer mmember;
    /**
     * 总会员数
     */
    private Integer amember;
    /**
     * 本人备注数
     */
    private Integer dnote;
    /**
     * 本周备注数
     */
    private Integer wnote;
    /**
     * 本月备注数
     */
    private Integer mnote;
    /**
     * 总备注数
     */
    private Integer anote;
    /**
     * 是否是销售
     */
    private Integer issale;
    /**
     * 月目标
     */
    private Integer mgoal;
    /**
     * 部长ID
     */
    private Integer minister;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getLevelnum() {
        return levelnum;
    }

    public void setLevelnum(Integer levelnum) {
        this.levelnum = levelnum;
    }

    public Integer getTodaynum() {
        return todaynum;
    }

    public void setTodaynum(Integer todaynum) {
        this.todaynum = todaynum;
    }

    public Integer getYesterdaynum() {
        return yesterdaynum;
    }

    public void setYesterdaynum(Integer yesterdaynum) {
        this.yesterdaynum = yesterdaynum;
    }

    public Integer getTotalnum() {
        return totalnum;
    }

    public void setTotalnum(Integer totalnum) {
        this.totalnum = totalnum;
    }

    public Integer getTodaynote() {
        return todaynote;
    }

    public void setTodaynote(Integer todaynote) {
        this.todaynote = todaynote;
    }

    public Integer getYesterdaynote() {
        return yesterdaynote;
    }

    public void setYesterdaynote(Integer yesterdaynote) {
        this.yesterdaynote = yesterdaynote;
    }

    public Integer getTotalnote() {
        return totalnote;
    }

    public void setTotalnote(Integer totalnote) {
        this.totalnote = totalnote;
    }

    public Integer getLimitnum() {
        return limitnum;
    }

    public void setLimitnum(Integer limitnum) {
        this.limitnum = limitnum;
    }

    public Integer getLeader() {
        return leader;
    }

    public void setLeader(Integer leader) {
        this.leader = leader;
    }

    public Integer getDcheck() {
        return dcheck;
    }

    public void setDcheck(Integer dcheck) {
        this.dcheck = dcheck;
    }

    public Integer getWcheck() {
        return wcheck;
    }

    public void setWcheck(Integer wcheck) {
        this.wcheck = wcheck;
    }

    public Integer getMcheck() {
        return mcheck;
    }

    public void setMcheck(Integer mcheck) {
        this.mcheck = mcheck;
    }

    public Integer getDachievement() {
        return dachievement;
    }

    public void setDachievement(Integer dachievement) {
        this.dachievement = dachievement;
    }

    public Integer getWachievement() {
        return wachievement;
    }

    public void setWachievement(Integer wachievement) {
        this.wachievement = wachievement;
    }

    public Integer getMachievement() {
        return machievement;
    }

    public void setMachievement(Integer machievement) {
        this.machievement = machievement;
    }

    public Integer getChecknum() {
        return checknum;
    }

    public void setChecknum(Integer checknum) {
        this.checknum = checknum;
    }

    public Integer getAchievement() {
        return achievement;
    }

    public void setAchievement(Integer achievement) {
        this.achievement = achievement;
    }

    public Integer getDmember() {
        return dmember;
    }

    public void setDmember(Integer dmember) {
        this.dmember = dmember;
    }

    public Integer getWmember() {
        return wmember;
    }

    public void setWmember(Integer wmember) {
        this.wmember = wmember;
    }

    public Integer getMmember() {
        return mmember;
    }

    public void setMmember(Integer mmember) {
        this.mmember = mmember;
    }

    public Integer getAmember() {
        return amember;
    }

    public void setAmember(Integer amember) {
        this.amember = amember;
    }

    public Integer getDnote() {
        return dnote;
    }

    public void setDnote(Integer dnote) {
        this.dnote = dnote;
    }

    public Integer getWnote() {
        return wnote;
    }

    public void setWnote(Integer wnote) {
        this.wnote = wnote;
    }

    public Integer getMnote() {
        return mnote;
    }

    public void setMnote(Integer mnote) {
        this.mnote = mnote;
    }

    public Integer getAnote() {
        return anote;
    }

    public void setAnote(Integer anote) {
        this.anote = anote;
    }

    public Integer getIssale() {
        return issale;
    }

    public void setIssale(Integer issale) {
        this.issale = issale;
    }

    public Integer getMgoal() {
        return mgoal;
    }

    public void setMgoal(Integer mgoal) {
        this.mgoal = mgoal;
    }

    public Integer getMinister() {
        return minister;
    }

    public void setMinister(Integer minister) {
        this.minister = minister;
    }
}