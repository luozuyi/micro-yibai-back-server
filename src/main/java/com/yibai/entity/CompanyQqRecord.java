package com.yibai.entity;

import java.io.Serializable;
import java.util.Date;

public class CompanyQqRecord implements Serializable{
    /*
    主键id
     */
    private Long id;
    /*
    IP地址
     */
    private String ip;
   /*
   会员名
    */
    private String membername;
    /*
    销售名
     */
    private String name;
    /*
    时间
     */
    private Date time;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername == null ? null : membername.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}