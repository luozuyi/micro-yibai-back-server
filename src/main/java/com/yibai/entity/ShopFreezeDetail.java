package com.yibai.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 冻结明细实体类
 */
public class ShopFreezeDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private Long id;

    /**
     * 会员id
     */
    private Long memberId;

    /**
     * 发生日期
     */
    private String createTime;

    /**
     * 类型 0押金 1提现申请
     */
    private Integer freeType;

    /**
     * 冻结金额
     */
    private Double freeMoney;

    /**
     * 状态 0冻结中 1已解冻
     */
    private Integer status;

    /**
     * 提现时写入提现明细编号
     */
    private Long withdrawId;

    /**
     * 订单支付和竞拍时写入订单编号
     */
    private Long orderId;

    /**
     * 交纳保证金时写入商品编号
     */
    private Long productId;

    /**
     *
     */
    private Date repaymentDate;

    /**
     * 审核人id
     */
    private Long approveAdminId;

    /**
     * 审核时间
     */
    private Date approveTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public Integer getFreeType() {
        return freeType;
    }

    public void setFreeType(Integer freeType) {
        this.freeType = freeType;
    }

    public Double getFreeMoney() {
        return freeMoney;
    }

    public void setFreeMoney(Double freeMoney) {
        this.freeMoney = freeMoney;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getWithdrawId() {
        return withdrawId;
    }

    public void setWithdrawId(Long withdrawId) {
        this.withdrawId = withdrawId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Date getRepaymentDate() {
        return repaymentDate;
    }

    public void setRepaymentDate(Date repaymentDate) {
        this.repaymentDate = repaymentDate;
    }

    public Long getApproveAdminId() {
        return approveAdminId;
    }

    public void setApproveAdminId(Long approveAdminId) {
        this.approveAdminId = approveAdminId;
    }

    public Date getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(Date approveTime) {
        this.approveTime = approveTime;
    }
}