package com.yibai.entity;

import java.io.Serializable;
import java.util.Date;

public class ShopBargain implements Serializable{
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private Integer id;
    /**
     * 砍价电话
     */
    private String bargainTel;
    /**
     * 提交价格
     */
    private Double submitPrice;
    /**
     * 提交时间
     */
    private Date submitTime;
    /**
     * 店铺id
     */
    private Long productId;
    /**
     * 会员id
     */
    private Integer memberId;
    /**
     * 所属销售
     */
    private String besaleman;
    /**
     * 评论
     */
    private String remarks;
    /**
     * 评论时间
     */
    private Date remarkstime;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 抢单时间
     */
    private Date grabtime;
    /**
     * 会员名
     */
    private String memberName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBargainTel() {
        return bargainTel;
    }

    public void setBargainTel(String bargainTel) {
        this.bargainTel = bargainTel == null ? null : bargainTel.trim();
    }

    public Double getSubmitPrice() {
        return submitPrice;
    }

    public void setSubmitPrice(Double submitPrice) {
        this.submitPrice = submitPrice;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getBesaleman() {
        return besaleman;
    }

    public void setBesaleman(String besaleman) {
        this.besaleman = besaleman == null ? null : besaleman.trim();
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public Date getRemarkstime() {
        return remarkstime;
    }

    public void setRemarkstime(Date remarkstime) {
        this.remarkstime = remarkstime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getGrabtime() {
        return grabtime;
    }

    public void setGrabtime(Date grabtime) {
        this.grabtime = grabtime;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName == null ? null : memberName.trim();
    }
}