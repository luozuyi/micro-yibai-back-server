package com.yibai.entity;

import java.io.Serializable;
import java.util.Date;

public class UserUnsuccessreg implements Serializable{
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private Integer id;
    /**
     * 电话
     */
    private String tel;
    /**
     * 0还未注册会员1已经注册会员
     */
    private Integer sign;
    /**
     * 统计时间
     */
    private Date time;
    /**
     * 备注
     */
    private String note;
    /**
     * 销售名字
     */
    private String salesname;
    /**
     * 销售操作时间
     */
    private Date optime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    public Integer getSign() {
        return sign;
    }

    public void setSign(Integer sign) {
        this.sign = sign;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public String getSalesname() {
        return salesname;
    }

    public void setSalesname(String salesname) {
        this.salesname = salesname == null ? null : salesname.trim();
    }

    public Date getOptime() {
        return optime;
    }

    public void setOptime(Date optime) {
        this.optime = optime;
    }
}