package com.yibai.entity;

import java.io.Serializable;
import java.util.Date;

public class Artificial implements Serializable{

    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private Long artificialId;
    /**
     *会员id
     */
    private Long memberId;
    /**
     * 商城类型：旗舰店，专营店，专卖店
     */
    private String tmshoptype;
    /**
     * 店铺类型：淘宝，天猫，京东，1号店等
     */
    private String shopcategory;
    /**
     * 商标类型:tm,r,
     */
    private String sbtype;
    /**
     * 行业
     */
    private String industry;
    /**
     * 最小价格
     */
    private Integer minprice;
    /**
     * 最大价格
     */
    private Integer maxprice;
    /**
     * 省
     */
    private String province;
    /**
     * 2级联动： 市
     */
    private Integer city;
    /**
     * 信用等级
     */
    private Integer prop2;
    /**
     * 等级子类
     */
    private Integer prop3;
    /**
     * praise
     */
    private Integer praise;
    /**
     * 创建时间
     */
    private Date createtime;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 所属销售员
     */
    private String besaleman;
    /**
     * 业务员抢单时间
     */
    private Date grabtime;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 备注时间
     */
    private Date remarkstime;
    /**
     * 是否达成成交
     */
    private Integer isDeal;
    /**
     * 成交店铺名或链接
     */
    private String dealShopUrl;
    /**
     * 电话
     */
    private String memberTel;
    /**
     * 会员qq
     */
    private String memberQq;
    /**
     * 用户描述
     */
    private String userDescription;

    public Long getArtificialId() {
        return artificialId;
    }

    public void setArtificialId(Long artificialId) {
        this.artificialId = artificialId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getTmshoptype() {
        return tmshoptype;
    }

    public void setTmshoptype(String tmshoptype) {
        this.tmshoptype = tmshoptype == null ? null : tmshoptype.trim();
    }

    public String getShopcategory() {
        return shopcategory;
    }

    public void setShopcategory(String shopcategory) {
        this.shopcategory = shopcategory == null ? null : shopcategory.trim();
    }

    public String getSbtype() {
        return sbtype;
    }

    public void setSbtype(String sbtype) {
        this.sbtype = sbtype == null ? null : sbtype.trim();
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry == null ? null : industry.trim();
    }

    public Integer getMinprice() {
        return minprice;
    }

    public void setMinprice(Integer minprice) {
        this.minprice = minprice;
    }

    public Integer getMaxprice() {
        return maxprice;
    }

    public void setMaxprice(Integer maxprice) {
        this.maxprice = maxprice;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Integer getProp2() {
        return prop2;
    }

    public void setProp2(Integer prop2) {
        this.prop2 = prop2;
    }

    public Integer getProp3() {
        return prop3;
    }

    public void setProp3(Integer prop3) {
        this.prop3 = prop3;
    }

    public Integer getPraise() {
        return praise;
    }

    public void setPraise(Integer praise) {
        this.praise = praise;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getBesaleman() {
        return besaleman;
    }

    public void setBesaleman(String besaleman) {
        this.besaleman = besaleman == null ? null : besaleman.trim();
    }

    public Date getGrabtime() {
        return grabtime;
    }

    public void setGrabtime(Date grabtime) {
        this.grabtime = grabtime;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public Date getRemarkstime() {
        return remarkstime;
    }

    public void setRemarkstime(Date remarkstime) {
        this.remarkstime = remarkstime;
    }

    public Integer getIsDeal() {
        return isDeal;
    }

    public void setIsDeal(Integer isDeal) {
        this.isDeal = isDeal;
    }

    public String getDealShopUrl() {
        return dealShopUrl;
    }

    public void setDealShopUrl(String dealShopUrl) {
        this.dealShopUrl = dealShopUrl == null ? null : dealShopUrl.trim();
    }

    public String getMemberTel() {
        return memberTel;
    }

    public void setMemberTel(String memberTel) {
        this.memberTel = memberTel == null ? null : memberTel.trim();
    }

    public String getMemberQq() {
        return memberQq;
    }

    public void setMemberQq(String memberQq) {
        this.memberQq = memberQq == null ? null : memberQq.trim();
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription == null ? null : userDescription.trim();
    }
}