package com.yibai.entity;

import java.io.Serializable;
import java.util.Date;

public class Business implements Serializable{
    /*
   主键id
     */
    private Long id;
  /*
  时间
   */
    private Date time;
     /*
     联系人
      */
    private String companyConstacts;
   /*
   联系电话
    */
    private String companyTel;
   /*
    联系人邮箱
    */
    private String email;
   /*
   联系人qq
    */
    private String qq;
  /*
  公司名称
   */
    private String companyName;
   /*
   公司网站
    */
    private String companyWebsite;

    private String cooperationContents;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getCompanyConstacts() {
        return companyConstacts;
    }

    public void setCompanyConstacts(String companyConstacts) {
        this.companyConstacts = companyConstacts == null ? null : companyConstacts.trim();
    }

    public String getCompanyTel() {
        return companyTel;
    }

    public void setCompanyTel(String companyTel) {
        this.companyTel = companyTel == null ? null : companyTel.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite == null ? null : companyWebsite.trim();
    }

    public String getCooperationContents() {
        return cooperationContents;
    }

    public void setCooperationContents(String cooperationContents) {
        this.cooperationContents = cooperationContents == null ? null : cooperationContents.trim();
    }
}