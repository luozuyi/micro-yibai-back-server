package com.yibai.entity;

import java.io.Serializable;

public class ShopAdmin implements Serializable{
    /**
     * 管理员id
     */
    private Long adminId;
    /**
     * 站点id
     */
    private Long websiteId;
    /**
     * 名
     */
    private String firstname;
    /**
     * 姓
     */
    private String lastname;
    /**
     * 销售id
     */
    private Long salesId;

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public Long getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(Long websiteId) {
        this.websiteId = websiteId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname == null ? null : firstname.trim();
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname == null ? null : lastname.trim();
    }

    public Long getSalesId() {
        return salesId;
    }

    public void setSalesId(Long salesId) {
        this.salesId = salesId;
    }
}