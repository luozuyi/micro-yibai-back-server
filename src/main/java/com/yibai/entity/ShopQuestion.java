package com.yibai.entity;

import java.util.Date;

public class ShopQuestion {
    /*主键id*/
    private Integer id;
    /*提问或者建议的用户id*/
    private Integer userid;
    /*业务代表id*/
    private Integer businesid;
    /*业务代表名字*/
    private String username;
    /*提问标题*/
    private String title;
    /*提问描述或者建议内容*/
    private String description;
    /*回答状态:  0:正在处理    1:已回答*/
    private Integer style;
    /*提问类型*/
    private String type;
    /*提问浏览量*/
    private Integer num;
    /*提问或者建议时间*/
    private Date createdate;
    /*回答时间*/
    private Date answerdate;
    /*回复内容*/
    private String reply;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getBusinesid() {
        return businesid;
    }

    public void setBusinesid(Integer businesid) {
        this.businesid = businesid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Integer getStyle() {
        return style;
    }

    public void setStyle(Integer style) {
        this.style = style;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Date getAnswerdate() {
        return answerdate;
    }

    public void setAnswerdate(Date answerdate) {
        this.answerdate = answerdate;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply == null ? null : reply.trim();
    }
}
