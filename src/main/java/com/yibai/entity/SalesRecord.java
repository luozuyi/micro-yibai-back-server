package com.yibai.entity;

import java.io.Serializable;
import java.util.Date;

public class SalesRecord implements Serializable{
    /**
     * 主键id
     */
    private Long id;
    /**
     * 公共会员池id
     */
    private Long memberId;
    /**
     *adminid
     */
    private Long admin;
    /**
     * 迁入私人池时间
     */
    private Date impactTime;
    /**
     * 迁出私人池时间
     */
    private Date ingoingTime;
    /**
     * 最近增加备注或者推荐店铺时间
     */
    private Date updateTime;
    /**
     * 最终结果
     */
    private String result;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getAdmin() {
        return admin;
    }

    public void setAdmin(Long admin) {
        this.admin = admin;
    }

    public Date getImpactTime() {
        return impactTime;
    }

    public void setImpactTime(Date impactTime) {
        this.impactTime = impactTime;
    }

    public Date getIngoingTime() {
        return ingoingTime;
    }

    public void setIngoingTime(Date ingoingTime) {
        this.ingoingTime = ingoingTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result == null ? null : result.trim();
    }
}