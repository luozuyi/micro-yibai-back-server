package com.yibai.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 收支明细实体类
 */
public class ShopIncome implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private Long id;

    /**
     * 类型
     * 0:支付宝充值 1：线下汇款 2：网银充值 3：购买看店卡 4:购店支付 5：卖家支付保证金  6：买家担保资金 7：卖店 8:卖店服务费 9:买店服务费 10:买家担保服务费 11:提现服务费 12：提现  13:后台调整 14.违约 15:重申担保资金 16：促销
     */
    private Integer flunFlow;

    /**
     * 资金类型
     * 0:代金券 1：钱2：优惠劵
     */
    private Integer moneyType;

    /**
     * 收支类型
     * 0:收入 1:支出
     */
    private Integer financeType;

    /**
     * 金额
     */
    private BigDecimal num;

    /**
     * 产生时间
     */
    private Date cTime;

    /**
     * 会员id
     */
    private Long memberId;

    /**
     * 会员名
     */
    private String memberName;

    /**
     * 会员电话
     */
    private String memberPhone;

    /**
     * 查询操作时间起始值
     */
    private String startCTime;

    /**
     * 查询操作时间终止值
     */
    private String endCTime;

    /**
     * 查询起始金额输入
     */
    private BigDecimal startNum;

    /**
     * 查询终止金额输入
     */
    private BigDecimal endNum;


    public String getStartCTime() {
        return startCTime;
    }

    public void setStartCTime(String startCTime) {
        this.startCTime = startCTime;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public BigDecimal getStartNum() {
        return startNum;
    }

    public void setStartNum(BigDecimal startNum) {
        this.startNum = startNum;
    }

    public BigDecimal getEndNum() {
        return endNum;
    }

    public void setEndNum(BigDecimal endNum) {
        this.endNum = endNum;
    }


    public String getEndCTime() {
        return endCTime;
    }

    public void setEndCTime(String endCTime) {
        this.endCTime = endCTime;
    }

    public ShopIncome() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFlunFlow() {
        return flunFlow;
    }

    public void setFlunFlow(Integer flunFlow) {
        this.flunFlow = flunFlow;
    }

    public Integer getMoneyType() {
        return moneyType;
    }

    public void setMoneyType(Integer moneyType) {
        this.moneyType = moneyType;
    }

    public Integer getFinanceType() {
        return financeType;
    }

    public void setFinanceType(Integer financeType) {
        this.financeType = financeType;
    }


    public Date getcTime() {
        return cTime;
    }

    public void setcTime(Date cTime) {
        this.cTime = cTime;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberPhone() {
        return memberPhone;
    }

    public void setMemberPhone(String memberPhone) {
        this.memberPhone = memberPhone;
    }
}