package com.yibai.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 提现实体类
 */
public class ShopWithdraw implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private Long id;

    /**
     * 会员id
     */
    private Long memberId;

    /**
     * 银行id
     */
    private Long myBankId;

    /**
     * 流水号
     */
    private String serialNumber;

    /**
     * 申请时间
     */
    private String createTime;

    /**
     * 提现金额
     */
    private Double drawMoney;

    /**
     * 实际金额
     */
    private Double realMoney;

    /**
     * 状态
     */
    private Integer status;

    /**
     *备注
     */
    private String note;

    /**
     * 手续费
     */
    private Double feeMoney;

    /**
     *
     */
    private Integer eMark;

    /**
     * 审核人
     */
    private String approveMan;

    /**
     * 是否免除手续费1：代表已经操作
     */
    private String isFreeFee;

    /**
     * 会员名
     */
    private String memberName;

    /**
     * 会员电话
     */
    private String memberPhone;

    /**
     * 银行名称
     */
    private String bankName;

    /**
     * 银行卡号
     */
    private String cardAccount;

    /**
     * 详细支行
     */
    private String bankDetailName;

    /**
     * 开户名
     */
    private String realName;

    /**
     * 操作信息
     */
    private String[] operation;

    /**
     * 查询起始金额输入
     */
    private BigDecimal startNum;

    /**
     * 查询终止金额输入
     */
    private BigDecimal endNum;

    public String[] getOperation() {
        return operation;
    }

    public void setOperation(String[] operation) {
        this.operation = operation;
    }

    public Integer geteMark() {
        return eMark;
    }

    public void seteMark(Integer eMark) {
        this.eMark = eMark;
    }

    public String getApproveMan() {
        return approveMan;
    }

    public void setApproveMan(String approveMan) {
        this.approveMan = approveMan;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberPhone() {
        return memberPhone;
    }

    public void setMemberPhone(String memberPhone) {
        this.memberPhone = memberPhone;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCardAccount() {
        return cardAccount;
    }

    public void setCardAccount(String cardAccount) {
        this.cardAccount = cardAccount;
    }

    public String getBankDetailName() {
        return bankDetailName;
    }

    public void setBankDetailName(String bankDetailName) {
        this.bankDetailName = bankDetailName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public BigDecimal getStartNum() {
        return startNum;
    }

    public void setStartNum(BigDecimal startNum) {
        this.startNum = startNum;
    }

    public BigDecimal getEndNum() {
        return endNum;
    }

    public void setEndNum(BigDecimal endNum) {
        this.endNum = endNum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getMyBankId() {
        return myBankId;
    }

    public void setMyBankId(Long myBankId) {
        this.myBankId = myBankId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber == null ? null : serialNumber.trim();
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public Double getDrawMoney() {
        return drawMoney;
    }

    public void setDrawMoney(Double drawMoney) {
        this.drawMoney = drawMoney;
    }

    public Double getRealMoney() {
        return realMoney;
    }

    public void setRealMoney(Double realMoney) {
        this.realMoney = realMoney;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public Double getFeeMoney() {
        return feeMoney;
    }

    public void setFeeMoney(Double feeMoney) {
        this.feeMoney = feeMoney;
    }



    public String getIsFreeFee() {
        return isFreeFee;
    }

    public void setIsFreeFee(String isFreeFee) {
        this.isFreeFee = isFreeFee == null ? null : isFreeFee.trim();
    }
}