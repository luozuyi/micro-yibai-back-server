package com.yibai.entity;

import java.io.Serializable;

public class Account implements Serializable{
    /*
    主键
     */
    private Integer id;
    /*
    账号
     */
    private String account;
   /*
   账户密码
    */
    private String password;
   /*
   账号所属平台
    */
    private String fromplatform;
   /*
   认证人
    */
    private String accreditationperson;
    /*
    身份证
     */
    private String idcard;
  /*
  注册
   */
    private String tel;
  /*
  邮箱
   */
    private String email;
    /*
    账号作用
     */
    private String accountrole;
   /*
   所属销售
    */
    private String belongsales;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getFromplatform() {
        return fromplatform;
    }

    public void setFromplatform(String fromplatform) {
        this.fromplatform = fromplatform == null ? null : fromplatform.trim();
    }

    public String getAccreditationperson() {
        return accreditationperson;
    }

    public void setAccreditationperson(String accreditationperson) {
        this.accreditationperson = accreditationperson == null ? null : accreditationperson.trim();
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard == null ? null : idcard.trim();
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getAccountrole() {
        return accountrole;
    }

    public void setAccountrole(String accountrole) {
        this.accountrole = accountrole == null ? null : accountrole.trim();
    }

    public String getBelongsales() {
        return belongsales;
    }

    public void setBelongsales(String belongsales) {
        this.belongsales = belongsales == null ? null : belongsales.trim();
    }
}