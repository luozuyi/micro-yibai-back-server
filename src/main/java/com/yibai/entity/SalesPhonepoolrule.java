package com.yibai.entity;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

public class SalesPhonepoolrule implements Serializable{
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private Long id;
    /**
     * 用户类型 0：不区分买卖家 1：只取买家
     */
    @NotNull
    private Integer type;
    /**
     * 时间
     */
    @NotNull(message = "截止时间不能为空")
    private Date time;
    /**
     * 数量
     */
    @NotNull(message = "单次生成电话池数量不能为空")
    private Integer num;
    /**
     * 日期天数
     */
    @NotNull(message = "日期天数不能为空")
    private Integer dateNum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getDateNum() {
        return dateNum;
    }

    public void setDateNum(Integer dateNum) {
        this.dateNum = dateNum;
    }
}