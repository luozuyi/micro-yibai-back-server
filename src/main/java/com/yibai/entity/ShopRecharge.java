package com.yibai.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 网银支付实体类
 */
public class ShopRecharge implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键Id
     */
    private Long id;

    /**
     * 会员id
     */
    private Long memberId;

    /**
     * 银行id
     */
    private Long bankId;

    /**
     * 流水号
     */
    private String serialNumber;

    /**
     * 付款日期
     */
    private String createTime;

    /**
     * 充值金额
     */
    private Double rechargeMoney;

    /**
     * 状态
     * 0成功 1失败 2处理中
     */
    private Integer status;

    /**
     * 充值方式
     * 0-网上银行 1-线下汇款2-支付宝
     */
    private Integer rechargeType;

    /**
     * 商户订单号
     */
    private String orderCode;

    /**
     * 所属销售员
     */
    private String beSalman;

    /**
     *
     */
    private Integer eMark;

    /**
     * 银行名称
     */
    private String bankName;

    /**
     * 会员名称
     */
    private String memberName;

    /**
     * 会员电话
     */
    private String memberPhone;

    /**
     * 查询操作时间起始值
     */
    private String startCTime;

    /**
     * 查询操作时间终止值
     */
    private String endCTime;

    /**
     * 查询起始金额输入
     */
    private BigDecimal startNum;

    /**
     * 查询终止金额输入
     */
    private BigDecimal endNum;


    public String getStartCTime() {
        return startCTime;
    }

    public void setStartCTime(String startCTime) {
        this.startCTime = startCTime;
    }

    public String getEndCTime() {
        return endCTime;
    }

    public void setEndCTime(String endCTime) {
        this.endCTime = endCTime;
    }

    public BigDecimal getStartNum() {
        return startNum;
    }

    public void setStartNum(BigDecimal startNum) {
        this.startNum = startNum;
    }

    public BigDecimal getEndNum() {
        return endNum;
    }

    public void setEndNum(BigDecimal endNum) {
        this.endNum = endNum;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberPhone() {
        return memberPhone;
    }

    public void setMemberPhone(String memberPhone) {
        this.memberPhone = memberPhone;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Double getRechargeMoney() {
        return rechargeMoney;
    }

    public void setRechargeMoney(Double rechargeMoney) {
        this.rechargeMoney = rechargeMoney;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getRechargeType() {
        return rechargeType;
    }

    public void setRechargeType(Integer rechargeType) {
        this.rechargeType = rechargeType;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getBeSalman() {
        return beSalman;
    }

    public void setBeSalman(String beSalman) {
        this.beSalman = beSalman;
    }

    public Integer geteMark() {
        return eMark;
    }

    public void seteMark(Integer eMark) {
        this.eMark = eMark;
    }
}