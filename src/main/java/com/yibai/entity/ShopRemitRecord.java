package com.yibai.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 线下汇款记录实体类
 */
public class ShopRemitRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private Long id;

    /**
     * 会员名称
     */
    private String memberName;

    /**
     * 会员id
     */
    private Long memberId;

    /**
     * 会员电话
     */
    private String memberPhone;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 真实名字
     */
    private String realName;

    /**
     * 收款账号
     */
    private String inAccount;

    /**
     * 汇款银行
     */
    private String remitBank;

    /**
     * 汇款金额
     */
    private Double remitMoney;

    /**
     * 汇款日期
     */
    private String createTime;

    /**
     * 汇款人姓名
     */
    private String remitName;

    /**
     * 汇款凭证URL
     */
    private String voucherPath;

    /**
     * 备注
     */
    private String note;

    /**
     * 充值所属销售员
     */
    private String beSalman;

    /**
     * 	审核人
     */
    private String approveMan;


    /**
     *
     */
    private Integer eMark;

    /**
     *
     */
    private Long rechargeId;

    /**
     * 操作信息
     */
    private String operation;

    /**
     * 查询操作时间起始值
     */
    private String startCTime;

    /**
     * 查询操作时间终止值
     */
    private String endCTime;

    /**
     * 查询起始金额输入
     */
    private BigDecimal startNum;

    /**
     * 查询终止金额输入
     */
    private BigDecimal endNum;


    public ShopRemitRecord() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getStartCTime() {
        return startCTime;
    }

    public void setStartCTime(String startCTime) {
        this.startCTime = startCTime;
    }

    public String getEndCTime() {
        return endCTime;
    }

    public void setEndCTime(String endCTime) {
        this.endCTime = endCTime;
    }

    public BigDecimal getStartNum() {
        return startNum;
    }

    public void setStartNum(BigDecimal startNum) {
        this.startNum = startNum;
    }

    public BigDecimal getEndNum() {
        return endNum;
    }

    public void setEndNum(BigDecimal endNum) {
        this.endNum = endNum;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getInAccount() {
        return inAccount;
    }

    public void setInAccount(String inAccount) {
        this.inAccount = inAccount;
    }

    public String getRemitBank() {
        return remitBank;
    }

    public void setRemitBank(String remitBank) {
        this.remitBank = remitBank;
    }

    public Double getRemitMoney() {
        return remitMoney;
    }

    public void setRemitMoney(Double remitMoney) {
        this.remitMoney = remitMoney;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getRemitName() {
        return remitName;
    }

    public void setRemitName(String remitName) {
        this.remitName = remitName;
    }

    public String getVoucherPath() {
        return voucherPath;
    }

    public void setVoucherPath(String voucherPath) {
        this.voucherPath = voucherPath;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getBeSalman() {
        return beSalman;
    }

    public void setBeSalman(String beSalman) {
        this.beSalman = beSalman;
    }

    public String getApproveMan() {
        return approveMan;
    }

    public void setApproveMan(String approveMan) {
        this.approveMan = approveMan;
    }

    public Integer geteMark() {
        return eMark;
    }

    public void seteMark(Integer eMark) {
        this.eMark = eMark;
    }

    public Long getRechargeId() {
        return rechargeId;
    }

    public void setRechargeId(Long rechargeId) {
        this.rechargeId = rechargeId;
    }

    public String getMemberPhone() {
        return memberPhone;
    }

    public void setMemberPhone(String memberPhone) {
        this.memberPhone = memberPhone;
    }

}
