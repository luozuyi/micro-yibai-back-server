package com.yibai.entity;

public class ShopCategory {
    /*主键ID*/
    private Long categoryId;
    /*站点ID*/
    private Long websiteId;
    /*类型ID*/
    private Long ptypeId;
    /*父类别ID*/
    private Long parentId;
    /*名称*/
    private String name;
    /*访问路径*/
    private String path;
    /*树左边*/
    private Integer lft;
    /*树右边*/
    private Integer rgt;
    /*排列顺序*/
    private Integer priority;
    /*页面关键字*/
    private String keywords;
    /*页面描述*/
    private String description;
    /*栏目页模板*/
    private String tplChannel;
    /*内容页模板*/
    private String tplContent;
    /*图片路径*/
    private String imagePath;
    /*页面标题*/
    private String title;
    /*是否需要尺寸和样式*/
    private Boolean isColorsize;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(Long websiteId) {
        this.websiteId = websiteId;
    }

    public Long getPtypeId() {
        return ptypeId;
    }

    public void setPtypeId(Long ptypeId) {
        this.ptypeId = ptypeId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path == null ? null : path.trim();
    }

    public Integer getLft() {
        return lft;
    }

    public void setLft(Integer lft) {
        this.lft = lft;
    }

    public Integer getRgt() {
        return rgt;
    }

    public void setRgt(Integer rgt) {
        this.rgt = rgt;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getTplChannel() {
        return tplChannel;
    }

    public void setTplChannel(String tplChannel) {
        this.tplChannel = tplChannel == null ? null : tplChannel.trim();
    }

    public String getTplContent() {
        return tplContent;
    }

    public void setTplContent(String tplContent) {
        this.tplContent = tplContent == null ? null : tplContent.trim();
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath == null ? null : imagePath.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Boolean getIsColorsize() {
        return isColorsize;
    }

    public void setIsColorsize(Boolean isColorsize) {
        this.isColorsize = isColorsize;
    }
}
