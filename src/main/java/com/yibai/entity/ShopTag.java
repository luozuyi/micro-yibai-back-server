package com.yibai.entity;

public class ShopTag {
    /*主键id*/
    private Long stagId;
    /*站点ID*/
    private Long websiteId;
    /*名称*/
    private String name;
    /*关联商品个数*/
    private Integer count;

    public Long getStagId() {
        return stagId;
    }

    public void setStagId(Long stagId) {
        this.stagId = stagId;
    }

    public Long getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(Long websiteId) {
        this.websiteId = websiteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}