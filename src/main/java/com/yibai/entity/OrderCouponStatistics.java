package com.yibai.entity;

import java.math.BigDecimal;

public class OrderCouponStatistics {
    /**
     * 主键id
     * */
    private Long id;

    /**
     * 规则id
     * */
    private Long activityCouponId;

    /**
     * 领取优惠券总价值
     * */
    private BigDecimal getCouponValue;

    /**
     * 冻结的优惠券总价
     * */
    private BigDecimal freezedCouponValue;

    /**
     * 使用的优惠券总价
     * */
    private BigDecimal usedCouponValue;

    /**
     * 领取优惠券总数
     * */
    private Integer getCouponCount;

    /**
     * 冻结优惠券总数
     * */
    private Integer freezedCouponCount;

    /**
     * 使用优惠券总数
     * */
    private Integer usedCouponCount;

    /**
     * 优惠券类型0：满减1：直接抵扣
     * */
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getActivityCouponId() {
        return activityCouponId;
    }

    public void setActivityCouponId(Long activityCouponId) {
        this.activityCouponId = activityCouponId;
    }

    public BigDecimal getGetCouponValue() {
        return getCouponValue;
    }

    public void setGetCouponValue(BigDecimal getCouponValue) {
        this.getCouponValue = getCouponValue;
    }

    public BigDecimal getFreezedCouponValue() {
        return freezedCouponValue;
    }

    public void setFreezedCouponValue(BigDecimal freezedCouponValue) {
        this.freezedCouponValue = freezedCouponValue;
    }

    public BigDecimal getUsedCouponValue() {
        return usedCouponValue;
    }

    public void setUsedCouponValue(BigDecimal usedCouponValue) {
        this.usedCouponValue = usedCouponValue;
    }

    public Integer getGetCouponCount() {
        return getCouponCount;
    }

    public void setGetCouponCount(Integer getCouponCount) {
        this.getCouponCount = getCouponCount;
    }

    public Integer getFreezedCouponCount() {
        return freezedCouponCount;
    }

    public void setFreezedCouponCount(Integer freezedCouponCount) {
        this.freezedCouponCount = freezedCouponCount;
    }

    public Integer getUsedCouponCount() {
        return usedCouponCount;
    }

    public void setUsedCouponCount(Integer usedCouponCount) {
        this.usedCouponCount = usedCouponCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }
}