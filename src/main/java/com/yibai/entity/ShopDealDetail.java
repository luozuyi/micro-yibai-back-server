package com.yibai.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 处理详情实体类
 */
public class ShopDealDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private Long id;

    /**
     * 会员id
     */
    private Long memberId;

    /**
     * 流水号
     */
    private String serialNumber;

    /**
     * 发生日期
     */
    private String createTime;

    /**
     * 资金流向 0收入 1支出
     */
    private Integer dealDirect;

    /**
     * 类型 0充值 1支付 2提现 3其他
     */
    private Integer dealType;

    /**
     * 收入金额
     */
    private Double inMoney;

    /**
     * 支出金额
     */
    private Double outMoney;

    /**
     * 用户余额
     */
    private Double memMoney;

    /**
     * 备注
     */
    private String note;

    /**
     * 审核人id
     */
    private Long approveAdminId;

    /**
     * 审核时间
     */
    private Date approveTime;

    /**
     * 会员
     */
    private ShopMember shopMember;



    public ShopMember getShopMember() {
        return shopMember;
    }

    public void setShopMember(ShopMember shopMember) {
        this.shopMember = shopMember;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber == null ? null : serialNumber.trim();
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public Integer getDealDirect() {
        return dealDirect;
    }

    public void setDealDirect(Integer dealDirect) {
        this.dealDirect = dealDirect;
    }

    public Integer getDealType() {
        return dealType;
    }

    public void setDealType(Integer dealType) {
        this.dealType = dealType;
    }

    public Double getInMoney() {
        return inMoney;
    }

    public void setInMoney(Double inMoney) {
        this.inMoney = inMoney;
    }

    public Double getOutMoney() {
        return outMoney;
    }

    public void setOutMoney(Double outMoney) {
        this.outMoney = outMoney;
    }

    public Double getMemMoney() {
        return memMoney;
    }

    public void setMemMoney(Double memMoney) {
        this.memMoney = memMoney;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public Long getApproveAdminId() {
        return approveAdminId;
    }

    public void setApproveAdminId(Long approveAdminId) {
        this.approveAdminId = approveAdminId;
    }

    public Date getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(Date approveTime) {
        this.approveTime = approveTime;
    }
}