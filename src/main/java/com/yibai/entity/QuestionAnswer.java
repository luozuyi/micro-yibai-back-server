package com.yibai.entity;

import java.util.Date;

public class QuestionAnswer {
    /*主键ID*/
    private Long id;
    /*站点ID*/
    private Long webStie;
    /*店铺ID*/
    private Long shopId;
    /*发送时间*/
    private Date startdate;
    /*发送内容*/
    private String content;
    /*回复时间*/
    private Date enddate;
    /*回复内容*/
    private String endcontent;
    /*发送人,普通用户*/
    private Long sendUser;
    /*接收人*/
    private Long receiverUser;
    /*消息状态： 0，卖家未读  1，卖家已读 2，买家未读 3，买家已读*/
    private Integer status;
    /*消息审核状态： 0，买家未审核  1，买家已审核 2，卖家未审核 3，卖家已审核 4，提问审核失败 5，回复审核失败*/
    private Integer approvestatus;
    /*发送人 管理员*/
    private Long admin;
    /*统计点击有用的数量*/
    private Long goodcount;
    /*提问会员所属销售员*/
    private String belongsalman;

    private Integer emark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWebStie() {
        return webStie;
    }

    public void setWebStie(Long webStie) {
        this.webStie = webStie;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getEndcontent() {
        return endcontent;
    }

    public void setEndcontent(String endcontent) {
        this.endcontent = endcontent == null ? null : endcontent.trim();
    }

    public Long getSendUser() {
        return sendUser;
    }

    public void setSendUser(Long sendUser) {
        this.sendUser = sendUser;
    }

    public Long getReceiverUser() {
        return receiverUser;
    }

    public void setReceiverUser(Long receiverUser) {
        this.receiverUser = receiverUser;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getApprovestatus() {
        return approvestatus;
    }

    public void setApprovestatus(Integer approvestatus) {
        this.approvestatus = approvestatus;
    }

    public Long getAdmin() {
        return admin;
    }

    public void setAdmin(Long admin) {
        this.admin = admin;
    }

    public Long getGoodcount() {
        return goodcount;
    }

    public void setGoodcount(Long goodcount) {
        this.goodcount = goodcount;
    }

    public String getBelongsalman() {
        return belongsalman;
    }

    public void setBelongsalman(String belongsalman) {
        this.belongsalman = belongsalman == null ? null : belongsalman.trim();
    }

    public Integer getEmark() {
        return emark;
    }

    public void setEmark(Integer emark) {
        this.emark = emark;
    }
}
