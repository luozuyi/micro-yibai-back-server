package com.yibai.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 代金券变更实体类
 */
public class ShopVoucher implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private Long id;

    /**
     * 代金券变更时间
     */
    private Date voucherCreateTime;

    /**
     * 代金券价格
     */
    private Long voucherPrice;

    /**
     * 站点id
     */
    private Long websiteId;

    /**
     * 会员id
     */
    private Long memberId;

    /**
     * 代金券状态：0，购买看店卡 1，交易支付 2，交易终止返还 3，交易违约扣除  4，管理员调整 5，系统促销赠送
     */
    private Integer useStatus;

    /**
     * 会员名字
     */
    private String memberName;

    /**
     * 查询操作时间起始值
     */
    private String startCTime;

    /**
     * 查询操作时间终止值
     */
    private String endCTime;

    /**
     * 查询起始金额输入
     */
    private BigDecimal startNum;

    /**
     * 查询终止金额输入
     */
    private BigDecimal endNum;

    public String getStartCTime() {
        return startCTime;
    }

    public void setStartCTime(String startCTime) {
        this.startCTime = startCTime;
    }

    public String getEndCTime() {
        return endCTime;
    }

    public void setEndCTime(String endCTime) {
        this.endCTime = endCTime;
    }

    public BigDecimal getStartNum() {
        return startNum;
    }

    public void setStartNum(BigDecimal startNum) {
        this.startNum = startNum;
    }

    public BigDecimal getEndNum() {
        return endNum;
    }

    public void setEndNum(BigDecimal endNum) {
        this.endNum = endNum;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getVoucherCreateTime() {
        return voucherCreateTime;
    }

    public void setVoucherCreateTime(Date voucherCreateTime) {
        this.voucherCreateTime = voucherCreateTime;
    }

    public Long getVoucherPrice() {
        return voucherPrice;
    }

    public void setVoucherPrice(Long voucherPrice) {
        this.voucherPrice = voucherPrice;
    }

    public Long getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(Long websiteId) {
        this.websiteId = websiteId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Integer getUseStatus() {
        return useStatus;
    }

    public void setUseStatus(Integer useStatus) {
        this.useStatus = useStatus;
    }
}