package com.yibai.entity;

import java.io.Serializable;
import java.util.Date;

public class CoreAdmin implements Serializable{
    /**
     * 管理员id
     */
    private Long adminId;
    /**
     * 网站site
     */
    private Long websiteId;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 图片
     */
    private String img;
    /**
     * 退款率
     */
    private Double refundRate;
    /**
     * 已售店铺
     */
    private Long sold;
    /**
     * 待售店铺
     */
    private Long forSale;
    /**
     * 满意次数
     */
    private Long satisfaction;
    /**
     * 当前积分
     */
    private Long integral;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 是否禁用
     */
    private Boolean isDisabled;
    /**
     * 角色id
     */
    private Integer roleId;
    /**
     * 销售头衔
     */
    private String title;
    /**
     * 服务电话
     */
    private String tel;
    /**
     * qq
     */
    private String qq;
    /**
     * 客服手机
     */
    private String phone;

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public Long getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(Long websiteId) {
        this.websiteId = websiteId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img == null ? null : img.trim();
    }

    public Double getRefundRate() {
        return refundRate;
    }

    public void setRefundRate(Double refundRate) {
        this.refundRate = refundRate;
    }

    public Long getSold() {
        return sold;
    }

    public void setSold(Long sold) {
        this.sold = sold;
    }

    public Long getForSale() {
        return forSale;
    }

    public void setForSale(Long forSale) {
        this.forSale = forSale;
    }

    public Long getSatisfaction() {
        return satisfaction;
    }

    public void setSatisfaction(Long satisfaction) {
        this.satisfaction = satisfaction;
    }

    public Long getIntegral() {
        return integral;
    }

    public void setIntegral(Long integral) {
        this.integral = integral;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Boolean getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(Boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }
}