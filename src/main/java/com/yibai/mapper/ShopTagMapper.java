package com.yibai.mapper;

import com.yibai.entity.ShopTag;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface ShopTagMapper {

    List<ShopTag> selectAll();

    int deleteByPrimaryKey(Long stagId);

    int insert(ShopTag record);

    int insertSelective(ShopTag record);

    ShopTag selectByPrimaryKey(Long stagId);

    int updateByPrimaryKeySelective(ShopTag record);

    int updateByPrimaryKey(ShopTag record);
}