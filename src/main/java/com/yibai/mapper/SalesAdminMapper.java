package com.yibai.mapper;

import com.yibai.entity.SalesAdmin;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface SalesAdminMapper extends BaseMapper<SalesAdmin,Long>{
    List<Map<String,Object>> selectLeaderList();

    List<Map<String,Object>> selectMinisterList();

    String selectUsername(Long id);
}