package com.yibai.mapper;

import com.yibai.entity.ShopVoucher;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface ShopVoucherMapper extends BaseMapper<ShopVoucher,Long>{
    int deleteByPrimaryKey(Long id);

    int insert(ShopVoucher record);

    int insertSelective(ShopVoucher record);

    ShopVoucher selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ShopVoucher record);

    int updateByPrimaryKey(ShopVoucher record);

    List<ShopVoucher> selectAllShopVoucher(ShopVoucher shopVoucher);
}