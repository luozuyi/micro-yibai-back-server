package com.yibai.mapper;

import com.yibai.entity.ShopDealDetail;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface ShopDealDetailMapper extends BaseMapper<ShopDealDetail, Long>{
    int deleteByPrimaryKey(Long id);

    int insert(ShopDealDetail record);

    int insertSelective(ShopDealDetail record);

    ShopDealDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ShopDealDetail record);

    int updateByPrimaryKey(ShopDealDetail record);
}