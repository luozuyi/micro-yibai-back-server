package com.yibai.mapper;

import com.yibai.entity.CompanyQqRecord;
import com.yibai.utils.Result;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CompanyQqRecordMapper {
    List<CompanyQqRecord> selectAll(CompanyQqRecord companyQqRecord);
    int deleteByPrimaryKey(Long id);

    int insert(CompanyQqRecord record);

    int insertSelective(CompanyQqRecord record);

    CompanyQqRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CompanyQqRecord record);

    int updateByPrimaryKey(CompanyQqRecord record);
}