package com.yibai.mapper;

import com.yibai.entity.OrderCoupon;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface OrderCouponMapper extends BaseMapper<OrderCoupon, Long>{
    int deleteByPrimaryKey(Long id);

    int insert(OrderCoupon record);

    OrderCoupon selectByPrimaryKey(Long id);

    List<OrderCoupon> selectAll();

    int updateByPrimaryKey(OrderCoupon record);
}