package com.yibai.mapper;

import com.yibai.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SysRoleMapper extends BaseMapper<SysRole,String> {
    SysRole selectByRoleName(String roleName);

    List<SysRole> selectAll();
}