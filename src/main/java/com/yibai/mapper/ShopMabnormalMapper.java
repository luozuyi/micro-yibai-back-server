package com.yibai.mapper;

import com.yibai.entity.ShopMabnormal;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ShopMabnormalMapper extends BaseMapper<ShopMabnormal,Long> {
    int deleteByPrimaryKey(Long id);

    int insert(ShopMabnormal record);

    int insertSelective(ShopMabnormal record);

    ShopMabnormal selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ShopMabnormal record);

    int updateByPrimaryKey(ShopMabnormal record);

    List<ShopMabnormal> selectAllShopMabnormal(ShopMabnormal shopMabnormal);
}