package com.yibai.mapper;


import com.yibai.entity.Business;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface BusinessMapper extends BaseMapper<Business,String>{
    List<Business> selectAll(Business business);

    Business findBusinessById(Long id);

    int deleteByPrimaryKey(Long id);

    int insert(Business record);

    int insertSelective(Business record);

    Business selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Business record);

    int updateByPrimaryKey(Business record);
}