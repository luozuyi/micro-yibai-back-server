package com.yibai.mapper;

import com.yibai.entity.SalesPhonepool;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SalesPhonepoolMapper extends BaseMapper<SalesPhonepool,Long>{
    List<SalesPhonepool> selectNoAllocListByLeader(String leaderName);

    List<SalesPhonepool> selectAllNoAllocList();
}