package com.yibai.mapper;

import com.yibai.entity.ShopMember;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface ShopMemberMapper extends BaseMapper<ShopMember, Long>{
    int deleteByPrimaryKey(Long memberId);

    int insert(ShopMember record);

    int insertSelective(ShopMember record);

    ShopMember selectByPrimaryKey(Long memberId);

    int updateByPrimaryKeySelective(ShopMember record);

    int updateByPrimaryKey(ShopMember record);
}