package com.yibai.mapper;

import com.yibai.entity.SalesPhonePoolRecord;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SalesPhonePoolRecordMapper extends BaseMapper<SalesPhonePoolRecord,Long>{

}