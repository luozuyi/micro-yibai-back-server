package com.yibai.mapper;

import com.yibai.entity.ShopBargain;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ShopBargainMapper extends BaseMapper<ShopBargain,Integer>{
    List<ShopBargain> selectByBesaleman(String besaleman);
}