package com.yibai.mapper;

import com.yibai.entity.ShopRemitRecord;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface ShopRemitRecordMapper extends BaseMapper<ShopRemitRecord,Long>{

    int deleteByPrimaryKey(Long id);

    int insert(ShopRemitRecord record);

    int insertSelective(ShopRemitRecord record);

    ShopRemitRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ShopRemitRecord record);

    int updateByPrimaryKey(ShopRemitRecord record);

    List<ShopRemitRecord> selectAllRemitRecord(ShopRemitRecord shopRemitRecord);

    ShopRemitRecord selectByApprove(Long id);
}
