package com.yibai.mapper;

import com.yibai.entity.ShopInterview;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ShopInterviewMapper {
    ShopInterview selectById(Long id);

    List<ShopInterview> selectAll(ShopInterview record);

    int deleteById(Long id);

    int insertSelective(ShopInterview record);

    int updateByIdSelective(ShopInterview record);

}