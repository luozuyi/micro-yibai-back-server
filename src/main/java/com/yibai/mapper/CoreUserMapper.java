package com.yibai.mapper;

import com.yibai.entity.CoreUser;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CoreUserMapper extends BaseMapper<CoreUser,Long>{

}