package com.yibai.mapper;

import com.yibai.entity.ShopIncome;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ShopIncomeMapper extends BaseMapper<ShopIncome,Long>{
    int deleteByPrimaryKey(Long id);

    int insert(ShopIncome record);

    int insertSelective(ShopIncome record);

    ShopIncome selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ShopIncome record);

    int updateByPrimaryKey(ShopIncome record);

    List<ShopIncome> selectAllShopIncome(ShopIncome shopIncome);
}