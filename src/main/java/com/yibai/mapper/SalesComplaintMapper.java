package com.yibai.mapper;

import com.yibai.entity.SalesComplaint;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SalesComplaintMapper extends BaseMapper<SalesComplaint,Long>{

}