package com.yibai.mapper;

import com.yibai.entity.SalesRecord;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SalesRecordMapper extends BaseMapper<SalesRecord,Long>{

}