package com.yibai.mapper;

import com.yibai.entity.UserUnsuccessreg;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserUnsuccessregMapper extends BaseMapper<UserUnsuccessreg,Integer>{

}