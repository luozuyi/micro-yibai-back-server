package com.yibai.mapper;

import com.yibai.entity.Question;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface QuestionMapper {

    List<Map<String,Object>> selectByIdQuestion(Long questionId);

    List<Map<String,Object>> selectAllQuestion(Map<String,Object> params);

    int deleteByIdsQuestion(Long[] array);

    int insertSelectiveQuestion(Question record);

    int updateByIdSelectiveQuestion(Question record);

    List<Map<String,Object>> selectByIdProposal(Long questionId);

    List<Map<String,Object>> selectAllProposal(Map<String,Object> params);

    int deleteByIdProposal(Long questionId);

    //int updateByIdSelectiveProposal(Question record);
}