package com.yibai.mapper;

import com.yibai.entity.SalesPhonepoolrule;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SalesPhonepoolruleMapper extends BaseMapper<SalesPhonepoolrule,Long>{

}