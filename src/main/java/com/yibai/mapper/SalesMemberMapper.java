package com.yibai.mapper;

import com.yibai.entity.SalesMember;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface SalesMemberMapper extends BaseMapper<SalesMember,Integer>{
    SalesMember selectByMemberName(String memberName);

    List<SalesMember> selectByMobile(String mobile);

    List<SalesMember> selectByBelongSalesman(String belongSalesman);

    List<SalesMember> selectByRule(Map<String,Object> params);

    List<SalesMember> selectByBelongSalesmanSelection(Map<String,Object> params);
}