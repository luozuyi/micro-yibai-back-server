package com.yibai.mapper;

import com.yibai.entity.SysRoleRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SysRoleResMapper extends BaseMapper<SysRoleRes,String>{
    List<SysRoleRes> selectByRoleId(String roleId);
}