package com.yibai.mapper;

import com.yibai.entity.CoreAdmin;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface CoreAdminMapper extends BaseMapper<CoreAdmin, Long>{
    List<CoreAdmin> selectAll();

    List<Map<String,Object>> selectAllMap();
}