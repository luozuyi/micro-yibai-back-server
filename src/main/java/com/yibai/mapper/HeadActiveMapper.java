package com.yibai.mapper;


import com.yibai.entity.HeadActive;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface HeadActiveMapper extends BaseMapper<HeadActive,Integer>{
    List<HeadActive> selectAll(HeadActive headActive);
    int deleteByPrimaryKey(Integer id);

    int insert(HeadActive record);

    int insertSelective(HeadActive record);

    HeadActive selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(HeadActive record);

    int updateByPrimaryKey(HeadActive record);
}