package com.yibai.mapper;

import com.yibai.entity.ShopAdmin;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ShopAdminMapper extends BaseMapper<ShopAdmin,Long>{

}