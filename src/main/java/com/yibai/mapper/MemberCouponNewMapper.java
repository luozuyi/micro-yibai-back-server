package com.yibai.mapper;

import com.yibai.entity.MemberCouponNew;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface MemberCouponNewMapper extends BaseMapper<MemberCouponNew, Long>{
    int deleteByPrimaryKey(Long id);

    int insert(MemberCouponNew record);

    MemberCouponNew selectByPrimaryKey(Long id);

    List<MemberCouponNew> selectAll();

    int updateByPrimaryKey(MemberCouponNew record);
}