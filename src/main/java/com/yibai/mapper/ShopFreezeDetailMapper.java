package com.yibai.mapper;

import com.yibai.entity.ShopFreezeDetail;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface ShopFreezeDetailMapper extends BaseMapper<ShopFreezeDetail,Long>{
    int deleteByPrimaryKey(Long id);

    int insert(ShopFreezeDetail record);

    int insertSelective(ShopFreezeDetail record);

    ShopFreezeDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ShopFreezeDetail record);

    int updateByPrimaryKey(ShopFreezeDetail record);

    ShopFreezeDetail selectByWithdrawId(Long id);
}