package com.yibai.mapper;

import com.yibai.entity.Artificial;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ArtificialMapper extends BaseMapper<Artificial,Long>{
    List<Artificial> selectByBesaleman(String besaleman);
}