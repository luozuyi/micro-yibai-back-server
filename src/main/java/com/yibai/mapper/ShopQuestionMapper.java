package com.yibai.mapper;

import com.yibai.entity.ShopQuestion;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface ShopQuestionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShopQuestion record);

    int insertSelective(ShopQuestion record);

    ShopQuestion selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShopQuestion record);

    int updateByPrimaryKey(ShopQuestion record);
}