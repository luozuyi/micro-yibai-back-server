package com.yibai.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface QuestionAnswerMapper {
    List<Map<String,Object>> selectByIdProblem(Long id);

    List<Map<String,Object>> selectAllProblem(Map<String,Object> params);

    int updateByIdProblem(Map<String,Object> params);

    List<Map<String,Object>> selectByIdReply(Long id);

    List<Map<String,Object>> selectAllReply(Map<String,Object> params);

    int updateByIdReply(Map<String,Object> params);
}