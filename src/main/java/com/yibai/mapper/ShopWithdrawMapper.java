package com.yibai.mapper;

import com.yibai.entity.ShopWithdraw;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
@Mapper
public interface ShopWithdrawMapper extends BaseMapper<ShopWithdraw,Long>{
    int deleteByPrimaryKey(Long id);

    int insert(ShopWithdraw record);

    int insertSelective(ShopWithdraw record);

    ShopWithdraw selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ShopWithdraw record);

    int updateByPrimaryKey(ShopWithdraw record);

    List<ShopWithdraw> selectAllShopWithdraw(ShopWithdraw shopWithdraw);

    ShopWithdraw selectShopWithdrawById(Long id);
}