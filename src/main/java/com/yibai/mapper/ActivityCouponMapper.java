package com.yibai.mapper;


import com.yibai.entity.ActivityCoupon;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ActivityCouponMapper extends BaseMapper<ActivityCoupon,Long> {
    ActivityCoupon findById(Long id);

    int update(ActivityCoupon activityCoupon);

    List<Map<String, Object>> selectAllBySelection(Map<String, Object> params);
}
