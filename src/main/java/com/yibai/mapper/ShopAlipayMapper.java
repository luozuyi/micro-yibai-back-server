package com.yibai.mapper;

import com.yibai.entity.ShopAlipay;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ShopAlipayMapper extends BaseMapper<ShopAlipay, Long>{
    int deleteByPrimaryKey(Long id);

    int insert(ShopAlipay record);

    int insertSelective(ShopAlipay record);

    ShopAlipay selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ShopAlipay record);

    int updateByPrimaryKey(ShopAlipay record);

    List<ShopAlipay> selectAllShopAlipay(ShopAlipay record);

    ShopAlipay selectShopAlipayById(Long id);
}