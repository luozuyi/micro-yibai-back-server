package com.yibai.mapper;

import com.yibai.entity.ShopRecharge;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ShopRechargeMapper extends BaseMapper<ShopRecharge,Long>{
    int deleteByPrimaryKey(Long id);

    int insert(ShopRecharge record);

    int insertSelective(ShopRecharge record);

    ShopRecharge selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ShopRecharge record);

    int updateByPrimaryKey(ShopRecharge record);

    List<ShopRecharge> selectAllRecharge(ShopRecharge shopRecharge);
}