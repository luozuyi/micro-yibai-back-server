package com.yibai.mapper;

import com.yibai.entity.OrderCouponStatistics;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface OrderCouponStatisticsMapper extends BaseMapper<OrderCouponStatistics, Long> {
    List<Map<String, Object>> selectAllBySelection(Map<String, Object> params);
}