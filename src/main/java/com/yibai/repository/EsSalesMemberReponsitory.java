package com.yibai.repository;

import com.yibai.esEntity.EsSalesMember;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EsSalesMemberReponsitory extends ElasticsearchRepository<EsSalesMember, Integer> {
}
