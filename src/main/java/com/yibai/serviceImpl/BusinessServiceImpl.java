package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.Business;
import com.yibai.mapper.BusinessMapper;
import com.yibai.service.BusinessService;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class BusinessServiceImpl extends BaseServiceImpl<Business,Integer> implements BusinessService {

    @Autowired
    private BusinessMapper businessMapper;
    @Override
    public Result pageList(Business business, Integer pageNum, Integer pageSize){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<Business> businesses = businessMapper.selectAll(business);
            PageInfo page = new PageInfo(businesses);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result findBusinessById(Long id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(null == id){
                code = "-3";
                msg = "主键id不能为空";
            }else{
               Business  b = businessMapper.findBusinessById(id);
                result.setData(b);
                code = Constants.SUCCESS;
                msg = "成功";
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

}
