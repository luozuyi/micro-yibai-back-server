package com.yibai.serviceImpl;

import com.yibai.entity.Artificial;
import com.yibai.entity.YibaidianAdmin;
import com.yibai.mapper.ArtificialMapper;
import com.yibai.mapper.YibaidianAdminMapper;
import com.yibai.service.ArtificialService;
import com.yibai.utils.CommonUtil;
import com.yibai.utils.Constants;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;

@Transactional
@Service
public class ArtificialServiceImpl extends BaseServiceImpl<Artificial,Long> implements ArtificialService{
    @Autowired
    private ArtificialMapper artificialMapper;
    @Autowired
    private YibaidianAdminMapper yibaidianAdminMapper;
    @Override
    public Result updateNote(Long id, String remarks, String isDeal, String dealShopUrl) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(id == null){
                code = "-3";
                msg = "主键id不能为空";
            }else{
                Artificial artificial = artificialMapper.selectByPrimaryKey(id);
                if(artificial == null){
                    code = "-4";
                    msg = "修改的对象不存在";
                }else{
                    artificial.setRemarks(remarks);
                    artificial.setRemarkstime(new Date());
                    artificial.setIsDeal(Integer.parseInt(isDeal));
                    artificial.setDealShopUrl(dealShopUrl);
                    artificialMapper.updateByPrimaryKeySelective(artificial);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result impackMyMember(Long id,String yibaiAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            YibaidianAdmin yibaidianAdmin = yibaidianAdminMapper.selectByPrimaryKey(CommonUtil.getYibaidianAdminId(yibaiAdminToken));
            String username = yibaidianAdmin.getAdminName();
            Artificial artificial = artificialMapper.selectByPrimaryKey(id);
            List<Artificial> artificialList = artificialMapper.selectByBesaleman(username);
            if (artificialList.size()>0) {
                Artificial oldartificial = artificialList.get(0);
                long nowdate=new Date().getTime();
                long olddate=oldartificial.getGrabtime().getTime();
                /*5分钟之内抢过单*/
                if(Math.abs(nowdate - olddate)>300000) {
                    artificial.setBesaleman(username);
                    artificial.setGrabtime(new Date());
                    artificialMapper.updateByPrimaryKeySelective(artificial);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }else{
                    code = "-3";
                    msg = "未到5分钟";
                }
            }else{
                artificial.setBesaleman(username);
                artificial.setGrabtime(new Date());
                artificialMapper.updateByPrimaryKeySelective(artificial);
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
