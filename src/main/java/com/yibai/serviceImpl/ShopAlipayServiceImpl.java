package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.*;
import com.yibai.mapper.*;
import com.yibai.service.ShopAlipayService;
import com.yibai.service.ShopMemberService;
import com.yibai.utils.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Transactional
@Service
public class ShopAlipayServiceImpl extends BaseServiceImpl<ShopAlipay, Long> implements ShopAlipayService {

    @Autowired
    private ShopAlipayMapper dao;

    @Autowired
    private ShopRechargeMapper shopRechargeMapper;

    @Autowired
    private ShopDealDetailMapper shopDealDetailMapper;

    @Autowired
    private ShopMemberService shopMemberService;

    @Autowired
    private ShopMemberMapper shopMemberMapper;

    @Autowired
    private ShopIncomeMapper shopIncomeMapper;

    @Autowired
    private YibaidianAdminMapper yibaidianAdminMapper;

    /**
     * 分页多条件筛选查询支付宝充值记录
     * @param record
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result selectAllShopAlipay(ShopAlipay record,String yibaiAdminToken, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            //通过token获取yibaidianAdminId
            String yibaidianAdminId = CommonUtil.getYibaidianAdminId(yibaiAdminToken);
            //通过yibaidianAdminId查询YibaidianAdmin对象
            YibaidianAdmin yibaidianAdmin = yibaidianAdminMapper.selectByPrimaryKey(yibaidianAdminId);
            if (yibaidianAdmin==null){
                return new Result(Constants.FAIL,"请登录之后再进行操作");
            }
            PageHelperNew.startPage(pageNum, pageSize);
            List<ShopAlipay> shopAlipays = dao.selectAllShopAlipay(record);
            //根据状态和登录的管理员确认操作信息
            for (ShopAlipay shopAlipay:
                 shopAlipays) {
                if (shopAlipay.getStatus()==2){
                    shopAlipay.setOperation("审核");
                }else if (shopAlipay.getStatus()==0){
                    shopAlipay.setOperation("无法操作");
                } else if (shopAlipay.getStatus()==1 && yibaidianAdmin.getCoreAdminId()==1){
                    shopAlipay.setOperation("管理员审核");
                }
            }
            PageInfo page = new PageInfo(shopAlipays);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;

    }


    /**
     * 审核修改对应的状态
     *
     * @param id
     * @param approve
     * @param yibaiAdminToken
     * @return
     */
    public Result approve(String id,String approve,String yibaiAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            //通过token获取yibaidianAdminId
            String yibaidianAdminId = CommonUtil.getYibaidianAdminId(yibaiAdminToken);
            //通过yibaidianAdminId查询YibaidianAdmin对象
            YibaidianAdmin yibaidianAdmin = yibaidianAdminMapper.selectByPrimaryKey(yibaidianAdminId);
            if (yibaidianAdmin==null){
                return new Result(Constants.FAIL,"请登录之后再进行操作");
            }
            ShopAlipay shopAlipay = dao.selectByPrimaryKey(Long.parseLong(id));
            shopAlipay.setApprove(Integer.parseInt(approve));
            ShopRecharge recharge = shopRechargeMapper.selectByPrimaryKey(shopAlipay.getRechargeId());
            //如果状态为0或者审核人字段不为空，则表示已经成功处理
            if (recharge.getStatus() == 0 || shopAlipay.getApproveMan() != null) {
                code = Constants.SUCCESS;
                msg = "该审核已经处理";
            } else {
                //审核成功之后进行三个操作
                //1 修改ShopRecharge表中的状态
                //2 修改ShopMember表中的余额
                //3 新增ShopDealDetail详情
                msg=remitRecordSuccess(shopAlipay, yibaidianAdmin.getCoreAdminId());
                //审核通过
                if (shopAlipay.getApprove() != null && shopAlipay.getApprove() == 0) {
                    ShopMember member = shopMemberMapper.selectByPrimaryKey(shopAlipay.getMemberId());
                    //收入详细表
                    ShopIncome shopIncome = new ShopIncome();
                    shopIncome.setMemberId(shopAlipay.getMemberId());
                    shopIncome.setcTime(new Date());
                    // 0：代金券 1：RMB
                    shopIncome.setMoneyType(1);
                    //0：收入 1：支出
                    shopIncome.setFinanceType(0);
                    //2：支付宝充值
                    shopIncome.setFlunFlow(2);
                    shopIncome.setNum(BigDecimal.valueOf(recharge.getRechargeMoney()));
                    shopIncomeMapper.insertSelective(shopIncome);
                    shopAlipay.setApproveMan(yibaidianAdmin.getAdminName());
                    dao.updateByPrimaryKeySelective(shopAlipay);
                    code = Constants.SUCCESS;
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 根据id获取支付宝充值信息
     * @param id
     * @return
     */
    @Override
    public Result selectShopAlipayById(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            ShopAlipay shopAlipay = dao.selectShopAlipayById(Long.parseLong(id));
            result.setData(shopAlipay);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    /**
     * 审核对应的操作
     *
     * @param shopAlipay
     * @param approveAdminId
     */
    public String remitRecordSuccess(ShopAlipay shopAlipay, Long approveAdminId) {
        Long memberId = shopAlipay.getMemberId();
        //如果rechargeId不为空，则根据该id查询出网银支付对象
        if (StringUtils.isEmpty(shopAlipay.getRechargeId() + "")) {
            return "审核异常";
        }
        ShopRecharge shopRecharge = shopRechargeMapper.selectByPrimaryKey(shopAlipay.getRechargeId());
        if (shopRecharge.getStatus() == 0) {
            return "该审核已经处理";
        }
        try {
            //如果入参approve为0表示审核成功
            if (shopAlipay.getApprove() != null && shopAlipay.getApprove() == Integer.parseInt(Constants.SUCCESS)) {
                //修改shopRecharge字段status为0，表示成功
                shopRecharge.setStatus(0);
                //更改用户余额
                ShopMember shopMember = updateMoney(memberId, shopRecharge.getRechargeMoney());
                ShopDealDetail detail = new ShopDealDetail();
                detail.setShopMember(shopMember);
                detail.setMemberId(memberId);
                //流水号
                String serialNumber = new Date().getTime() + "" + memberId;
                detail.setSerialNumber(serialNumber);
                detail.setCreateTime(DateUtil.getDateFormatter());
                //资金流向：收入
                detail.setDealDirect(0);
                //类型：充值
                detail.setDealType(0);
                //收入金额
                detail.setInMoney(shopRecharge.getRechargeMoney());
                //用户余额
                detail.setMemMoney(shopMember.getMoney());
                detail.setApproveAdminId(approveAdminId);
                detail.setApproveTime(new Date());
                //保存收支明细
                shopDealDetailMapper.insertSelective(detail);
                //审核失败
            } else if (shopAlipay.getApprove() != null && shopAlipay.getApprove() == 1) {
                //修改shopRecharge字段status为1，表示失败
                shopRecharge.setStatus(1);
            }
            //修改shopRecharge的状态
            shopRechargeMapper.updateByPrimaryKeySelective(shopRecharge);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            return "审核异常";
        }
        return "审核处理成功";
    }

    /**
     * 修改账户余额
     * @param id
     * @param money
     * @return
     */
    public ShopMember updateMoney(Long id, double money) {
        if (id==null){
            return null;
        }
        //根据会员id查询账户余额
        ShopMember shopMember = shopMemberMapper.selectByPrimaryKey(id);
        //如果余额为空，则设置余额为0.0
        if (shopMember.getMoney()==null){
            shopMember.setMoney(0.0);
        }
        //金额叠加
        shopMember.setMoney(shopMember.getMoney()+money);
        shopMemberMapper.updateByPrimaryKeySelective(shopMember);
        return shopMemberMapper.selectByPrimaryKey(id);
    }

}
