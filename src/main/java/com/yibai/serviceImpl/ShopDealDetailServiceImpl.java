package com.yibai.serviceImpl;

import com.yibai.entity.ShopDealDetail;
import com.yibai.service.ShopDealDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ShopDealDetailServiceImpl extends BaseServiceImpl<ShopDealDetail,Long> implements ShopDealDetailService{
}
