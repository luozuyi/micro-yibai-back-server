package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.HeadActive;
import com.yibai.mapper.HeadActiveMapper;
import com.yibai.service.HeadActiveService;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class HeadActiveServiceImpl extends BaseServiceImpl<HeadActive,Integer> implements HeadActiveService {
    @Autowired
    private HeadActiveMapper headActiveMapper;
    @Override
    public Result pageList(HeadActive headActive, Integer pageNum, Integer pageSize){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<HeadActive> headActives = headActiveMapper.selectAll(headActive);
            PageInfo page = new PageInfo(headActives);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

}
