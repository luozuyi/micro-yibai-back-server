package com.yibai.serviceImpl;

import com.yibai.entity.MemberCouponNew;
import com.yibai.service.MemberCouponNewService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class MemberCouponNewServiceImpl extends BaseServiceImpl<MemberCouponNew, Long> implements MemberCouponNewService {
}
