package com.yibai.serviceImpl;

import com.yibai.entity.*;
import com.yibai.esEntity.EsSalesMember;
import com.yibai.mapper.*;
import com.yibai.repository.EsSalesMemberReponsitory;
import com.yibai.service.ShopBargainService;
import com.yibai.utils.CommonUtil;
import com.yibai.utils.Constants;
import com.yibai.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Transactional
@Service
public class ShopBargainServiceImpl extends BaseServiceImpl<ShopBargain,Integer> implements ShopBargainService{
    @Autowired
    private ShopBargainMapper shopBargainMapper;
    @Autowired
    private EsSalesMemberReponsitory esSalesMemberReponsitory;
    @Autowired
    private SalesAdminMapper salesAdminMapper;
    @Autowired
    private SalesMemberMapper salesMemberMapper;
    @Autowired
    private ShopAdminMapper shopAdminMapper;
    @Autowired
    private CoreAdminMapper coreAdminMapper;
    @Autowired
    private CoreUserMapper coreUserMapper;
    @Autowired
    private SalesRecordMapper salesRecordMapper;
    @Override
    public Result updateNote(Integer id, String remarks) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(id == null){
                code = "-3";
                msg = "主键id不能为空";
            }else{
                 ShopBargain shopBargain = shopBargainMapper.selectByPrimaryKey(id);
                 if(shopBargain == null){
                     code = "-4";
                     msg = "修改的对象不存在";
                 }else{
                     shopBargain.setRemarks(remarks);
                     shopBargain.setRemarkstime(new Date());
                     shopBargainMapper.updateByPrimaryKeySelective(shopBargain);
                     code = Constants.SUCCESS;
                     msg = "修改备注成功";
                 }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result impackMyMember(Integer id, String yibaiAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            Integer coreAmindId = Integer.valueOf(CommonUtil.getCoreAdminId(yibaiAdminToken));
            SalesAdmin salesAdmin = salesAdminMapper.selectByPrimaryKey(Long.valueOf(coreAmindId));
           // ShopAdmin shopAdmin = shopAdminMapper.selectByPrimaryKey(Long.valueOf(coreAmindId));
            CoreAdmin coreAdmin = coreAdminMapper.selectByPrimaryKey(Long.valueOf(coreAmindId));
            CoreUser coreUser = coreUserMapper.selectByPrimaryKey(coreAdmin.getUserId());
            if (salesAdmin.getNum() > salesAdmin.getLevelnum()) {
                code = "-3";
                msg = "拥有的数量已经超过最大数量";
            }else{
                ShopBargain shopBargain = shopBargainMapper.selectByPrimaryKey(id);
                String bargain_tel = shopBargain.getBargainTel();
                List<ShopBargain> list = null;
                String saleman = coreUser.getUsername();
                list = shopBargainMapper.selectByBesaleman(saleman);
                if (list.size() > 0) {
                    ShopBargain oldbargainprice = list.get(0);
                    long nowdate = new Date().getTime();
                    long olddate = 0;
                    if (null != oldbargainprice.getGrabtime()) {
                        olddate = oldbargainprice.getGrabtime().getTime();
                    }
                    if(Math.abs(nowdate - olddate) < 300000){
                        code = "-4";
                        msg = "还没有超过5分钟";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                }
                String salesUser = shopBargain.getMemberName();
                if(salesUser != null && (salesUser.length()>1)) {//用户是注册用户
                    SalesMember salesmember = salesMemberMapper.selectByMemberName(salesUser);
                    if (salesmember != null && salesmember.getBelongSalesman() == null) {
                        salesmember.setBelongSalesman(coreUser.getUsername());
                        salesmember.setLeader(salesAdmin.getLeader());
                        salesmember.setIsovertime(0);
                        salesMemberMapper.updateByPrimaryKeySelective(salesmember);

                        SalesRecord salesRecord = new SalesRecord();
                        salesRecord.setImpactTime(new Date());
                        salesRecord.setUpdateTime(new Date());
                        salesRecord.setAdmin(Long.valueOf(coreAmindId));
                        salesRecord.setMemberId(salesmember.getId());
                        salesRecordMapper.insertSelective(salesRecord);


                        salesAdmin.setNum(salesAdmin.getNum()+1);
                        salesAdmin.setTodaynum(salesAdmin.getTodaynum()+1);
                        salesAdmin.setTotalnum(salesAdmin.getTodaynum()+1);
                        salesAdminMapper.updateByPrimaryKeySelective(salesAdmin);

                        EsSalesMember esSalesMember = new EsSalesMember();
                        esSalesMember.setId(salesmember.getId());
                        esSalesMember.setBelongSalman(coreUser.getUsername());
                        esSalesMember.setCheckCount(salesmember.getCheckcount());
                        esSalesMember.setCurrentLoginTime(salesmember.getCurrentLoginTime());
                        esSalesMember.setFreezeMoney(salesmember.getFreezeMoney());
                        esSalesMember.setIsOverTime(salesmember.getIsovertime());
                        esSalesMember.setLastJoinPoolTime(salesmember.getLastJoinPoolTime());
                        esSalesMember.setLeader(salesmember.getLeader());
                        esSalesMember.setLoginCount(salesmember.getLoginCount());
                        esSalesMember.setMemberAddress(salesmember.getMemberAddress());
                        esSalesMember.setMemberDemand(salesmember.getMemberDemand());
                        esSalesMember.setMemberName(salesmember.getMemberName());
                        esSalesMember.setMobile(salesmember.getMobile());
                        esSalesMember.setMoney(salesmember.getMoney());
                        esSalesMember.setNeedsales(salesmember.getNeedsales());
                        esSalesMember.setQq(salesmember.getQq());
                        esSalesMember.setQuickNote(salesmember.getQuicknote());
                        esSalesMember.setReg_classify(salesmember.getRegClassify());
                        esSalesMember.setReg_origin(salesmember.getRegOrigin());
                        esSalesMember.setShopCount(salesmember.getShopCount());
                        esSalesMember.setSort(salesmember.getSort());
                        esSalesMember.setStatus(salesmember.getStatus());
                        esSalesMember.setTbCheckCount(salesmember.getTbcheckcount());
                        esSalesMember.setVoucher(salesmember.getVoucher());
                        esSalesMemberReponsitory.save(esSalesMember);
                    }else{
                        code = "-4";
                        msg = "已经被别人添加";
                        result.setCode(code);
                        result.setMsg(msg);
                        return result;
                    }
                }else{
                    List<SalesMember> salesmemberlist = salesMemberMapper.selectByMobile(shopBargain.getBargainTel());
                    if(salesmemberlist.size()>0){
                        SalesMember salesmember = salesmemberlist.get(0);
                        if(salesmember.getBelongSalesman() == null || salesmember.getBelongSalesman() == "") {
                            salesmember.setBelongSalesman(coreUser.getUsername());
                            salesMemberMapper.updateByPrimaryKeySelective(salesmember);
                            /*修改记录*/
                            SalesRecord salesRecord = new SalesRecord();
                            salesRecord.setImpactTime(new Date());
                            salesRecord.setUpdateTime(new Date());
                            salesRecord.setAdmin(Long.valueOf(coreAmindId));
                            salesRecord.setMemberId(salesmember.getId());
                            salesRecordMapper.insertSelective(salesRecord);


                            salesAdmin.setNum(salesAdmin.getNum()+1);
                            salesAdmin.setTodaynum(salesAdmin.getTodaynum()+1);
                            salesAdmin.setTotalnum(salesAdmin.getTodaynum()+1);
                            salesAdminMapper.updateByPrimaryKeySelective(salesAdmin);

                            EsSalesMember esSalesMember = new EsSalesMember();
                            esSalesMember.setId(salesmember.getId());
                            esSalesMember.setBelongSalman(coreUser.getUsername());
                            esSalesMember.setCheckCount(salesmember.getCheckcount());
                            esSalesMember.setCurrentLoginTime(salesmember.getCurrentLoginTime());
                            esSalesMember.setFreezeMoney(salesmember.getFreezeMoney());
                            esSalesMember.setIsOverTime(salesmember.getIsovertime());
                            esSalesMember.setLastJoinPoolTime(salesmember.getLastJoinPoolTime());
                            esSalesMember.setLeader(salesmember.getLeader());
                            esSalesMember.setLoginCount(salesmember.getLoginCount());
                            esSalesMember.setMemberAddress(salesmember.getMemberAddress());
                            esSalesMember.setMemberDemand(salesmember.getMemberDemand());
                            esSalesMember.setMemberName(salesmember.getMemberName());
                            esSalesMember.setMobile(salesmember.getMobile());
                            esSalesMember.setMoney(salesmember.getMoney());
                            esSalesMember.setNeedsales(salesmember.getNeedsales());
                            esSalesMember.setQq(salesmember.getQq());
                            esSalesMember.setQuickNote(salesmember.getQuicknote());
                            esSalesMember.setReg_classify(salesmember.getRegClassify());
                            esSalesMember.setReg_origin(salesmember.getRegOrigin());
                            esSalesMember.setShopCount(salesmember.getShopCount());
                            esSalesMember.setSort(salesmember.getSort());
                            esSalesMember.setStatus(salesmember.getStatus());
                            esSalesMember.setTbCheckCount(salesmember.getTbcheckcount());
                            esSalesMember.setVoucher(salesmember.getVoucher());
                            esSalesMemberReponsitory.save(esSalesMember);
                        }
                    }
                }

                if (list.size() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                    ShopBargain oldbargainprice = list.get(0);
                    long nowdate = new Date().getTime();
                    long olddate = 0;
                    if (null != oldbargainprice.getGrabtime()) {
                        olddate = oldbargainprice.getGrabtime().getTime();
                    }
                    //long olddate=oldbargainprice.getGrabtime().getTime();
                    /*5分钟之内抢过单*/
                    if (Math.abs(nowdate - olddate) > 300000) {
                        List<ShopBargain> list_tel = shopBargainMapper.selectByBesaleman(bargain_tel);
                        if (list_tel.size() > 0) {
                            for (int i = 0; i < list_tel.size(); i++) {
                                if (list_tel.get(i).getBesaleman() == null || list_tel.get(i).getBesaleman() == "") {
                                    list_tel.get(i).setBesaleman(coreUser.getUsername());
                                }
                                if (list_tel.get(i).getGrabtime() == null) {
                                    list_tel.get(i).setGrabtime(new Date());
                                }
                            }
                        }
                        shopBargain.setBesaleman(coreUser.getUsername());
                        shopBargain.setGrabtime(new Date());
                        shopBargainMapper.updateByPrimaryKeySelective(shopBargain);

                        code = Constants.SUCCESS;
                        msg = "成功";
                    } else {
                        code = "-4";
                        msg = "还没有超过5分钟";
                    }
                } else {
                    List<ShopBargain> list_tel = shopBargainMapper.selectByBesaleman(bargain_tel);
                    if (list_tel.size() > 0) {
                        for (int i = 0; i < list_tel.size(); i++) {
                            if (list_tel.get(i).getBesaleman() == null || list_tel.get(i).getBesaleman() == "") {
                                list_tel.get(i).setBesaleman(coreUser.getUsername());
                            }
                            if (list_tel.get(i).getGrabtime() == null) {
                                list_tel.get(i).setGrabtime(new Date());
                            }
                        }
                    }
                    shopBargain.setBesaleman(coreUser.getUsername());
                    shopBargain.setGrabtime(new Date());
                    shopBargainMapper.updateByPrimaryKeySelective(shopBargain);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
