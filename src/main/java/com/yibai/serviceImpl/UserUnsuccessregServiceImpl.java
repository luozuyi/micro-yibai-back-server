package com.yibai.serviceImpl;

import com.yibai.entity.UserUnsuccessreg;
import com.yibai.entity.YibaidianAdmin;
import com.yibai.mapper.UserUnsuccessregMapper;
import com.yibai.mapper.YibaidianAdminMapper;
import com.yibai.service.UserUnsuccessregService;
import com.yibai.utils.CommonUtil;
import com.yibai.utils.Constants;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;

@Transactional
@Service
public class UserUnsuccessregServiceImpl extends BaseServiceImpl<UserUnsuccessreg,Integer> implements UserUnsuccessregService{
    @Autowired
    private UserUnsuccessregMapper userUnsuccessregMapper;
    @Autowired
    private YibaidianAdminMapper yibaidianAdminMapper;
    @Override
    public Result updateNote(Integer id, String note, String yibaiAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            YibaidianAdmin yibaidianAdmin = yibaidianAdminMapper.selectByPrimaryKey(CommonUtil.getYibaidianAdminId(yibaiAdminToken));
            String username = yibaidianAdmin.getAdminName();
            if(id == null){
                code = "-3";
                msg = "主键id不能为空";
            }else{
                UserUnsuccessreg userUnsuccessreg = userUnsuccessregMapper.selectByPrimaryKey(id);
                if(userUnsuccessreg == null){
                    code = "-4";
                    msg = "修改的对象不存在";
                }else{
                    userUnsuccessreg.setNote(note);
                    userUnsuccessreg.setSalesname(username);
                    userUnsuccessreg.setOptime(new Date());
                    userUnsuccessregMapper.updateByPrimaryKeySelective(userUnsuccessreg);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
