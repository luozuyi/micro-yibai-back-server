package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.ShopMabnormal;
import com.yibai.mapper.ShopMabnormalMapper;
import com.yibai.service.ShopMabnormalService;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class ShopMabnormalServiceImpl extends BaseServiceImpl<ShopMabnormal,Long> implements ShopMabnormalService{

    @Autowired
    private ShopMabnormalMapper dao;

    /**
     * 多条件查询会员资金调整记录并分页
     * @param shopMabnormal
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result selectAllShopMabnormal(ShopMabnormal shopMabnormal, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<ShopMabnormal> shopMabnormals = dao.selectAllShopMabnormal(shopMabnormal);
            PageInfo page = new PageInfo(shopMabnormals);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
