package com.yibai.serviceImpl;

import com.yibai.entity.SalesAdmin;
import com.yibai.entity.SalesMember;
import com.yibai.esEntity.EsSalesMember;
import com.yibai.mapper.SalesAdminMapper;
import com.yibai.mapper.SalesMemberMapper;
import com.yibai.repository.EsSalesMemberReponsitory;
import com.yibai.service.SalesAdminService;
import com.yibai.utils.Constants;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;
import java.util.Map;

@Transactional
@Service
public class SalesAdminServiceImpl extends BaseServiceImpl<SalesAdmin,Long> implements SalesAdminService{
    @Autowired
    private SalesAdminMapper salesAdminMapper;
    @Autowired
    private SalesMemberMapper salesMemberMapper;
    @Autowired
    private EsSalesMemberReponsitory esSalesMemberReponsitory;
    @Override
    public Result selectLeaderList() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<Map<String,Object>> mapList = salesAdminMapper.selectLeaderList();
            result.setData(mapList);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result selectMinisterList() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<Map<String,Object>> mapList = salesAdminMapper.selectMinisterList();
            result.setData(mapList);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result addBatch(Long[] list, Integer leaderId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            /*查询所有组员的客户把客户leader设置为当前leader组长的id*/
            for (Long id:list) {
                SalesAdmin salesAdmin = salesAdminMapper.selectByPrimaryKey(id);
                salesAdmin.setLeader(leaderId);
                String username = salesAdminMapper.selectUsername(id);
                List<SalesMember> salesMembers = salesMemberMapper.selectByBelongSalesman(username);
                for (SalesMember salesMember:salesMembers) {
                    salesMember.setLeader(leaderId);
                    salesMemberMapper.updateByPrimaryKeySelective(salesMember);

                    EsSalesMember esSalesMember = new EsSalesMember();
                    esSalesMember.setBelongSalman(salesMember.getBelongSalesman());
                    esSalesMember.setLeader(salesMember.getLeader());
                    esSalesMember.setIsOverTime(0);

                    esSalesMember.setNeedsales(salesMember.getNeedsales());
                    esSalesMember.setId(salesMember.getId());
                    esSalesMember.setCheckCount(salesMember.getCheckcount());
                    esSalesMember.setCreateTime(salesMember.getCreateTime());
                    esSalesMember.setCurrentLoginTime(salesMember.getCurrentLoginTime());
                    esSalesMember.setFreezeMoney(salesMember.getFreezeMoney());
                    esSalesMember.setLastJoinPoolTime(salesMember.getLastJoinPoolTime());
                    esSalesMember.setLoginCount(salesMember.getLoginCount());
                    esSalesMember.setMemberAddress(salesMember.getMemberAddress());
                    esSalesMember.setMemberDemand(salesMember.getMemberDemand());
                    esSalesMember.setMemberName(salesMember.getMemberName());
                    esSalesMember.setMobile(salesMember.getMobile());
                    esSalesMember.setMoney(salesMember.getMoney());
                    esSalesMember.setQq(salesMember.getQq());
                    esSalesMember.setQuickNote(salesMember.getQuicknote());
                    esSalesMember.setReg_classify(salesMember.getRegClassify());
                    esSalesMember.setReg_origin(salesMember.getRegOrigin());
                    esSalesMember.setShopCount(salesMember.getShopCount());
                    esSalesMember.setSort(salesMember.getSort());
                    esSalesMember.setStatus(salesMember.getStatus());
                    esSalesMember.setTbCheckCount(salesMember.getTbcheckcount());
                    esSalesMember.setVoucher(salesMember.getVoucher());
                    esSalesMemberReponsitory.save(esSalesMember);
                }
            }
            /*把组长的客户设置为组长id*/
            String leaderUsername = salesAdminMapper.selectUsername(Long.valueOf(leaderId));
            List<SalesMember> leaderSalesMembers = salesMemberMapper.selectByBelongSalesman(leaderUsername);
            for (SalesMember salesMember:leaderSalesMembers) {
                salesMember.setLeader(leaderId);
                salesMemberMapper.updateByPrimaryKeySelective(salesMember);

                EsSalesMember esSalesMember = new EsSalesMember();
                esSalesMember.setBelongSalman(salesMember.getBelongSalesman());
                esSalesMember.setLeader(salesMember.getLeader());
                esSalesMember.setIsOverTime(0);

                esSalesMember.setNeedsales(salesMember.getNeedsales());
                esSalesMember.setId(salesMember.getId());
                esSalesMember.setCheckCount(salesMember.getCheckcount());
                esSalesMember.setCreateTime(salesMember.getCreateTime());
                esSalesMember.setCurrentLoginTime(salesMember.getCurrentLoginTime());
                esSalesMember.setFreezeMoney(salesMember.getFreezeMoney());
                esSalesMember.setLastJoinPoolTime(salesMember.getLastJoinPoolTime());
                esSalesMember.setLoginCount(salesMember.getLoginCount());
                esSalesMember.setMemberAddress(salesMember.getMemberAddress());
                esSalesMember.setMemberDemand(salesMember.getMemberDemand());
                esSalesMember.setMemberName(salesMember.getMemberName());
                esSalesMember.setMobile(salesMember.getMobile());
                esSalesMember.setMoney(salesMember.getMoney());
                esSalesMember.setQq(salesMember.getQq());
                esSalesMember.setQuickNote(salesMember.getQuicknote());
                esSalesMember.setReg_classify(salesMember.getRegClassify());
                esSalesMember.setReg_origin(salesMember.getRegOrigin());
                esSalesMember.setShopCount(salesMember.getShopCount());
                esSalesMember.setSort(salesMember.getSort());
                esSalesMember.setStatus(salesMember.getStatus());
                esSalesMember.setTbCheckCount(salesMember.getTbcheckcount());
                esSalesMember.setVoucher(salesMember.getVoucher());
                esSalesMemberReponsitory.save(esSalesMember);
            }
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result addDepartMember(Long[] list, Integer minister) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            for (Long id:list) {
                SalesAdmin salesAdmin = salesAdminMapper.selectByPrimaryKey(id);
                salesAdmin.setMinister(minister);
                salesAdminMapper.updateByPrimaryKeySelective(salesAdmin);
            }
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result numUpdate(Long id, Integer num, Integer level, Integer limitnum) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if((num == null)&&(level == null)&&(limitnum == null)){
                code = "-3";
                msg = "参数不能都为空";
            }else{
                SalesAdmin salesAdmin = salesAdminMapper.selectByPrimaryKey(id);
                if(num != null){
                    salesAdmin.setLevelnum(num);
                }
                if(level != null){
                    salesAdmin.setLevel(level);
                }
                if(limitnum != null){
                    salesAdmin.setLimitnum(limitnum);
                }
                salesAdminMapper.updateByPrimaryKeySelective(salesAdmin);
                code = Constants.SUCCESS;
                msg = "成功";
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result mgoalUpdate(Long[] list, Integer mgoal) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            for (Long id:list) {
                SalesAdmin salesAdmin = salesAdminMapper.selectByPrimaryKey(id);
                salesAdmin.setMgoal(mgoal);
                salesAdminMapper.updateByPrimaryKeySelective(salesAdmin);
            }
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateBatchLevelChange(Long[] list, Integer levelChange) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            for (Long id:list) {
                SalesAdmin salesAdmin = salesAdminMapper.selectByPrimaryKey(id);
                salesAdmin.setLevel(levelChange);
                salesAdminMapper.updateByPrimaryKeySelective(salesAdmin);
            }
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateBatchLevelnum(Long[] list, Integer levelnum) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            for (Long id:list) {
                SalesAdmin salesAdmin = salesAdminMapper.selectByPrimaryKey(id);
                salesAdmin.setLevelnum(levelnum);
                salesAdminMapper.updateByPrimaryKeySelective(salesAdmin);
            }
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
