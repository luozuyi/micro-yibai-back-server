package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.Question;
import com.yibai.mapper.QuestionMapper;
import com.yibai.service.QuestionService;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;
import java.util.Map;
@Transactional
@Service
public class QuestionServiceImpl implements QuestionService {
    @Autowired
    private QuestionMapper questionMapper;
    /**
     * 在线回答列表》提问管理分页查询（完全匹配）
     * @param pageNum 当前页
     * @param pageSize 一页显示多少数据
     * @param params 参数map
     * @return
     */
    @Override
    public Result pageListQuestion(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> mapList = questionMapper.selectAllQuestion(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(mapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    /**
     * 在线回答列表》提问管理每列详细信息
     * @param questionId 参数questionId
     * @return
     */
    @Override
    public Result detailQuestion(Long questionId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(questionId == null){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                List<Map<String,Object>> list = questionMapper.selectByIdQuestion(questionId);
                result.setData(list);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    /**
     * 在线回答列表》提问管理更新数据
     * @param record 参数实体Question
     * @return
     */
    @Override
    public Result updateQuestion(Question record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(record.getQuestionId() == null){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                record.setEnddate(new Date());
                questionMapper.updateByIdSelectiveQuestion(record);
                code = Constants.SUCCESS;
                msg = "更新成功";
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    /**
     * 在线回答列表》提问管理新增数据
     * @param record 参数实体Question
     * @return
     */
    //缺当前登录用户信息
    @Override
    public Result insertSelectiveQuestion(Question record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(record.getTitle())){
                code = "-3";
                msg = "问题标题不能为空";
            }else if(StringUtils.isBlank(record.getStartcontent())){
                code = "-4";
                msg = "问题描述不能为空";
            }if(record.getCategoryId() == null){
                code = "-5";
                msg = "问题分类不能为空";
            }if(record.getCategoryType() == null){
                code = "-6";
                msg = "交易状态不能为空";
            }else{
                if(!StringUtils.isBlank(record.getEndcontent())){
                    //回复内容不为空
                    record.setEnddate(new Date());
                }
                //后台使用用户名找到对应的member_id
                record.setMemberId((long)109);
                record.setStartdate(new Date());
                record.setQuestionType((long)0);
                questionMapper.insertSelectiveQuestion(record);
                code = Constants.SUCCESS;
                msg = "新增成功";
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 在线回答列表》提问管理批量删除数据
     * @param array 参数数组
     * @return
     */
    @Override
    public Result deleteByIdsQuestion(Long[] array) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(array==null || (array!=null && array.length==0)){
                code = "-3";
                msg = "主键questionIds数组不能为空";
            }else {
                questionMapper.deleteByIdsQuestion(array);
                code = Constants.SUCCESS;
                msg = "删除成功";
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 在线回答列表》建议管理分页查询（完全匹配）
     * @param pageNum 当前页
     * @param pageSize 一页显示多少数据
     * @param params 参数map
     * @return
     */
    @Override
    public Result pageListProposal(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> mapList = questionMapper.selectAllProposal(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(mapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    /**
     * 在线回答列表》建议管理每列详细信息
     * @param questionId 参数questionId
     * @return
     */
    @Override
    public Result detailProposal(Long questionId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(questionId == null){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                List<Map<String,Object>> list = questionMapper.selectByIdProposal(questionId);
                result.setData(list);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    /**
     * 在线回答列表》建议管理删除数据
     * @param questionId 参数questionId
     * @return
     */
    @Override
    public Result deleteByIdProposal(Long questionId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(questionId == null){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                questionMapper.deleteByIdProposal(questionId);
                code = Constants.SUCCESS;
                msg = "删除成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
