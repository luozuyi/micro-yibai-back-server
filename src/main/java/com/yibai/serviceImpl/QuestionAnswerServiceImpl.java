package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.mapper.QuestionAnswerMapper;
import com.yibai.service.QuestionAnswerService;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;
import java.util.Map;
@Transactional
@Service
public class QuestionAnswerServiceImpl implements QuestionAnswerService{
    @Autowired
    private QuestionAnswerMapper questionAnswerMapper;
    /**
     * 咨询列表》问题列表分页查询（完全匹配）
     * @param pageNum 当前页
     * @param pageSize 一页显示多少数据
     * @param params 参数map
     * @return
     */
    //缺少登录的用户名
    @Override
    public Result pageListProblem(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(params.get("type")!= null && Integer.parseInt(params.get("type").toString())==1){
                //将登录的用户名存入belongSalman中
                //params.put("belongSalman",username);
                System.out.println("type=1");
            }else{
                System.out.println("type=0");
            }
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> mapList = questionAnswerMapper.selectAllProblem(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(mapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    /**
     * 咨询列表》问题列表每列详细信息
     * @param id 参数id
     * @return
     */
    @Override
    public Result detailProblem(Long id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(id == null){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                List<Map<String,Object>> list = questionAnswerMapper.selectByIdProblem(id);
                result.setData(list);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 咨询列表》问题列表更新数据（审核）
     * @param params 参数map
     * @return
     */
    //缺发送短信
    @Override
    public Result updateProblemCheck(Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(params.get("id") == null || StringUtils.isBlank(params.get("id").toString())){
                code = "-3";
                msg = "主键id不能为空";
            }else if(params.get("checkPassed")== null ||(StringUtils.isBlank(params.get("checkPassed").toString()))){
                code = "-4";
                msg = "审核类型不能为空";
            }else if (Integer.parseInt(params.get("checkPassed").toString())==1){
                if(params.get("shortMessage")!= null && Integer.parseInt(params.get("shortMessage").toString())==1){
                    if(params.get("content")==null ||(StringUtils.isBlank(params.get("content").toString()))) {
                        code = "-5";
                        msg = "提问内容不能为空";
                    }else if (params.get("receiverMobile")==null ||(StringUtils.isBlank(params.get("receiverMobile").toString()))){
                        code = "-6";
                        msg = "卖家手机号不能为空";
                    }else{
                        //将提问内容content发送给卖家 发送短信 通过 更新数据
                        params.put("approveStatus",1);
                        questionAnswerMapper.updateByIdProblem(params);
                        code = Constants.SUCCESS;
                        msg = "成功，通过，发送短信，更新数据";
                    }
                }else {
                    //不发送短信 通过 更新数据
                    params.put("approveStatus",1);
                    questionAnswerMapper.updateByIdProblem(params);
                    code = Constants.SUCCESS;
                    msg = "成功，通过，不发送短信，更新数据";
                }
            }else if (Integer.parseInt(params.get("checkPassed").toString())==0){
                //不发送短信 不通过 更新数据
                params.put("approveStatus",4);
                questionAnswerMapper.updateByIdProblem(params);
                code = Constants.SUCCESS;
                msg = "成功，不通过，不发送短信，更新数据";
            }else {
                code = "-7";
                msg = "审核类型数据错误";
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    /**
     * 咨询列表》问题列表更新数据（回复）
     * @param params 参数map
     * @return
     */
    //缺发送短信
    @Override
    public Result updateProblem(Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(params.get("id") == null || StringUtils.isBlank(params.get("id").toString())){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                if(params.get("shortMessage")!= null && Integer.parseInt(params.get("shortMessage").toString())==1){
                    if(params.get("endContent")==null ||(StringUtils.isBlank(params.get("endContent").toString()))) {
                        code = "-4";
                        msg = "回复内容不能为空";
                    }else if (params.get("sendMobile")==null ||(StringUtils.isBlank(params.get("sendMobile").toString()))){
                        code = "-5";
                        msg = "买家手机号不能为空";
                    }else {
                        //发送短信 通过 更新数据
                        params.put("endDate",new Date());
                        params.put("approveStatus",2);
                        questionAnswerMapper.updateByIdProblem(params);
                        code = Constants.SUCCESS;
                        msg = "成功，通过，发送短信，更新数据";
                    }
                }else {
                    //不发送短信 通过 更新数据
                    params.put("endDate",new Date());
                    params.put("approveStatus",2);
                    questionAnswerMapper.updateByIdProblem(params);
                    code = Constants.SUCCESS;
                    msg = "成功，通过，不发送短信，更新数据";
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 咨询系统>>回复列表分页查询（完全匹配）
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @Override
    public Result pageListReply(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(params.get("type")!= null && Integer.parseInt(params.get("type").toString())==1){
                //将登录的用户名存入belongSalman中
                //params.put("belongSalman",username);
                System.out.println("type=1");
            }else{
                System.out.println("type=0");
            }
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> mapList = questionAnswerMapper.selectAllReply(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(mapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    /**
     * 咨询系统>>回复列表详细信息
     * @param id 参数id
     * @return
     */
    @Override
    public Result detailReply(Long id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(id == null){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                List<Map<String,Object>> list = questionAnswerMapper.selectByIdReply(id);
                result.setData(list);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    /**
     * 咨询系统>>回复列表更新数据
     * @param params 参数params
     * @return
     */
    //缺发送短信
    @Override
    public Result updateReply(Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(params.get("id") == null || StringUtils.isBlank(params.get("id").toString())){
                code = "-3";
                msg = "主键id不能为空";
            }else if(params.get("checkPassed")== null ||(StringUtils.isBlank(params.get("checkPassed").toString()))){
                code = "-4";
                msg = "审核类型不能为空";
            }else if (Integer.parseInt(params.get("checkPassed").toString())==1){
                if(params.get("shortMessage")!= null && Integer.parseInt(params.get("shortMessage").toString())==1){
                    if(params.get("endContent")==null ||(StringUtils.isBlank(params.get("endContent").toString()))) {
                        code = "-5";
                        msg = "回复内容不能为空";
                    }else if (params.get("sendMobile")==null ||(StringUtils.isBlank(params.get("sendMobile").toString()))){
                        code = "-6";
                        msg = "买家手机号不能为空";
                    }else {
                        //发送短信 通过 更新数据
                        params.put("approveStatus",3);
                        questionAnswerMapper.updateByIdReply(params);
                        code = Constants.SUCCESS;
                        msg = "成功，通过，发送短信，更新数据";
                    }
                }else {
                    //不发送短信 通过 更新数据
                    params.put("approveStatus",3);
                    questionAnswerMapper.updateByIdReply(params);
                    code = Constants.SUCCESS;
                    msg = "成功，通过，不发送短信，更新数据";
                }
            }else if (Integer.parseInt(params.get("checkPassed").toString())==0){
                //不发送短信 不通过 更新数据
                params.put("endContent",null);
                params.put("approveStatus",5);
                questionAnswerMapper.updateByIdReply(params);
                code = Constants.SUCCESS;
                msg = "成功，不通过，不发送短信，更新数据";
            }else {
                code = "-7";
                msg = "审核类型数据错误";
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
