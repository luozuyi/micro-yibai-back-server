package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.ActivityCoupon;
import com.yibai.mapper.ActivityCouponMapper;
import com.yibai.service.ActivityCouponService;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class ActivityCouponServiceImpl extends BaseServiceImpl<ActivityCoupon,Long> implements ActivityCouponService {
    @Autowired
    private ActivityCouponMapper activityCouponMapper;

    @Override
    public Result addActivityCoupon(ActivityCoupon activityCoupon){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            if(activityCoupon.getCouponValue()==null){
                code="-3";
                msg="优惠卷价值不能为空";
            }else if(StringUtils.isBlank(activityCoupon.getType())){
                code="-4";
                msg="优惠卷类型不能为空";
            }else if (StringUtils.isBlank(activityCoupon.getCategoryType())){
                code="-5";
                msg="可使用平台不能为空";
            }else if (StringUtils.isBlank(activityCoupon.getIsCanAdd())){
                code="-6";
                msg="是否可以叠加不能为空";
            }else if (activityCoupon.getCouponLeastPrice()==null){
                code="-7";
                msg="优惠券最低抵扣不能为空";
            }else if (StringUtils.isBlank(activityCoupon.getShopType())) {
                code = "-8";
                msg = "类目不能为空";
            }else if (StringUtils.isBlank(activityCoupon.getNote())) {
                code = "-9";
                msg = "规则说明不能为空";
            }else if (StringUtils.isBlank(activityCoupon.getStatus())) {
                code = "-10";
                msg = "状态不能为空";
            }else if (activityCoupon.getStartTime()==null){
                code = "-11";
                msg = "截止时间不能为空";
            }else if (activityCoupon.getEndTime()==null){
                code = "-12";
                msg = "生效时间不能为空";
            }else {
                activityCoupon.setCreateTime(new Date());
                activityCouponMapper.insert(activityCoupon);
                code = Constants.SUCCESS;
                msg = "成功";
            }
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result change(Long id,String status){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            ActivityCoupon activityCoupon = activityCouponMapper.findById(id);
            activityCoupon.setStatus(status);
            activityCouponMapper.update(activityCoupon);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result listScreen(Integer pageNum,Integer pageSize,  Map<String, Object> params){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> mapList = activityCouponMapper.selectAllBySelection(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(mapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
