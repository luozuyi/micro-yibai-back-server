package com.yibai.serviceImpl;

import com.yibai.entity.SalesPhonepoolrule;
import com.yibai.mapper.SalesPhonepoolruleMapper;
import com.yibai.service.SalesPhonepoolruleService;
import com.yibai.utils.Constants;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;

@Transactional
@Service
public class SalesPhonepoolruleServiceImpl extends BaseServiceImpl<SalesPhonepoolrule,Long> implements SalesPhonepoolruleService{
    @Autowired
    private SalesPhonepoolruleMapper salesPhonepoolruleMapper;
    @Override
    public Result updateById(Long id, Integer type, Integer num, Integer dateNum, Date time) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(id == null){
                code = "-3";
                msg = "主键id不能为空";
            }else{
                SalesPhonepoolrule salesPhonepoolrule = salesPhonepoolruleMapper.selectByPrimaryKey(id);
                if(salesPhonepoolrule == null){
                    code = "-4";
                    msg = "修改的对象不存在";
                }else{
                    salesPhonepoolrule.setType(type);
                    salesPhonepoolrule.setNum(num);
                    salesPhonepoolrule.setDateNum(dateNum);
                    salesPhonepoolrule.setTime(time);
                    salesPhonepoolruleMapper.updateByPrimaryKeySelective(salesPhonepoolrule);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
