package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.OrderCouponStatistics;
import com.yibai.mapper.OrderCouponStatisticsMapper;
import com.yibai.service.OrderCouponStatisticsService;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;
import java.util.Map;

@Transactional
@Service
public class OrderCouponStatisticsServiceImpl extends BaseServiceImpl<OrderCouponStatistics, Long> implements OrderCouponStatisticsService {
    @Autowired
    private OrderCouponStatisticsMapper orderCouponStatisticsMapper;


    public  Result listpage(Integer pageNum, Integer pageSize, Map<String, Object> params){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<Map<String, Object>> mapList = orderCouponStatisticsMapper.selectAllBySelection(params);
            PageInfo<Map<String, Object>> page = new PageInfo<>(mapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
