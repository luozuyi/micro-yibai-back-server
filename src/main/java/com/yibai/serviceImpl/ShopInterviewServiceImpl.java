package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.ShopInterview;
import com.yibai.mapper.ShopInterviewMapper;
import com.yibai.service.ShopInterviewService;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;

@Transactional
@Service
public class ShopInterviewServiceImpl implements ShopInterviewService {
    @Autowired
    private ShopInterviewMapper shopInterviewMapper;
    /**
     * 售后采访分页查询（完全匹配）
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param record 参数实体ShopInterview（）
     * @return
     */
    @Override
    public Result pageList(Integer pageNum, Integer pageSize, ShopInterview record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<ShopInterview> list = shopInterviewMapper.selectAll(record);
            PageInfo<ShopInterview> page = new PageInfo<>(list);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    /**
     * 售后采访每列详细信息
     * @param id 参数id
     * @return
     */
    @Override
    public Result detail(Long id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(id == null){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                ShopInterview shopInterview = shopInterviewMapper.selectById(id);
                result.setData(shopInterview);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    /**
     * 售后采访删除数据
     * @param id 参数id
     * @return
     */
    @Override
    public Result deleteById(Long id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(id == null){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                shopInterviewMapper.deleteById(id);
                code = Constants.SUCCESS;
                msg = "删除成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    /**
     * 售后采访更新数据
     * @param record 参数实体Qusetion
     * @return
     */
    @Override
    public Result update(ShopInterview record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(record.getId() == null){
                code = "-3";
                msg = "主键id不能为空";
            }else {
                shopInterviewMapper.updateByIdSelective(record);
                code = Constants.SUCCESS;
                msg = "更新成功";
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    /**
     * 售后采访新增数据
     * @param record 参数实体ShopInterview
     * @return
     */
    @Override
    public Result insertSelective(ShopInterview record) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(record.getTitle())){
                code = "-3";
                msg = "采访标题不能为空";
            }else if(StringUtils.isBlank(record.getProducttitle())){
                code = "-4";
                msg = "店铺名称不能为空";
            }if(record.getPrice() == null){
                code = "-5";
                msg = "价格不能为空";
            }if(record.getProp2() == null){
                code = "-6";
                msg = "信用等级不能为空";
            }if(record.getProp3() == null){
                code = "-7";
                msg = "详细信用等级不能为空";
            }if(record.getRank() == null){
                code = "-8";
                msg = "信用值不能为空";
            }if(record.getPraise() == null){
                code = "-9";
                msg = "好评率不能为空";
            }if(StringUtils.isBlank(record.getTag())){
                code = "-9";
                msg = "所属栏目不能为空";
            }else{
                record.setDate(new Date());
                shopInterviewMapper.insertSelective(record);
                code = Constants.SUCCESS;
                msg = "新增成功";
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    //缺少上传图片
    /*@Autowired
    private FastFileStorageClient fastFileStorageClient;
    @Override
    public Result addInterviewPhoto(MultipartFile file) {
        Result result = new Result();
        String msg = "初始化";
        String code = Constants.FAIL;
        try {
            if(file!=null && !file.isEmpty()){
                StorePath storePath = fastFileStorageClient.uploadFile(file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), null);
                String fileUrl = storePath.getFullPath();
                String[] fileName = fileUrl.split("/");
                String name= fileName[fileName.length-1];
                String prefix = name.substring(name.lastIndexOf(".") + 1);
                if (isImage(prefix)) {
                    ShopInterview shopInterview = new ShopInterview();
                    shopInterview.setId((long) 10);;
                    shopInterview.setTitleImg(storePath.getPath());
                    interviewMapper.insertSelective(shopInterview);
                    code = "0";
                    msg = "成功";
                } else {
                    code = "-3";
                    msg = "只能上传图片";
                }
            }else{
                code = "-4";
                msg = "请选择上传图片";
            }
        } catch (Exception e) {
            code = "-2";
            msg = "上传出错";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }*/
}
