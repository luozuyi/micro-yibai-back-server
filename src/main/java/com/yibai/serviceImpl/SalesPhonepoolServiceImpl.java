package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.*;
import com.yibai.esEntity.EsSalesMember;
import com.yibai.mapper.*;
import com.yibai.repository.EsSalesMemberReponsitory;
import com.yibai.service.SalesPhonepoolService;
import com.yibai.utils.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;

@Transactional
@Service
public class SalesPhonepoolServiceImpl extends BaseServiceImpl<SalesPhonepool,Long> implements SalesPhonepoolService{
    @Autowired
    private SalesPhonepoolMapper salesPhonepoolMapper;
    @Autowired
    private SalesPhonePoolRecordMapper salesPhonePoolRecordMapper;
    @Autowired
    private SalesPhonepoolruleMapper salesPhonepoolruleMapper;
    @Autowired
    private SalesMemberMapper salesMemberMapper;
    @Autowired
    private EsSalesMemberReponsitory esSalesMemberReponsitory;
    @Autowired
    private SalesAdminMapper salesAdminMapper;
    @Autowired
    private ShopAdminMapper shopAdminMapper;
    @Override
    public Result selectType1SalesPhonepool(Integer pageNum, Integer pageSize, Map<String, Object> params, String yibaiAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String username = JwtToken.getUsername(CommonUtil.getToken(yibaiAdminToken));
            System.out.println(username);
            params.put("leaderName",username);
            PageHelperNew.startPage(pageNum,pageSize);
            List<SalesPhonepool> salesPhonepools = salesPhonepoolMapper.selectAll(params);
            PageInfo<SalesPhonepool> page = new PageInfo<>(salesPhonepools);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result selectType2SalesPhonepool(Integer pageNum, Integer pageSize, Map<String, Object> params, String yibaiAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String username = JwtToken.getUsername(CommonUtil.getToken(yibaiAdminToken));
            System.out.println(username);
            params.put("saleName",username);
            PageHelperNew.startPage(pageNum,pageSize);
            List<SalesPhonepool> salesPhonepools = salesPhonepoolMapper.selectAll(params);
            PageInfo<SalesPhonepool> page = new PageInfo<>(salesPhonepools);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result creatSalesPhonePool() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            SalesPhonepool lastPool = salesPhonepoolMapper.selectByPrimaryKey(1L);
            if(lastPool != null){
                Calendar calendar = Calendar.getInstance();
                Date lastTime = lastPool.getTime();
                Date nowTime = new Date();
                calendar.setTime(lastTime);
                int lastDay = calendar.get(Calendar.DATE);
                calendar.setTime(nowTime);
                int nowDay = calendar.get(Calendar.DATE);
                if(lastDay == nowDay) {
                    code = "-3";
                    msg = "今天已经生成过电话池了";
                    result.setMsg(msg);
                    result.setCode(code);
                    return result;
                }
            }
            List<SalesPhonepool> poolList = salesPhonepoolMapper.selectAll(new HashMap<>());
            List<SalesPhonepool> noCmpList = new ArrayList<>();/*存放未备注电话池*/
            for(SalesPhonepool salesPhonepoolItem:poolList) {
                SalesPhonePoolRecord record = new SalesPhonePoolRecord();
                record.setMobile(salesPhonepoolItem.getMobile());
                record.setLeaderName(salesPhonepoolItem.getLeaderName());
                record.setSaleName(salesPhonepoolItem.getSaleName());
                record.setNote(salesPhonepoolItem.getNote());
                record.setNote(salesPhonepoolItem.getNote());
                record.setTime(salesPhonepoolItem.getTime());
                salesPhonePoolRecordMapper.insertSelective(record);
                salesPhonepoolItem.setDeleteFlag(2);
                salesPhonepoolMapper.updateByPrimaryKeySelective(salesPhonepoolItem);
                if(StringUtils.isBlank(salesPhonepoolItem.getNote())){
                    SalesPhonepool newObj = new SalesPhonepool();
                    newObj.setMobile(salesPhonepoolItem.getMobile());
                    newObj.setLeaderName(salesPhonepoolItem.getLeaderName());
                    newObj.setSaleName(salesPhonepoolItem.getSaleName());
                    newObj.setNote(salesPhonepoolItem.getNote());
                    newObj.setNote(salesPhonepoolItem.getNote());
                    newObj.setTime(salesPhonepoolItem.getTime());
                    newObj.setDeleteFlag(1);
                    noCmpList.add(newObj);
                }
            }
            SalesPhonepoolrule rule = salesPhonepoolruleMapper.selectByPrimaryKey(1L);
            Integer type = rule.getType();
            Integer num = rule.getNum();
            Integer dateNum = rule.getDateNum();
            Date time = rule.getTime();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, -dateNum);
            Date lastTime = c.getTime();
            Map<String,Object> params = new HashMap<>();
            params.put("type",type);
            params.put("num",num);
            params.put("time",time);
            params.put("lastTime",lastTime);
            List<SalesMember> saleList = salesMemberMapper.selectByRule(params);
            for (int i =0;i<saleList.size();i++) {
                SalesMember sbean = saleList.get(i);
                sbean.setLastJoinPoolTime(new Date());
                salesMemberMapper.updateByPrimaryKeySelective(sbean);
                //2018-1-29
                EsSalesMember esSalesMember = new EsSalesMember();
                esSalesMember.setId(sbean.getId());
                esSalesMember.setBelongSalman(sbean.getBelongSalesman());
                esSalesMember.setCheckCount(sbean.getCheckcount());
                esSalesMember.setCurrentLoginTime(sbean.getCurrentLoginTime());
                esSalesMember.setFreezeMoney(sbean.getFreezeMoney());
                esSalesMember.setIsOverTime(sbean.getIsovertime());
                esSalesMember.setLastJoinPoolTime(sbean.getLastJoinPoolTime());
                esSalesMember.setLeader(sbean.getLeader());
                esSalesMember.setLoginCount(sbean.getLoginCount());
                esSalesMember.setMemberAddress(sbean.getMemberAddress());
                esSalesMember.setMemberDemand(sbean.getMemberDemand());
                esSalesMember.setMemberName(sbean.getMemberName());
                esSalesMember.setMobile(sbean.getMobile());
                esSalesMember.setMoney(sbean.getMoney());
                esSalesMember.setNeedsales(sbean.getNeedsales());
                esSalesMember.setQq(sbean.getQq());
                esSalesMember.setQuickNote(sbean.getQuicknote());
                esSalesMember.setReg_classify(sbean.getRegClassify());
                esSalesMember.setReg_origin(sbean.getRegOrigin());
                esSalesMember.setShopCount(sbean.getShopCount());
                esSalesMember.setSort(sbean.getSort());
                esSalesMember.setStatus(sbean.getStatus());
                esSalesMember.setTbCheckCount(sbean.getTbcheckcount());
                esSalesMember.setVoucher(sbean.getVoucher());
                esSalesMemberReponsitory.save(esSalesMember);
                SalesPhonepool bean = salesPhonepoolMapper.selectByPrimaryKey((Long)(i+1L));
                if (bean == null) {
                    SalesPhonepool pool = new SalesPhonepool();
                    pool.setMobile(sbean.getMobile());
                    pool.setTime(new Date());
                    pool.setDeleteFlag(1);
                    salesPhonepoolMapper.insertSelective(pool);
                } else {
                    bean.setLeaderName(null);
                    bean.setMobile(sbean.getMobile());
                    bean.setNote(null);
                    bean.setSaleName(null);
                    bean.setTime(new Date());
                    bean.setDeleteFlag(1);
                    salesPhonepoolMapper.updateByPrimaryKeySelective(bean);
                }

            }
            for (int i =0;i<noCmpList.size();i++) {
                SalesPhonepool bean = salesPhonepoolMapper.selectByPrimaryKey((Long)(i+saleList.size()+1L));
                if (bean == null) {
                    SalesPhonepool newNoCmp = new SalesPhonepool();
                    newNoCmp.setMobile(noCmpList.get(i).getMobile());
                    newNoCmp.setLeaderName(noCmpList.get(i).getLeaderName());
                    newNoCmp.setSaleName(noCmpList.get(i).getSaleName());
                    newNoCmp.setTime(noCmpList.get(i).getTime());
                    newNoCmp.setDeleteFlag(1);
                    salesPhonepoolMapper.insertSelective(newNoCmp);
                }else{
                    bean.setLeaderName(noCmpList.get(i).getLeaderName());
                    bean.setMobile(noCmpList.get(i).getMobile());
                    bean.setNote(null);
                    bean.setSaleName(noCmpList.get(i).getSaleName());
                    bean.setTime(noCmpList.get(i).getTime());
                    bean.setDeleteFlag(1);
                    salesPhonepoolMapper.updateByPrimaryKeySelective(bean);
                }
            }
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result optionAlloc(Long[] list, Long leaderId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            for (Long poolId:list) {
                SalesPhonepool salesPhonePool = salesPhonepoolMapper.selectByPrimaryKey(poolId);
                ShopAdmin shopAdmin = shopAdminMapper.selectByPrimaryKey(leaderId);
                String leaderName = shopAdmin.getFirstname();
                salesPhonePool.setLeaderName(leaderName);
                salesPhonepoolMapper.updateByPrimaryKeySelective(salesPhonePool);
            }
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result allocPool(String[] list) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<SalesPhonepool> poolList = salesPhonepoolMapper.selectAllNoAllocList();
            int num = poolList.size()/list.length;
            for (int i=0;i<list.length;i++) {
                for(int j=i*num;j<((i+1==list.length)?poolList.size():((i+1)*num));j++) {
                    poolList.get(j).setLeaderName(list[i]);
                    salesPhonepoolMapper.updateByPrimaryKeySelective(poolList.get(j));
                }
            }
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
