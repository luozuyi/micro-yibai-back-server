package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.CompanyQqRecord;
import com.yibai.mapper.CompanyQqRecordMapper;
import com.yibai.service.CompanyQqRecordService;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class CompanyQqRecordServiceImpl extends BaseServiceImpl<CompanyQqRecord,Integer> implements CompanyQqRecordService {

    @Autowired
    private CompanyQqRecordMapper companyQqRecordMapper;
    @Override
    public Result pageList(CompanyQqRecord companyQqRecord, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<CompanyQqRecord> QqRecord = companyQqRecordMapper.selectAll(companyQqRecord);
            PageInfo page = new PageInfo(QqRecord);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
