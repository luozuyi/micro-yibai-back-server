package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.ShopVoucher;
import com.yibai.mapper.ShopVoucherMapper;
import com.yibai.service.ShopVoucherService;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class ShopVoucherServiceImpl extends BaseServiceImpl<ShopVoucher, Long> implements ShopVoucherService{

    @Autowired
    private ShopVoucherMapper dao;

    /**
     * 多条件筛选查询shopVoucher
     * @param shopVoucher
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result selectAllShopVoucher(ShopVoucher shopVoucher, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<ShopVoucher> shopVouchers = dao.selectAllShopVoucher(shopVoucher);
            PageInfo page = new PageInfo(shopVouchers);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
