package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.ShopIncome;
import com.yibai.mapper.ShopIncomeMapper;
import com.yibai.service.ShopIncomeService;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ShopIncomeServiceImpl extends BaseServiceImpl<ShopIncome,Long> implements ShopIncomeService {

    @Autowired
    private ShopIncomeMapper dao;

    /**
     * 多条件筛选查询收支明细表并分页
     * @param shopIncome
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result selectAllShopIncome(ShopIncome shopIncome, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<ShopIncome> shopIncomes = dao.selectAllShopIncome(shopIncome);
            PageInfo page = new PageInfo(shopIncomes);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
