package com.yibai.serviceImpl;

import com.yibai.entity.ShopMember;
import com.yibai.mapper.ShopMemberMapper;
import com.yibai.service.ShopMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ShopMemberServiceImpl extends BaseServiceImpl<ShopMember,Long> implements ShopMemberService{


}
