package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.*;
import com.yibai.mapper.*;
import com.yibai.service.ShopRemitRecordService;
import com.yibai.utils.CommonUtil;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
@Transactional
@Service
public class ShopRemitRecordServiceImpl extends BaseServiceImpl<ShopRemitRecord,Long> implements ShopRemitRecordService {

    @Autowired
    private ShopRemitRecordMapper dao;

    @Autowired
    private YibaidianAdminMapper yibaidianAdminMapper;

    @Autowired
    private ShopRechargeMapper shopRechargeMapper;

    @Autowired
    private ShopMemberMapper shopMemberMapper;

    @Autowired
    private ShopIncomeMapper shopIncomeMapper;

    /**
     * 多条件筛选查询线下汇款记录并分页
     * @param shopRemitRecord
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result selectAllRemitRecord(ShopRemitRecord shopRemitRecord,String yibaiAdminToken,Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            //通过token获取yibaidianAdminId
            String yibaidianAdminId = CommonUtil.getYibaidianAdminId(yibaiAdminToken);
            //通过yibaidianAdminId查询YibaidianAdmin对象
            YibaidianAdmin yibaidianAdmin = yibaidianAdminMapper.selectByPrimaryKey(yibaidianAdminId);
            if (yibaidianAdmin==null){
                return new Result(Constants.FAIL,"请登录之后再进行操作");
            }
            PageHelperNew.startPage(pageNum, pageSize);
            List<ShopRemitRecord> shopRemitRecords = dao.selectAllRemitRecord(shopRemitRecord);
            //根据状态信息和登录管理员id确认操作信息
            for (ShopRemitRecord remitRecord:
                 shopRemitRecords) {
                if (remitRecord.getStatus()==2){
                    remitRecord.setOperation("审核");
                } else if (remitRecord.getStatus()==1 && yibaidianAdmin.getCoreAdminId()==1){
                    remitRecord.setOperation("管理员审核");
                }else {
                    remitRecord.setOperation("无法操作");
                }
            }
            PageInfo page = new PageInfo(shopRemitRecords);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;

    }

    /**
     * 审核线下汇款时根据id查询对应信息
     * @param id
     * @return
     */
    @Override
    public Result selectByApprove(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            ShopRemitRecord shopRemitRecords = dao.selectByApprove(Long.parseLong(id));
            result.setData(shopRemitRecords);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 审核线下汇款
     * @param id
     * @param approve
     * @param yibaiAdminToken
     * @return
     */
    @Override
    public synchronized Result approveRemitRecord(String id,String approve,String yibaiAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        //根据id查询shopRemitRecord信息
        ShopRemitRecord shopRemitRecord = dao.selectByPrimaryKey(Long.parseLong(id));
        try {
            //通过token获取yibaidianAdminId
            String yibaidianAdminId = CommonUtil.getYibaidianAdminId(yibaiAdminToken);
            //通过yibaidianAdminId查询YibaidianAdmin对象
            YibaidianAdmin yibaidianAdmin = yibaidianAdminMapper.selectByPrimaryKey(yibaidianAdminId);
            if (yibaidianAdmin==null){
                return new Result(Constants.FAIL,"请登录之后再进行操作");
            }
            ShopRecharge shopRecharge = shopRechargeMapper.selectByPrimaryKey(shopRemitRecord.getRechargeId());
            //如果状态为0或者已有审核人，则表示已经审核
            if (shopRecharge.getStatus() == 0 || shopRemitRecord.getApproveMan() != null) {
                return new Result(Constants.FAIL,"该线下汇款已经审核");
            }
            //审核通过
            if(approve != null && "0".equals(approve)){
                ShopMember shopMember = shopMemberMapper.selectByPrimaryKey(shopRemitRecord.getMemberId());
                /* 收入详细表  */
                ShopIncome shopIncome = new ShopIncome();
                //1：线下汇款
                shopIncome.setFlunFlow(1);
                //0：收入 1：支出
                shopIncome.setFinanceType(0);
                // 0：代金券 1：RMB
                shopIncome.setMoneyType(1);
                shopIncome.setcTime(new Date());
                //金额
                shopIncome.setNum(BigDecimal.valueOf(shopRecharge.getRechargeMoney()));
                //插入收支明细
                shopIncomeMapper.insertSelective(shopIncome);
                //新增审核人
                shopRemitRecord.setApproveMan(yibaidianAdmin.getAdminName());
                //修改线下汇款审核人为当前登录管理员
                dao.updateByPrimaryKeySelective(shopRemitRecord);
                shopRecharge.setStatus(0);
                //审核不通过
            }else if (approve != null && "1".equals(approve)){
                shopRecharge.setStatus(1);
            }
            shopRechargeMapper.updateByPrimaryKeySelective(shopRecharge);
            code=Constants.SUCCESS;
            msg="操作成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
