package com.yibai.serviceImpl;

import com.yibai.entity.OrderCoupon;
import com.yibai.service.OrderCouponService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class OrderCouponServiceImpl extends BaseServiceImpl<OrderCoupon, Long> implements OrderCouponService {

}
