package com.yibai.serviceImpl;

import com.yibai.entity.ShopTag;
import com.yibai.mapper.ShopTagMapper;
import com.yibai.service.ShopTagService;
import com.yibai.utils.Constants;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class ShopTagServiceImpl implements ShopTagService {
    @Autowired
    private ShopTagMapper shopTagMapper;
    @Override
    public Result ListShopTag() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            System.out.println(msg);
            List<ShopTag> list = shopTagMapper.selectAll();
            result.setData(list);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
