package com.yibai.serviceImpl;

import com.yibai.entity.SalesComplaint;
import com.yibai.service.SalesComplaintService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class SalesComplaintServiceImpl extends BaseServiceImpl<SalesComplaint,Long> implements SalesComplaintService {
}
