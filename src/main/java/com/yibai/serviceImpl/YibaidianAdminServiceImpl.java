package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.SysRole;
import com.yibai.entity.YibaidianAdmin;
import com.yibai.mapper.CoreAdminMapper;
import com.yibai.mapper.SysRoleMapper;
import com.yibai.mapper.YibaidianAdminMapper;
import com.yibai.service.YibaidianAdminService;
import com.yibai.utils.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class YibaidianAdminServiceImpl extends BaseServiceImpl<YibaidianAdmin, String> implements YibaidianAdminService {
    @Autowired
    private YibaidianAdminMapper yibaidianAdminMapper;
    @Autowired
    private CoreAdminMapper coreAdminMapper;
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Override
    public Result insertList() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            //List<CoreAdmin> coreAdminList = coreAdminMapper.selectAll();
            List<Map<String,Object>> mapList = coreAdminMapper.selectAllMap();
            for (Map<String,Object> coreAdmin:mapList) {
                Long id = Long.valueOf(coreAdmin.get("adminId").toString());
                YibaidianAdmin yibaidianAdmin = yibaidianAdminMapper.selectByCoreAdminId(id);
                if(yibaidianAdmin == null){
                    yibaidianAdmin = new YibaidianAdmin();
                    yibaidianAdmin.setId(CommonUtil.getUUID());
                    yibaidianAdmin.setCoreAdminId(id);
                    yibaidianAdmin.setAdminName((String)coreAdmin.get("username"));
                    yibaidianAdmin.setPassword((String)coreAdmin.get("password"));
                    if((Boolean)coreAdmin.get("isDisabled")){
                        yibaidianAdmin.setDelFlag("1");
                    }else{
                        yibaidianAdmin.setDelFlag("0");
                    }
                    yibaidianAdmin.setCreateTime((Date)coreAdmin.get("createTime"));
                    yibaidianAdminMapper.insertSelective(yibaidianAdmin);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    @Override
    public Result addAdmin(String adminName, String password,String roleId,String isDisable,String surePassword) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(adminName)){
                code = "-3";
                msg = "管理员登陆名不能为空";
            }else if(StringUtils.isBlank(password)){
                code = "-4";
                msg = "密码不能为空";
            }else if(StringUtils.isBlank(surePassword)){
                code = "-5";
                msg = "确认密码不能为空";
            }else if(StringUtils.isBlank(isDisable)){
                code = "-6";
                msg = "选择是否禁用";
            }else if(!PatternUtil.patternString(password, "password")){
                code = "-7";
                msg = "密码格式不正确";
            }else if(!password.equals(surePassword)){
                code = "-8";
                msg = "两次密码不一致";
            }else if(!isDisable.equals(Constants.AdminIsDisable.DISABLE.getIsDisable()) && !isDisable.equals(Constants.AdminIsDisable.UNDISABLE.getIsDisable())){
                code = "-9";
                msg = "请正确选择是否禁用";
            }else{
                YibaidianAdmin yibaidianAdmin_db = yibaidianAdminMapper.selectByAdminName(adminName);
                if(yibaidianAdmin_db != null){
                    code = "-10";
                    msg = "已存在该管理员";
                }else{
                    if(StringUtils.isNotBlank(roleId)){
                        SysRole sysRole = sysRoleMapper.selectByPrimaryKey(roleId);
                        if(sysRole == null){
                            code = "-11";
                            msg = "要分配的角色不存在";
                        }else{
                            YibaidianAdmin yibaidianAdmin = new YibaidianAdmin();
                            yibaidianAdmin.setAdminName(adminName);
                            yibaidianAdmin.setCreateTime(new Date());
                            yibaidianAdmin.setDelFlag("0");
                            yibaidianAdmin.setPassword(password);
                            yibaidianAdmin.setId(CommonUtil.getUUID());
                            yibaidianAdmin.setSysRoleId(roleId);
                            yibaidianAdminMapper.insertSelective(yibaidianAdmin);
                            code = Constants.SUCCESS;
                            msg = "成功";
                        }
                    }else{
                        YibaidianAdmin yibaidianAdmin = new YibaidianAdmin();
                        yibaidianAdmin.setAdminName(adminName);
                        yibaidianAdmin.setCreateTime(new Date());
                        yibaidianAdmin.setDelFlag("0");
                        yibaidianAdmin.setPassword(password);
                        yibaidianAdmin.setId(CommonUtil.getUUID());
                        yibaidianAdminMapper.insertSelective(yibaidianAdmin);
                        code = Constants.SUCCESS;
                        msg = "成功";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageList(Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<YibaidianAdmin> mapList = yibaidianAdminMapper.selectAll();
            PageInfo page = new PageInfo(mapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result update(String adminId, String password, String roleId, String isDisable, String surePassword) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(adminId)){
                code = "-3";
                msg = "主键id不能为空";
            }else if(StringUtils.isBlank(password)){
                code = "-4";
                msg = "密码不能为空";
            }else if(StringUtils.isBlank(surePassword)){
                code = "-5";
                msg = "确认密码不能为空";
            }else if(StringUtils.isBlank(isDisable)){
                code = "-6";
                msg = "选择是否禁用";
            }else if(!PatternUtil.patternString(password, "password")){
                code = "-7";
                msg = "密码格式不正确";
            }else if(!password.equals(surePassword)){
                code = "-8";
                msg = "两次密码不一致";
            }else if(!isDisable.equals(Constants.AdminIsDisable.DISABLE.getIsDisable()) && !isDisable.equals(Constants.AdminIsDisable.UNDISABLE.getIsDisable())){
                code = "-9";
                msg = "请正确选择是否禁用";
            }else{
                YibaidianAdmin yibaidianAdmin_db = yibaidianAdminMapper.selectByPrimaryKey(adminId);
                yibaidianAdmin_db.setPassword(password);
                yibaidianAdmin_db.setSysRoleId(roleId);
                yibaidianAdmin_db.setDelFlag(isDisable);
                yibaidianAdminMapper.updateByPrimaryKeySelective(yibaidianAdmin_db);
                code = Constants.SUCCESS;
                msg = "修改成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
