package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.ShopRecharge;
import com.yibai.mapper.ShopRechargeMapper;
import com.yibai.service.ShopRechargeService;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ShopRechargeServiceImpl extends BaseServiceImpl<ShopRecharge,Long> implements ShopRechargeService {

    @Autowired
    private ShopRechargeMapper dao;

    /**
     * 多条件筛选网银支付记录并分页
     * @param shopRecharge
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result selectAllRecharge(ShopRecharge shopRecharge, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<ShopRecharge> shopRecharges = dao.selectAllRecharge(shopRecharge);
            PageInfo page = new PageInfo(shopRecharges);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
