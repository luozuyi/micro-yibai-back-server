package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.*;
import com.yibai.mapper.*;
import com.yibai.service.ShopMemberService;
import com.yibai.service.ShopWithdrawService;
import com.yibai.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Transactional
@Service
public class ShopWithdrawServiceImpl extends BaseServiceImpl<ShopWithdraw, Long> implements ShopWithdrawService {

    @Autowired
    private ShopWithdrawMapper dao;

    @Autowired
    private ShopMemberMapper shopMemberMapper;

    @Autowired
    private YibaidianAdminMapper yibaidianAdminMapper;

    @Autowired
    private ShopFreezeDetailMapper shopFreezeDetailMapper;

    @Autowired
    private ShopMemberService shopMemberService;

    @Autowired
    private ShopIncomeMapper shopIncomeMapper;

    /**
     * 多条件查询提现记录并分页
     * @param shopWithdraw
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result selectAllShopWithdraw(ShopWithdraw shopWithdraw, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<ShopWithdraw> shopWithdraws = dao.selectAllShopWithdraw(shopWithdraw);
            //根据status状态确定操作信息
            for (ShopWithdraw withdraw:
                 shopWithdraws) {
                if (withdraw.getStatus()==3){
                    if (withdraw.getFeeMoney()>0){
                        withdraw.setOperation(new String[]{"拒绝申请","接受申请","免手续费"});
                    }else {
                        withdraw.setOperation(new String[]{"拒绝申请","接受申请"});
                    }
                }else if (withdraw.getStatus()==2){
                    withdraw.setOperation(new String[]{"处理"});
                }else if (withdraw.getStatus()==4){
                    withdraw.setOperation(new String[]{withdraw.getNote()});
                }else if (withdraw.getStatus()==0||withdraw.getStatus()==1){
                    withdraw.setOperation(new String[]{"无法操作"});
                }
            }
            PageInfo page = new PageInfo(shopWithdraws);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 通过id查询提现记录
     * @param id
     * @return
     */
    @Override
    public Result selectShopWithdrawById(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            ShopWithdraw shopWithdraw = dao.selectShopWithdrawById(Long.parseLong(id));
            result.setData(shopWithdraw);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 接受申请，根据id修改status为2
     * @param id
     * @return
     */
    @Override
    public Result updateStatus(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            ShopWithdraw shopWithdraw = dao.selectByPrimaryKey(Long.parseLong(id));
            shopWithdraw.setStatus(2);
            dao.updateByPrimaryKeySelective(shopWithdraw);
            code = Constants.SUCCESS;
            msg = "修改成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 免手续费，通过id修改免手续费字段唯一标识为1
     * @param id
     * @return
     */
    @Override
    public Result updateFreeOfFee(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            ShopWithdraw shopWithdraw = dao.selectByPrimaryKey(Long.parseLong(id));
            shopWithdraw.setIsFreeFee("1");
            dao.updateByPrimaryKeySelective(shopWithdraw);
            code = Constants.SUCCESS;
            msg = "修改成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 拒绝提现
     * @param id
     * @param reason
     * @param yibaiAdminToken
     * @return
     */
    public Result refuseRequest(String id, String reason, String yibaiAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        //根据提现id查询对应的提现信息
        ShopWithdraw shopWithdraw = dao.selectByPrimaryKey(Long.parseLong(id));
        //新建一个note信息
        String refuseNote = "";
        //如果审核人不为空
        if (shopWithdraw.getApproveMan() != null) {
            return new Result(Constants.FAIL,"该提现已经处理");
        }
        try {
            //通过token获取yibaidianAdminId
            String yibaidianAdminId = CommonUtil.getYibaidianAdminId(yibaiAdminToken);
            //通过yibaidianAdminId查询YibaidianAdmin对象
            YibaidianAdmin yibaidianAdmin = yibaidianAdminMapper.selectByPrimaryKey(yibaidianAdminId);
            if (yibaidianAdmin==null){
                return new Result(Constants.FAIL,"请登录之后再进行操作");
            }
            //根据传回来的reason判断拒绝原因
            int reasonNum = Integer.parseInt(reason);
            if (reasonNum == 1) {
                refuseNote = "真实姓名有误";
            } else if (reasonNum == 2) {
                refuseNote = "支行信息有误";
            } else if (reasonNum == 3) {
                refuseNote = "银行账户有误";
            } else {
                refuseNote = "账户余额不足";
            }
            //拒绝申请提现需要修改如下四个地方
            //1 修改对应提现表的状态和审核人
            //2 解冻冻结明细
            //3 更新用户余额和冻结资金
            //4 写入收支明细
            withdrawSuccess(Long.parseLong(id),shopWithdraw.getMemberId(),"1",refuseNote,yibaidianAdmin);
            code = Constants.SUCCESS;
            msg = "操作成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 处理提现
     * @param withdrawId
     * @param memberId
     * @param result
     * @param yibaidianAdmin
     */
    public void withdrawSuccess(Long withdrawId, Long memberId, String result,String note, YibaidianAdmin yibaidianAdmin) {
        ShopWithdraw shopWithdraw = dao.selectByPrimaryKey(withdrawId);
        ShopMember shopMember = shopMemberMapper.selectByPrimaryKey(memberId);
        //处理成功
        if (result != null && "0".equals(result)) {
            shopWithdraw.setStatus(0);
            ShopIncome shopIncome = new ShopIncome();
            shopIncome.setMemberId(memberId);
            shopIncome.setcTime(new Date());
            // 0：代金券 1：RMB
            shopIncome.setMoneyType(1);
            //0：收入 1：支出
            shopIncome.setFinanceType(1);
            //12：提现
            shopIncome.setFlunFlow(12);
            shopIncome.setNum(BigDecimal.valueOf(shopWithdraw.getDrawMoney()));
            //新增收支明细
            shopIncomeMapper.insertSelective(shopIncome);
            ShopFreezeDetail detail = shopFreezeDetailMapper.selectByPrimaryKey(withdrawId);
            detail.setStatus(1);
            detail.setApproveAdminId(yibaidianAdmin.getCoreAdminId());
            detail.setApproveTime(new Date());
            //解冻冻结明细
            shopFreezeDetailMapper.updateByPrimaryKeySelective(detail);
            ShopMember member = shopMemberMapper.selectByPrimaryKey(memberId);
            //提现审核成功从冻结资金中减去提现金额
            member.setFreezeMoney(member.getFreezeMoney() - shopWithdraw.getDrawMoney());
            shopMemberMapper.updateByPrimaryKeySelective(member);
            //处理失败
        } else if (result != null && "1".equals(result)) {
            ShopFreezeDetail detail = shopFreezeDetailMapper.selectByWithdrawId(withdrawId);
            detail.setStatus(1);
            detail.setApproveAdminId(yibaidianAdmin.getCoreAdminId());
            detail.setApproveTime(new Date());
            //解冻冻结明细
            shopFreezeDetailMapper.updateByPrimaryKeySelective(detail);
            // 提现审核失败更新用户余额和冻结资金
            updateForWithdrawFail(shopMember.getMemberId(), shopWithdraw.getDrawMoney());
            //将提现状态设置为1
            if (shopWithdraw.getStatus() != 4) {
                shopWithdraw.setStatus(1);
            }
            //加入备注
            shopWithdraw.setNote(note);
            /* 收入详细表  */
            // 0：代金券 1：RMB   0：收入 1：支出  	12：提现 new Date(), shopMember, 1, 0, 12, (detail.getFreeMoney()
            ShopIncome income = new ShopIncome();
            income.setMemberId(memberId);
            income.setcTime(new Date());
            // 0：代金券 1：RMB
            income.setMoneyType(1);
            //0：收入 1：支出
            income.setFinanceType(0);
            //12：提现
            income.setFlunFlow(12);
            income.setNum(BigDecimal.valueOf(shopWithdraw.getDrawMoney()));
            // 写入收支明细
            shopIncomeMapper.insertSelective(income);
        }
        shopWithdraw.setApproveMan(yibaidianAdmin.getAdminName());
        //更新提现记录
        dao.updateByPrimaryKeySelective(shopWithdraw);
    }

    /**
     * 审核提现处理
     * @param id
     * @param reason
     * @param yibaiAdminToken
     * @return
     */
    @Override
    public Result approveWithdraw(String id,String reason,String yibaiAdminToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        //根据提现id查询对应的提现信息
        ShopWithdraw shopWithdraw = dao.selectByPrimaryKey(Long.parseLong(id));
        try {
            //通过token获取yibaidianAdminId
            String yibaidianAdminId = CommonUtil.getYibaidianAdminId(yibaiAdminToken);
            //通过yibaidianAdminId查询YibaidianAdmin对象
            YibaidianAdmin yibaidianAdmin = yibaidianAdminMapper.selectByPrimaryKey(yibaidianAdminId);
            if (yibaidianAdmin==null){
                return new Result(Constants.FAIL,"请登录之后再进行操作");
            }
            //根据审核结果进行对应的操作
            withdrawSuccess(shopWithdraw.getId(),shopWithdraw.getMemberId(),reason,null,yibaidianAdmin);
            code=Constants.SUCCESS;
            msg="操作成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    /**
     * 提现审核失败账户余额和冻结金额的update
     * @param id
     * @param freeMoney
     */
    public void updateForWithdrawFail(Long id, double freeMoney){
        if (id==null){
            return;
        }
        //根据会员id查询账户余额
        ShopMember shopMember = shopMemberMapper.selectByPrimaryKey(id);
        //如果余额为空，则设置余额为0.0
        if (shopMember.getMoney()==null){
            shopMember.setMoney(0.0);
        }
        //账户余额加上冻结余额
        shopMember.setMoney(shopMember.getMoney()+freeMoney);
        if (shopMember.getFreezeMoney() == null){
            shopMember.setFreezeMoney(0.0);
        }
        //账户冻结余额减去冻结余额
        shopMember.setFreezeMoney(shopMember.getFreezeMoney()-freeMoney);
        shopMemberMapper.updateByPrimaryKeySelective(shopMember);
    }
}
