package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.SalesMember;
import com.yibai.entity.SalesPhonepool;
import com.yibai.mapper.SalesMemberMapper;
import com.yibai.service.SalesMemberService;
import com.yibai.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;
import java.util.Map;

@Transactional
@Service
public class SalesMemberServiceImpl extends BaseServiceImpl<SalesMember,Long> implements SalesMemberService{
    @Autowired
    private SalesMemberMapper salesMemberMapper;
    @Override
    public Result selectByBelongSalesmanSelection(Integer pageNum, Integer pageSize, Map<String, Object> params, String yibaiAdminToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String username = JwtToken.getUsername(CommonUtil.getToken(yibaiAdminToken));
            params.put("belongSalesman",username);
            PageHelperNew.startPage(pageNum,pageSize);
            List<SalesMember> salesMembers = salesMemberMapper.selectByBelongSalesmanSelection(params);
            PageInfo<SalesMember> page = new PageInfo<>(salesMembers);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
