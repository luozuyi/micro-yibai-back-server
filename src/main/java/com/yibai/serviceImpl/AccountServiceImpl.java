package com.yibai.serviceImpl;

import com.github.pagehelper.PageInfo;
import com.yibai.entity.Account;
import com.yibai.mapper.AccountMapper;
import com.yibai.service.AccountService;
import com.yibai.utils.Constants;
import com.yibai.utils.PageHelperNew;
import com.yibai.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;

@Transactional
@Service
public class AccountServiceImpl extends BaseServiceImpl<Account,Integer> implements AccountService{

    @Autowired
    private AccountMapper accountMapper;
    @Override
   public Result pageList(Account account,Integer pageNum, Integer pageSize){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<Account> accounts = accountMapper.selectAccountAll(account);
            PageInfo page = new PageInfo(accounts);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result addAccount(Account account){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            if(StringUtils.isBlank(account.getAccount())){
                code ="-3";
                msg = "账号不能为空";
            }else if(StringUtils.isBlank(account.getPassword())){
                code ="-4";
                msg ="密码不能为空";
            }else if(StringUtils.isBlank(account.getFromplatform())){
                code ="-5";
                msg ="账号平台不能为空";
            }else if(StringUtils.isBlank(account.getAccreditationperson())){
                code="-6";
                msg ="认证人不能为空";
            }else if(StringUtils.isBlank(account.getIdcard())){
                code ="-7";
                msg ="身份证不能为空";
            }else if(StringUtils.isBlank(account.getEmail())){
                code ="-8";
                msg="邮箱不能为空";
            }else if(StringUtils.isBlank(account.getTel())){
                code ="-9";
                msg ="注册手机号不能为空";
            }else if(StringUtils.isBlank(account.getAccountrole())){
                code ="-10";
                msg ="账号作用不能为空";
            }else {
                accountMapper.insert(account);
                code = Constants.SUCCESS;
                msg = "成功";
            }
            }catch (Exception e){
                code = Constants.ERROR;
                msg = "后台繁忙";
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                e.printStackTrace();
            }
            result.setCode(code);
            result.setMsg(msg);
            return result;
    }


    @Override
    public Result updateAccount(Integer id,Account account){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(null == id){
                code ="-3";
                msg ="主键id不能为空";
            }else {
                Account account1 = accountMapper.selectByPrimaryKey(id);
                if (account1 == null) {
                    code = "-4";
                    msg = "修改对象不存在";
                } else {
                    accountMapper.updateByPrimaryKeySelective(account1);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


}
