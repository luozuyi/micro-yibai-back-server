package com.yibai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class MicroYibaiBackServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroYibaiBackServerApplication.class, args);
	}
}
