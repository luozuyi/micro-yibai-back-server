package com.yibai.controller;

import com.yibai.entity.ShopAlipay;
import com.yibai.entity.ShopRecharge;
import com.yibai.service.ShopAlipayService;
import com.yibai.utils.Constants;
import com.yibai.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.ConsoleHandler;

@RestController
public class ShopAlipayController {

    @Autowired
    private ShopAlipayService service;

    /**
     * 分页查询支付宝充值记录列表
     * memberPhone  会员电话
     * payNo  交易号
     * payName  支付宝名称
     * beSalman  所属销售员
     * startNum 初始金额
     * endNum   终止金额
     * startCTime  初始操作时间
     * endCTime    终止操作时间
     * status 状态
     * @return
     */
    @GetMapping(value = "v1/auth/shop-alipays/pagination")
    public Result pageLists(ShopAlipay shopAlipay,@CookieValue String yibaiAdminToken,Integer pageNum, Integer pageSize) {
        return service.selectAllShopAlipay(shopAlipay,yibaiAdminToken,pageNum,pageSize);
    }


    /**
     * 根据id查询支付宝充值对象
     * @param id
     * @return
     */
    @GetMapping(value = "v1/auth/shop-alipays/alipay-info")
    public Result getApproveInfo(String id){
        if (StringUtils.isEmpty(id)){
            return new Result(Constants.FAIL,"id不能为空");
        }
        return service.selectShopAlipayById(id);
    }

    /**
     * 审核修改对应的状态
     * @param id
     * @param approve
     * @param yibaiAdminToken
     * @return
     */
    @PatchMapping(value = "v1/auth/shop-alipays/alipay-approval")
    public Result updateStatus(String id,String approve,@CookieValue String yibaiAdminToken){
            if (StringUtils.isEmpty(id)){
                return new Result(Constants.FAIL,"id不能为空");
            }
            if (StringUtils.isEmpty(approve)){
                return new Result(Constants.FAIL,"审核结果不能为空");
            }
        return service.approve(id,approve,yibaiAdminToken);
    }
}
