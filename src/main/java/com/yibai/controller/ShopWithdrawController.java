package com.yibai.controller;

import com.yibai.entity.ShopWithdraw;
import com.yibai.service.ShopWithdrawService;
import com.yibai.utils.Constants;
import com.yibai.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShopWithdrawController {

    @Autowired
    private ShopWithdrawService service;

    /**
     * 分页查询提现记录列表
     * memberPhone  会员电话
     * status 状态
     * startNum 初始金额
     * endNum   终止金额
     * @return
     */
    @GetMapping(value = "v1/auth/shop-withdraws/pagination")
    public Result pageLists(ShopWithdraw shopWithdraw, Integer pageNum, Integer pageSize) {
        return service.selectAllShopWithdraw(shopWithdraw,pageNum,pageSize);
    }

    /**
     * 接受申请，根据提现id查询提现信息
     * @param id
     * @return
     */
    @GetMapping(value = "v1/auth/shop-withdraws/withdraw-info")
    public Result acceptRequest(String id) {
        if (StringUtils.isEmpty(id)){
            return new Result(Constants.FAIL,"提现id不能为空");
        }
        return service.selectShopWithdrawById(id);
    }

    /**
     * 接受申请，确认之后修改对应的状态为2，处理中
     * @param id
     * @return
     */
    @PatchMapping(value = "v1/auth/shop-withdraws/withdraw-acceptance")
    public Result changeStatusDeal(String id){
        if (StringUtils.isEmpty(id)){
            return new Result(Constants.FAIL,"提现id不能为空");
        }
        return service.updateStatus(id);
    }

    /**
     * 拒绝申请
     * @param id
     * @param reason
     * @param yibaiAdminToken
     * @return
     */
    @PatchMapping(value = "v1/auth/shop-withdraws/withdraw-abnegation")
    public synchronized Result refuseRequest(String id,String reason,@CookieValue String yibaiAdminToken){
        if (StringUtils.isEmpty(id)){
            return new Result(Constants.FAIL,"提现id不能为空");
        }
        if (StringUtils.isEmpty(reason)){
            return new Result(Constants.FAIL,"拒绝原因不能为空");
        }
        return service.refuseRequest(id,reason,yibaiAdminToken);
    }

    /**
     * 免手续费
     * @param id
     * @return
     */
    @PatchMapping(value = "v1/auth/shop-withdraws/withdraw-freeOfFee")
    public synchronized Result freeOfFee(String id){
        if (StringUtils.isEmpty(id)){
            return new Result(Constants.FAIL,"提现id不能为空");
        }
        return service.updateFreeOfFee(id);
    }

    /**
     * 审核提现操作
     * @param id
     * @param reason
     * @param yibaiAdminToken
     * @return
     */
    @PatchMapping(value = "v1/auth/shop-withdraws/withdraw-approval")
    public synchronized Result approveWithdraw(String id,String reason,@CookieValue String yibaiAdminToken){
        if (StringUtils.isEmpty(id)){
            return new Result(Constants.FAIL,"提现id不能为空");
        }
        if (StringUtils.isEmpty(reason)){
            return new Result(Constants.FAIL,"审核结果不能为空");
        }
        return service.approveWithdraw(id,reason,yibaiAdminToken);
    }
}
