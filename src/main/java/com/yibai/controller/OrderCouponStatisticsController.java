package com.yibai.controller;

import com.yibai.service.OrderCouponStatisticsService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class OrderCouponStatisticsController {
    @Autowired
    private OrderCouponStatisticsService orderCouponStatisticsService;

    /**
     * 分页查询条件查询活动使用优惠券数量列表
     *
     * @param pageNum  当前页
     * @param pageSize 一页显示多少条
     * @param params   参数map
     * @return
     */
    @GetMapping(value = "v1/auth/order-coupon-statistics/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return orderCouponStatisticsService.listpage(pageNum, pageSize, params);
    }
    
}
