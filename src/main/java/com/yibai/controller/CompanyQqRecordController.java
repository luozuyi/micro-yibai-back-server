package com.yibai.controller;

import com.yibai.entity.CompanyQqRecord;
import com.yibai.service.CompanyQqRecordService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CompanyQqRecordController {

    @Autowired
    private CompanyQqRecordService companyQqRecordService;

    @GetMapping(value = "v1/auth/company-qq-records/pagination")
    public Result pageList(CompanyQqRecord companyQqRecord, Integer pageNum, Integer pageSize) {
        return companyQqRecordService.pageList(companyQqRecord, pageNum, pageSize);
    }
}
