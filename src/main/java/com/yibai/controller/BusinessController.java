package com.yibai.controller;


import com.yibai.entity.Business;
import com.yibai.service.BusinessService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BusinessController {

    @Autowired
    private BusinessService businessService;

    @GetMapping(value = "v1/auth/businesses/pagination")
    public Result pageList(Business business, Integer pageNum, Integer pageSize){
        return businessService.pageList(business,pageNum,pageSize);
    }

    @GetMapping(value = "v1/auth/business/id")
    public Result findById(Long id){
        return businessService.findBusinessById(id);
    }

}
