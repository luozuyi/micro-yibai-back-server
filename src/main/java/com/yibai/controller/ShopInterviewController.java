package com.yibai.controller;

import com.yibai.entity.ShopInterview;
import com.yibai.service.ShopInterviewService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShopInterviewController {
    @Autowired
    private ShopInterviewService shopInterviewService;
    /**
     * 售后采访分页查询（完全匹配）
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param record 参数实体ShopInterview
     * @return
     */
    @GetMapping(value = "v1/auth/shop-interview/paginations")
    public Result pageList(Integer pageNum, Integer pageSize, ShopInterview record) {
        return shopInterviewService.pageList(pageNum,pageSize,record);
    }
    /**
     * 售后采访每列详细信息
     * @param id 参数id
     * @return
     */
    @GetMapping(value = "v1/auth/shop-interview/details")
    public Result details(Long id) {
        return shopInterviewService.detail(id);
    }
    /**
     * 售后采访删除数据
     * @param id 参数id
     * @return
     */
    @DeleteMapping(value = "v1/auth/shop-interview/ids")
    public Result delete(Long id) {
        return shopInterviewService.deleteById(id);
    }
    /**
     * 售后采访更新数据
     * @param record 参数实体Qusetion
     * @return
     */
    //缺图片上传暂时没有搭建服务器存储静态资源
    //content字段对应的BLOB数据没有做渲染，后面存储时可能会有其他处理
    @PatchMapping(value = "v1/auth/shop-interview/contents")
    public Result update(ShopInterview record) {
        return shopInterviewService.update(record);
    }
    /**
     * 售后采访新增数据
     * @param record 参数实体ShopInterview
     * @return
     */
    //缺图片上传暂时没有搭建服务器存储静态资源
    //content字段对应的BLOB数据没有做渲染，后面存储时可能会有其他处理
    @PostMapping(value = "v1/auth/shop-interview/informations")
    public Result insertSelective(ShopInterview record) {
        return shopInterviewService.insertSelective(record);
    }
}
