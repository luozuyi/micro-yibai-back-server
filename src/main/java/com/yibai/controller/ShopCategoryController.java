package com.yibai.controller;

import com.yibai.service.ShopCategoryService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShopCategoryController {
    @Autowired
    private ShopCategoryService shopCategoryService;
    /**
     *²éÑ¯ËùÓÐÉÌÆÌµÄÀàÐÍ
     * @return
     */
    @GetMapping(value = "v1/auth/shop-category/category-paginations")
    public Result listShopCategory(){
        return shopCategoryService.ListShopCategory();
    }
}
