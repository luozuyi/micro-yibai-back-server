package com.yibai.controller;

import com.yibai.service.ArtificialService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class ArtificialController {
    @Autowired
    private ArtificialService artificialService;

    /**
     * 带条件分页查询找店工具搜集
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    @GetMapping(value = "v1/auth/artificials/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return artificialService.pageList(pageNum,pageSize,params);
    }

    /**
     * 主键查询找店工具搜集
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/artificials/detail")
    public Result detail(Long id) {
        return artificialService.selectByPrimaryKey(id);
    }

    /**
     * 修改备注以及一些信息
     * @param id 主键id
     * @param remarks 备注
     * @param isDeal 是否达成成交
     * @param dealShopUrl 成交店铺名或链接
     * @return
     */
    @PatchMapping(value = "v1/auth/artificials")
    public Result updateNote(Long id, String remarks, String isDeal, String dealShopUrl) {
        return artificialService.updateNote(id,remarks,isDeal,dealShopUrl);
    }

    /**
     *销售抢客户
     * @param id 主键id
     * @param yibaiAdminToken
     * @return
     */
    @PostMapping(value = "v1/auth/artificials/impack-my-member")
    public Result impackMyMember(Long id,@CookieValue String yibaiAdminToken) {
        return artificialService.impackMyMember(id,yibaiAdminToken);
    }
}
