package com.yibai.controller;

import com.yibai.entity.ShopIncome;
import com.yibai.entity.ShopRecharge;
import com.yibai.service.ShopRechargeService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShopRechargeController {

    @Autowired
    private ShopRechargeService service;

    /**
     * 分页查询网银支付记录列表
     * memberPhone  会员电话
     * orderCode  订单编号
     * serialNumber  交易号
     * startNum 初始金额
     * endNum   终止金额
     * StartCTime  初始操作时间
     * endCTime    终止操作时间
     * status 状态
     * @return
     */
    @GetMapping(value = "v1/auth/shop-recharges/pagination")
    public Result pageLists(ShopRecharge shopRecharge, Integer pageNum, Integer pageSize) {
        return service.selectAllRecharge(shopRecharge,pageNum,pageSize);
    }
}
