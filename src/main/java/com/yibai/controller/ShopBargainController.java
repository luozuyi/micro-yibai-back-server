package com.yibai.controller;

import com.yibai.service.ShopBargainService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class ShopBargainController {
    @Autowired
    private ShopBargainService shopBargainService;

    /**
     * 分页带条件查询砍价列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 查询条件
     * @return
     */
    @GetMapping(value = "v1/auth/shop-bargains/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return shopBargainService.pageList(pageNum,pageSize,params);
    }

    /**
     * 查询详情
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/shop-bargains/detail")
    public Result detail(Integer id) {
        return shopBargainService.selectMapByPrimaryKey(id);
    }

    /**
     * 修改备注
     * @param id 主键id
     * @param remarks 备注信息
     * @return
     */
    @PatchMapping(value = "v1/auth/shop-bargains")
    public Result updateNote(Integer id, String remarks) {
        return shopBargainService.updateNote(id,remarks);
    }

    /**
     * 销售员抢信息
     * @param id 主键id
     * @param yibaiAdminToken 凭据
     * @return
     */
    @PostMapping(value = "v1/auth/shop-bargains/impack-my-member")
    public Result impackMyMember(Integer id, @CookieValue String yibaiAdminToken){
        return shopBargainService.impackMyMember(id,yibaiAdminToken);
    }
}
