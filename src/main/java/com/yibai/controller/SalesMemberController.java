package com.yibai.controller;

import com.yibai.service.SalesMemberService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class SalesMemberController {
    @Autowired
    private SalesMemberService salesMemberService;

    /**
     * 分页查询我的客户
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @param yibaiAdminToken 凭据
     * @return
     */
    @GetMapping(value = "v1/auth/sales-members/pagination")
    public Result selectByBelongSalesmanSelection(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params, @CookieValue String yibaiAdminToken) {
        return salesMemberService.selectByBelongSalesmanSelection(pageNum,pageSize,params,yibaiAdminToken);
    }
}