package com.yibai.controller;

import com.yibai.entity.ActivityCoupon;
import com.yibai.service.ActivityCouponService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class ActivityCouponController {
    @Autowired
    private ActivityCouponService activityCouponService;

    /**
     * 添加优惠卷活动
     *
     * @param activityCoupon 优惠卷对象
     * @return
     */
    @PostMapping(value = "v1/auth/activity-coupons")
    public Result addActivityCoupon(ActivityCoupon activityCoupon) {
        return activityCouponService.addActivityCoupon(activityCoupon);
    }

    /**
     * 开启/关闭活动
     *
     * @param id     修改主键
     * @param status 修改状态
     * @return
     */
    @PutMapping(value = "v1/auth/activity-coupons")
    public Result change(Long id, String status) {
        return activityCouponService.change(id, status);
    }

    /**
     * 分页查询条件查询活动列表
     *
     * @param pageNum  当前页
     * @param pageSize 一页显示多少条
     * @param params   参数map
     * @return
     */
    @GetMapping(value = "v1/auth/activity-coupons/pagination")
    public Result listScreen(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return activityCouponService.listScreen(pageNum, pageSize, params);
    }
}
