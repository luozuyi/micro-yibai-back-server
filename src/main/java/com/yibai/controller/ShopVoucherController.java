package com.yibai.controller;

import com.yibai.entity.ShopVoucher;
import com.yibai.service.ShopVoucherService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShopVoucherController {

    @Autowired
    private ShopVoucherService service;

    /**
     * 分页查询代金券变更记录列表
     * memberName  会员名字
     * useStatus 代金券状态：0，购买看店卡 1，交易支付 2，交易终止返还 3，交易违约扣除  4，管理员调整 5，系统促销赠送
     * startNum 初始金额
     * endNum   终止金额
     * startCTime   初始查询时间
     * endCTime   终止查询时间
     * @return
     */
    @GetMapping(value = "v1/auth/shop-vouchers/pagination")
    public Result pageLists(ShopVoucher shopVoucher, Integer pageNum, Integer pageSize) {
        return service.selectAllShopVoucher(shopVoucher,pageNum,pageSize);
    }
}
