package com.yibai.controller;

import com.yibai.service.OrderCouponService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class OrderCouponController {
    @Autowired
    private OrderCouponService orderCouponService;

    /**
     * 分页查询条件查询优惠券使用记录列表
     *
     * @param pageNum  当前页
     * @param pageSize 一页显示多少条
     * @param params   参数map
     * @return
     */
    @GetMapping(value = "v1/auth/order-coupons/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return orderCouponService.pageList(pageNum, pageSize, params);
    }
}
