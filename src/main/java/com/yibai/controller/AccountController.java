package com.yibai.controller;

import com.yibai.entity.Account;
import com.yibai.service.AccountService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    /**
     * 分页查询账户管理列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping(value = "v1/auth/accounts/pagination")
    public Result pageList(Account account,Integer pageNum, Integer pageSize){
        return accountService.pageList(account,pageNum,pageSize);
    }

    /**
     * 添加账号信息
     * @param account
     * @return
     */
    @PostMapping(value = "v1/auth/account")
    public Result add(Account account){
        return accountService.addAccount(account);
    }

    /**
     * 修改账号信息
     * @param account
     * @return
     */
    @PutMapping(value = "v1/auth/accounts")
    public Result update(Integer id,Account account){
        return accountService.updateAccount(id,account);
    }

}
