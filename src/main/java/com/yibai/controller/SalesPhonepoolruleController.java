package com.yibai.controller;

import com.yibai.entity.SalesPhonepoolrule;
import com.yibai.service.SalesPhonepoolruleService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class SalesPhonepoolruleController {
    @Autowired
    private SalesPhonepoolruleService salesPhonepoolruleService;

    /**
     *分页查询电话池规则
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/sales-phonepoolrules/pagination")
    public Result pageList(Integer pageNum, Integer pageSize) {
        return salesPhonepoolruleService.pageList(pageNum,pageSize);
    }

    /**
     * 添加电话池规则
     * @param salesPhonepoolrule 电话池对象
     * @param bangResult 绑定对象
     * @return
     */
    @PostMapping(value = "v1/auth/sales-phonepoolrules")
    public Result add(@Validated SalesPhonepoolrule salesPhonepoolrule, BindingResult bangResult) {
        return salesPhonepoolruleService.insertSelective(salesPhonepoolrule, bangResult);
    }

    /**
     * 查询电话池规则详情
     * @param id
     * @return
     */
    @GetMapping(value = "v1/auth/sales-phonepoolrules")
    public Result detail(Long id) {
        return salesPhonepoolruleService.selectByPrimaryKey(id);
    }

    /**
     * 主键修改电话池规则
     * @param id 主键id
     * @param type 类型
     * @param num 数字
     * @param dateNum 天数
     * @param time 时间
     * @return
     */
    @PatchMapping(value = "v1/auth/sales-phonepoolrules/id")
    public Result updateById(Long id, Integer type, Integer num, Integer dateNum, Date time) {
        return salesPhonepoolruleService.updateById(id, type, num, dateNum, time);
    }
}
