package com.yibai.controller;

import com.yibai.entity.ShopMabnormal;
import com.yibai.service.ShopMabnormalService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShopMabnormalController {

    @Autowired
    private ShopMabnormalService service;

    /**
     * 分页查询会员资金调整记录列表
     * memberPhone  会员电话
     * adminName  管理员
     * type  收支类型 0 支出 1: 收入
     * fundType  资金类型: 0 钱  1 冻结资金 2 代金券
     * startNum 初始金额
     * endNum   终止金额
     * StartCTime  初始操作时间
     * endCTime    终止操作时间
     * @return
     */
    @GetMapping(value = "v1/auth/shop-mabnormals/pagination")
    public Result pageLists(ShopMabnormal shopMabnormal, Integer pageNum, Integer pageSize) {
        return service.selectAllShopMabnormal(shopMabnormal,pageNum,pageSize);
    }
}
