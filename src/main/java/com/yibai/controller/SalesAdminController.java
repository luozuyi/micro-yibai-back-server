package com.yibai.controller;

import com.yibai.service.SalesAdminService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class SalesAdminController {
    @Autowired
    private SalesAdminService salesAdminService;
    /**
     * 条件分页查询销售列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/sales-admins/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return salesAdminService.pageList(pageNum,pageSize,params);
    }

    /**
     * 查询组长列表
     * @return
     */
    @GetMapping(value = "v1/auth/sales-admins/leader-list")
    public Result selectLeaderList() {
        return salesAdminService.selectLeaderList();
    }

    /**
     * 查询部长列表
     * @return
     */
    @GetMapping(value = "v1/auth/sales-admins/minister-list")
    public Result selectMinisterList() {
        return salesAdminService.selectMinisterList();
    }

    /**
     * 分配组员给组长
     * @param list 组员id集合
     * @param leaderId 组长id
     * @return
     */
    @PostMapping(value = "v1/auth/sales-admins/add-batch")
    public Result addBatch(@RequestParam(value = "list[]") Long[] list, Integer leaderId) {
        return salesAdminService.addBatch(list,leaderId);
    }

    /**
     * 分配部长
     * @param list 组员id集合
     * @param minister 部长id
     * @return
     */
    @PostMapping(value = "v1/auth/sales-admins/depart-member")
    public Result addDepartMember(@RequestParam(value = "list[]") Long[] list, Integer minister){
        return salesAdminService.addDepartMember(list,minister);
    }

    /**
     * 主键查询销售详情
     * @param id
     * @return
     */
    @GetMapping(value = "v1/auth/sales-admins/id")
    public Result detail(Long id){
        return salesAdminService.selectMapByPrimaryKey(id);
    }

    /**
     * 调整数量
     * @param id 主键id
     * @param num 调整后的数量
     * @param level 销售级别 0：初级  1：中级 2:高级
     * @param limitnum 每日限制数量【暂时未用】
     * @return
     */
    @PatchMapping(value = "v1/auth/sales-admins/id")
    public Result numUpdate(Long id, Integer num, Integer level, Integer limitnum) {
        return salesAdminService.numUpdate(id, num, level, limitnum);
    }

    /**
     * 批量调整个人月目标
     * @param list 主键集合
     * @param mgoal 个人月目标
     * @return
     */
    @PatchMapping(value = "v1/auth/sales-admins/mgoal")
    public Result mgoalUpdate(@RequestParam(value = "list[]") Long[] list, Integer mgoal) {
        return salesAdminService.mgoalUpdate(list, mgoal);
    }

    /**
     * 批量调整销售级别
     * @param list 主键集合
     * @param level 销售级别 0：初级  1：中级 2:高级
     * @return
     */
    @PatchMapping(value = "v1/auth/sales-admins/level")
    public Result updateBatchLevelChange(@RequestParam(value = "list[]") Long[] list, Integer level) {
        return salesAdminService.updateBatchLevelChange(list, level);
    }

    /**
     * 批量修改最大限制数
     * @param list 主键集
     * @param levelnum 最大限制数
     * @return
     */
    @PatchMapping(value = "v1/auth/sales-admins/levelnum")
    public Result updateBatchLevelnum(@RequestParam(value = "list[]") Long[] list, Integer levelnum) {
        return salesAdminService.updateBatchLevelnum(list,levelnum);
    }

}
