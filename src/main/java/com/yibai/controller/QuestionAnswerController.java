package com.yibai.controller;

import com.yibai.service.QuestionAnswerService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class QuestionAnswerController {
    @Autowired
    private QuestionAnswerService questionAnswerService;
    /**
     * 咨询列表》问题列表分页查询（完全匹配）
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map（sendUserName，sendMobile,receiverUserName，receiverMobile,shopName,approveStatus）
     * @return
     */
    //缺少获取当前登录的用户名
    //前台传一个params.flag判断是否通过当前登录用户查询
    @GetMapping(value = "v1/auth/qusetion-answer/problem-paginations")
    public Result pageListProblem(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return questionAnswerService.pageListProblem(pageNum,pageSize,params);
    }
    /**
     * 咨询列表》问题列表每列详细信息
     * @param id 参数id
     * @return
     */
    @GetMapping(value = "v1/auth/qusetion-answer/problem-details")
    public Result detailsProblem(Long id) {
        return questionAnswerService.detailProblem(id);
    }

    /**
     * 咨询列表》问题列表更新数据(审核)
     * @param params 参数map（id,content，checkPassed审核，shortMessage发短信，卖家电话receiverMobile）
     * @return
     */
    @PatchMapping(value = "v1/auth/qusetion-answer/problem1-contents")
    public Result updateProblemCheck(@RequestParam Map<String, Object> params) {
        return questionAnswerService.updateProblemCheck(params);
    }
    /**
     * 咨询列表》问题列表更新数据（回复）
     * @param params 参数map（id,endContent，shortMessage发短信，买家电话sendMobile）
     * @return
     */
    @PatchMapping(value = "v1/auth/qusetion-answer/problem2-contents")
    public Result updateProblem(@RequestParam Map<String, Object> params) {
        return questionAnswerService.updateProblem(params);
    }
    /**
     * 咨询列表>>回复列表分页查询（完全匹配）
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    //缺少获取当前登录的用户名
    //前台传一个params.flag判断是否通过当前登录用户查询
    @GetMapping(value = "v1/auth/qusetion-answer/response-paginations")
    public Result PageListReply(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return questionAnswerService.pageListReply(pageNum,pageSize,params);
    }
    /**
     * 咨询列表>>回复列表未审核详细信息
     * @param id 参数id
     * @return
     */
    @GetMapping(value = "v1/auth/qusetion-answer/response-details")
    public Result detailsReply(Long id) {
        return questionAnswerService.detailReply(id);
    }
    /**
     * 咨询列表》回复列表更新数据
     * @param params 参数map（id，endContent，shortMessage是否发送短信，checkPassed是否审核通过）
     * @return
     */
    @PatchMapping(value = "v1/auth/qusetion-answer/response-contents")
    public Result updateReply(@RequestParam Map<String, Object> params) {
        return questionAnswerService.updateReply(params);
    }
}
