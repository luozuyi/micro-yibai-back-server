package com.yibai.controller;

import com.yibai.service.YibaidianAdminService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class YibaidianAdminController {
    @Autowired
    private YibaidianAdminService yibaidianAdminService;

    /**
     * 转移数据
     * @return
     */
    @PostMapping(value = "v1/yibaidian-admins")
    public Result insertList() {
        return yibaidianAdminService.insertList();
    }

    /**
     * 添加管理员
     * @param adminName 管理员名称
     * @param password 管理员密码
     * @param roleId 管理员角色id
     * @param isDisable 是否禁用0：否，1：是
     * @param surePassword 确认密码
     * @return
     */
    @PostMapping(value = "v1/auth/yibaidian-admins")
    public Result addAdmin(String adminName, String password, String roleId, String isDisable, String surePassword) {
        return yibaidianAdminService.addAdmin(adminName, password, roleId, isDisable, surePassword);
    }

    /**
     * 分页查询管理员列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/yibaidian-admins/pagination")
    public Result pageList(Integer pageNum, Integer pageSize) {
        return yibaidianAdminService.pageList(pageNum, pageSize);
    }

    /**
     * 查询管理员详情以及角色
     * @param adminId
     * @return
     */
    @GetMapping(value = "v1/auth/yibaidian-admins/id")
    public Result findById(String adminId) {
        return yibaidianAdminService.selectByPrimaryKey(adminId);
    }

    /**
     * 修改管理员以及角色
     * @param adminId 管理员id
     * @param password 密码
     * @param roleId 要修改的角色id
     * @param isDisable 是否禁用
     * @param surePassword 确认密码
     * @return
     */
    @PatchMapping(value = "v1/auth/yibaidian-admins")
    public Result update(String adminId, String password, String roleId, String isDisable, String surePassword) {
        return yibaidianAdminService.update(adminId, password, roleId, isDisable, surePassword);
    }
}
