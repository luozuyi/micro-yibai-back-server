package com.yibai.controller;

import com.yibai.entity.ShopRemitRecord;
import com.yibai.service.ShopRemitRecordService;
import com.yibai.utils.Constants;
import com.yibai.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShopRemitRecordController {

    @Autowired
    private ShopRemitRecordService service;

    /**
     * 分页查询线下汇款记录列表
     * @return
     */
    @GetMapping(value = "v1/auth/shop-remit-records/pagination")
    public Result pageList(ShopRemitRecord shopRemitRecord,@CookieValue String yibaiAdminToken,Integer pageNum, Integer pageSize) {
        return service.selectAllRemitRecord(shopRemitRecord,yibaiAdminToken,pageNum,pageSize);
    }

    /**
     * 审核线下汇款时根据id查询对应的信息
     * @param id
     * @return
     */
    @GetMapping(value = "v1/auth/shop-remit-records/remit-record-info")
    public Result getRemitRecordForApprove(String id) {
        if (StringUtils.isEmpty(id)){
            return new Result(Constants.FAIL,"线下汇款id不能为空");
        }
        return service.selectByApprove(id);
    }

    /**
     * 审核线下汇款
     * @param id
     * @param approve
     * @return
     */
    @PatchMapping(value = "v1/auth/shop-remit-records/remit-record-approval")
    public Result approveRemitRecord(String id,String approve,@CookieValue String yibaiAdminToken) {
        if (StringUtils.isEmpty(id)){
            return new Result(Constants.FAIL,"线下汇款id不能为空");
        }
        if (StringUtils.isEmpty(approve)){
            return new Result(Constants.FAIL,"审核线下汇款结果不能为空");
        }
        return service.approveRemitRecord(id,approve,yibaiAdminToken);
    }
}
