package com.yibai.controller;

import com.yibai.service.ShopTagService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShopTagController {
    @Autowired
    private ShopTagService shopTagService;
    /**
     * 查所有商品类型
     * @return
     */
    @GetMapping(value = "v1/auth/shop-tag/tag-paginations")
    public Result listShopCategory(){
        return shopTagService.ListShopTag();
    }
}
