package com.yibai.controller;

import com.yibai.service.SalesPhonepoolService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class SalesPhonepoolController {
    @Autowired
    private SalesPhonepoolService salesPhonepoolService;

    /**
     * 条件分页查询所有电话池列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/sales-phonepools/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return salesPhonepoolService.pageListEntity(pageNum,pageSize,params);
    }

    /**
     * 查询当前用户是组长的电话池列表 及type为1
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @param yibaiAdminToken 凭据
     * @return
     */
    @GetMapping(value = "v1/auth/sales-phonepools/pagination/type1")
    public Result selectType1SalesPhonepool(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params, @CookieValue String yibaiAdminToken) {
        return salesPhonepoolService.selectType1SalesPhonepool(pageNum,pageSize,params,yibaiAdminToken);
    }

    /**
     * 查询当前用户是组长的电话池列表 及type为2
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @param yibaiAdminToken 凭据
     * @return
     */
    @GetMapping(value = "v1/auth/sales-phonepools/pagination/type2")
    public Result selectType2SalesPhonepool(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params, @CookieValue String yibaiAdminToken) {
        return salesPhonepoolService.selectType2SalesPhonepool(pageNum,pageSize,params,yibaiAdminToken);
    }

    /**
     * 生成电话池
     * @return
     */
    @PostMapping(value = "v1/auth/sales-phonepools")
    public Result creatSalesPhonePool() {
        return salesPhonepoolService.creatSalesPhonePool();
    }

    /**
     * 批选择分配电话池给组长
     * @param list 电话池id集合
     * @param leaderId 组长id
     * @return
     */
    @PatchMapping(value = "v1/auth/sales-phonepools/option-alloction")
    public Result optionAlloc(@RequestParam(value = "list[]") Long[] list, Long leaderId) {
        return salesPhonepoolService.optionAlloc(list,leaderId);
    }

    /**
     * 一键分配电话池
     * @param list
     * @return
     */
    @PatchMapping(value = "v1/auth/sales-phonepools/alloction")
    public Result allocPool(@RequestParam(value = "list[]") String[] list) {
        return salesPhonepoolService.allocPool(list);
    }
}
