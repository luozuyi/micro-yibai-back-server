package com.yibai.controller;

import com.yibai.service.SalesComplaintService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class SalesComplaintController {
    @Autowired
    private SalesComplaintService salesComplaintService;

    /**
     * 条件分页查询销售投诉
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/sales-complaint/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return salesComplaintService.pageList(pageNum,pageSize,params);
    }

    /**
     * 主键查询详情
     * @param id 主键
     * @return
     */
    @GetMapping(value = "v1/auth/sales-complaint")
    public Result detail(Long id) {
        return salesComplaintService.selectMapByPrimaryKey(id);
    }
}
