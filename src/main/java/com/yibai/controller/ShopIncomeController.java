package com.yibai.controller;

import com.yibai.entity.ShopIncome;
import com.yibai.entity.ShopRemitRecord;
import com.yibai.service.ShopIncomeService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class ShopIncomeController {

    @Autowired
    private ShopIncomeService service;

    /**
     * 分页查询收支明细记录列表
     * memberPhone  会员电话
     * financeType  收支类型
     * startNum 初始金额
     * endNum   终止金额
     * StartCTime  初始操作时间
     * endCTime    终止操作时间
     * @return
     */
    @GetMapping(value = "v1/auth/shop-incomes/pagination")
    public Result pageLists(ShopIncome shopIncome, Integer pageNum, Integer pageSize) {
        return service.selectAllShopIncome(shopIncome,pageNum,pageSize);
    }
}
