package com.yibai.controller;

import com.yibai.service.UserUnsuccessregService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class UserUnsuccessregController {
    @Autowired
    private UserUnsuccessregService userUnsuccessregService;

    /**
     * 分页查询注册电话池
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 注册电话池对象参数
     * @return
     */
    @GetMapping(value = "v1/auth/user-unsuccessregs/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params) {
        return userUnsuccessregService.pageListEntity(pageNum, pageSize, params);
    }

    /**
     * 主键查询详情
     * @param id 主键id
     * @return
     */
    @GetMapping(value = "v1/auth/user-unsuccessregs")
    public Result detail(Integer id) {
        return userUnsuccessregService.selectByPrimaryKey(id);
    }

    /**
     * 修改电话池
     * @param id 主键id
     * @param note 备注
     * @param yibaiAdminToken 凭据
     * @return
     */
    @PatchMapping(value = "v1/auth/user-unsuccessregs")
    public Result updateNote(Integer id, String note, @CookieValue String yibaiAdminToken) {
        return userUnsuccessregService.updateNote(id,note,yibaiAdminToken);
    }
}
