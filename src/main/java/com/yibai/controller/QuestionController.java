package com.yibai.controller;

import com.yibai.entity.Question;
import com.yibai.service.QuestionService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class QuestionController {
    @Autowired
    private QuestionService questionService;
    /**
     * 在线回答列表》提问管理分页查询（完全匹配）
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map（username，response,startdate）
     * @return
     */
    @GetMapping(value = "v1/auth/qusetion/qusetion-paginations")
    public Result pageListQuestion(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return questionService.pageListQuestion(pageNum,pageSize,params);
    }
    /**
     * 在线回答列表》提问管理每列详细信息
     * @param questionId 参数questionId
     * @return
     */
    @GetMapping(value = "v1/auth/qusetion/qusetion-details")
    public Result detailsQuestion(Long questionId) {
        return questionService.detailQuestion(questionId);
    }

    /**
     * 在线回答列表》提问管理更新数据
     * @param record 参数实体Qusetion
     * @return
     */
    @PatchMapping(value = "v1/auth/qusetion/qusetion-contents")
    public Result updateQuestion(Question record) {
        return questionService.updateQuestion(record);
    }

    /**
     * 在线回答列表》提问管理新增数据
     * @param record 参数实体Qusetion
     * @return
     */
    //缺当前登录用户信息
    @PostMapping(value = "v1/auth/qusetion/qusetions")
    public Result insertSelectiveQuestion(Question record) {
        return questionService.insertSelectiveQuestion(record);
    }

    /**
     * 在线回答列表》提问管理批量删除数据
     * @param array 参数数组
     * @return
     */
    @DeleteMapping(value = "v1/auth/qusetion/qusetions-ids")
    public Result deleteByIdsQuestion(@RequestParam("array[]") Long[] array) {
        return questionService.deleteByIdsQuestion(array);
    }

    /**
     * 在线回答列表》建议管理分页查询（完全匹配）
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map（username，response,startdate）
     * @return
     */
    @GetMapping(value = "v1/auth/qusetion/proposal-paginations")
    public Result pageListProposal(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return questionService.pageListProposal(pageNum,pageSize,params);
    }
    /**
     * 在线回答列表》建议管理每列详细信息
     * @param questionId 参数questionId
     * @return
     */
    @GetMapping(value = "v1/auth/qusetion/proposal-details")
    public Result detailsProposal(Long questionId) {
        return questionService.detailProposal(questionId);
    }
    /**
     * 在线回答列表》建议管理更新数据
     * @param record 参数实体Qusetion
     * @return
     */
    @PatchMapping(value = "v1/auth/qusetion/proposal-contents")
    public Result updateProposal(Question record) {
        return questionService.updateQuestion(record);
    }
    /**
     * 在线回答列表》建议管理删除数据
     * @param questionId 参数questionId
     * @return
     */
    @DeleteMapping(value = "v1/auth/qusetion/proposal-ids")
    public Result deleteByIdsProposal(Long questionId) {
        return questionService.deleteByIdProposal(questionId);
    }
}
