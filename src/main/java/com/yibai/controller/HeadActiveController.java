package com.yibai.controller;

import com.yibai.entity.HeadActive;
import com.yibai.service.HeadActiveService;
import com.yibai.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeadActiveController {
    @Autowired
    private HeadActiveService headActiveService;

    @GetMapping(value = "v1/auth/head-actives/pagination")
    public Result pageList(HeadActive headActive, Integer pageNum, Integer pageSize){
        return headActiveService.pageList(headActive,pageNum,pageSize);
    }
}
