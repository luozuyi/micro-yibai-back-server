package com.yibai.utils;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class JwtToken {
    /**
     * 公共秘钥保存在服务器
     */
    private static String SECRET = "m13164138097@163.com";


    public static Map<String,Claim> verifyToken(String token) throws UnsupportedEncodingException {
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(SECRET))
                .build();
        DecodedJWT jwt = null;
        try {
            jwt = verifier.verify(token);
        }catch (Exception e){
            throw new RuntimeException("登陆凭证已经过期，请重新登陆");
        }
        return jwt.getClaims();
    }

    public static String getUsername(String token) throws UnsupportedEncodingException{
        Map<String,Claim> claim = JwtToken.verifyToken(token);
        return claim.get("adminName") == null ? null : claim.get("adminName").asString();
    }
}
