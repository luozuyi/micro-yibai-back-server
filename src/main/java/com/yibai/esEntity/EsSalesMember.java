package com.yibai.esEntity;

import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.util.Date;

@Document(indexName="yibai_new_back",type="salesMember",indexStoreType="fs",shards=5,replicas=1,refreshInterval="-1")
public class EsSalesMember implements Serializable{
    private static final long serialVersionUID = 1L;
    private Long id;
    private String memberName;
    private String mobile;
    private String qq;
    private String memberAddress; //会员所在地区
    private String memberDemand; //会员需求
    private String belongSalman; //会员当前所属销售员
    private Integer loginCount;//会员登录总次数
    private Date createTime;//账号创建时间
    private Date currentLoginTime;//账号登录时间
    private Integer shopCount; //当前会员店铺的总数量--包括已出售
    private Integer status;	//会员状态--判断当前会员属于买家还是卖家
    private Integer sort; //会员等级分类 -1-未选择 0-A 1-B 2-C 3-D
    private Integer quickNote; //快速备注
    private Integer isOverTime;//是否即将过期
    private Integer needsales;
    //===== 新增属性 createTime 2015.06.03 BY zq=====
    private Integer tbCheckCount;//淘宝卡数量
    private Integer	checkCount;//商城卡数量
    private Integer voucher;//代金券数量
    private Double freezeMoney;//冻结资金
    private Double money;//账户余额
    private Integer leader;//组长ID
    private String reg_origin;
    private String reg_classify;
    private Date lastJoinPoolTime;//最近加入电话池的时间

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getMemberAddress() {
        return memberAddress;
    }

    public void setMemberAddress(String memberAddress) {
        this.memberAddress = memberAddress;
    }

    public String getMemberDemand() {
        return memberDemand;
    }

    public void setMemberDemand(String memberDemand) {
        this.memberDemand = memberDemand;
    }

    public String getBelongSalman() {
        return belongSalman;
    }

    public void setBelongSalman(String belongSalman) {
        this.belongSalman = belongSalman;
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getCurrentLoginTime() {
        return currentLoginTime;
    }

    public void setCurrentLoginTime(Date currentLoginTime) {
        this.currentLoginTime = currentLoginTime;
    }

    public Integer getShopCount() {
        return shopCount;
    }

    public void setShopCount(Integer shopCount) {
        this.shopCount = shopCount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getQuickNote() {
        return quickNote;
    }

    public void setQuickNote(Integer quickNote) {
        this.quickNote = quickNote;
    }

    public Integer getIsOverTime() {
        return isOverTime;
    }

    public void setIsOverTime(Integer isOverTime) {
        this.isOverTime = isOverTime;
    }

    public Integer getNeedsales() {
        return needsales;
    }

    public void setNeedsales(Integer needsales) {
        this.needsales = needsales;
    }

    public Integer getTbCheckCount() {
        return tbCheckCount;
    }

    public void setTbCheckCount(Integer tbCheckCount) {
        this.tbCheckCount = tbCheckCount;
    }

    public Integer getCheckCount() {
        return checkCount;
    }

    public void setCheckCount(Integer checkCount) {
        this.checkCount = checkCount;
    }

    public Integer getVoucher() {
        return voucher;
    }

    public void setVoucher(Integer voucher) {
        this.voucher = voucher;
    }

    public Double getFreezeMoney() {
        return freezeMoney;
    }

    public void setFreezeMoney(Double freezeMoney) {
        this.freezeMoney = freezeMoney;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Integer getLeader() {
        return leader;
    }

    public void setLeader(Integer leader) {
        this.leader = leader;
    }

    public String getReg_origin() {
        return reg_origin;
    }

    public void setReg_origin(String reg_origin) {
        this.reg_origin = reg_origin;
    }

    public String getReg_classify() {
        return reg_classify;
    }

    public void setReg_classify(String reg_classify) {
        this.reg_classify = reg_classify;
    }

    public Date getLastJoinPoolTime() {
        return lastJoinPoolTime;
    }

    public void setLastJoinPoolTime(Date lastJoinPoolTime) {
        this.lastJoinPoolTime = lastJoinPoolTime;
    }
}
