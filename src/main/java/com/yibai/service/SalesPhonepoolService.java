package com.yibai.service;

import com.yibai.entity.SalesPhonepool;
import com.yibai.utils.Result;

import java.util.Map;

public interface SalesPhonepoolService extends BaseService<SalesPhonepool,Long>{
    /**
     * 当type为1的时候的查询
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @param yibaiAdminToken 凭据
     * @return
     */
    Result selectType1SalesPhonepool(Integer pageNum, Integer pageSize, Map<String,Object> params,String yibaiAdminToken);

    /**
     * 当type为2的时候的查询
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @param yibaiAdminToken 凭据
     * @return
     */
    Result selectType2SalesPhonepool(Integer pageNum, Integer pageSize, Map<String,Object> params,String yibaiAdminToken);

    /**
     * 创建电话池
     * @return
     */
    Result creatSalesPhonePool();

    /**
     * 分配组长电话吃
     * @param list 电话池id
     * @param leaderId 组长id
     * @return
     */
    Result optionAlloc(Long list[], Long leaderId);

    /**
     * 一键分配电话池
     * @param list
     * @return
     */
    Result allocPool(String list[]);
}
