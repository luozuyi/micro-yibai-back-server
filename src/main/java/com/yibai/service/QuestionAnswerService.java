package com.yibai.service;

import com.yibai.utils.Result;

import java.util.Map;

public interface QuestionAnswerService {

    Result pageListProblem(Integer pageNum, Integer pageSize, Map<String,Object> params);

    public Result detailProblem(Long id);

    public Result updateProblemCheck(Map<String,Object> params);
	
    public Result updateProblem(Map<String,Object> params);

    Result pageListReply(Integer pageNum, Integer pageSize, Map<String,Object> params);

    public Result detailReply(Long id);

    public Result updateReply(Map<String,Object> params);
}
