package com.yibai.service;

import com.yibai.entity.ShopVoucher;
import com.yibai.utils.Result;

public interface ShopVoucherService extends BaseService<ShopVoucher,Long> {

    Result selectAllShopVoucher(ShopVoucher shopVoucher, Integer pageNum, Integer pageSize);
}
