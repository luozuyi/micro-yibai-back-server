package com.yibai.service;

import com.yibai.entity.ShopWithdraw;
import com.yibai.utils.Result;

public interface ShopWithdrawService extends BaseService<ShopWithdraw,Long> {

    Result selectAllShopWithdraw(ShopWithdraw shopWithdraw,Integer pageNum, Integer pageSize);

    Result selectShopWithdrawById(String id);

    Result updateStatus(String id);

    Result refuseRequest(String id, String reason, String yibaiAdminToken);

    Result updateFreeOfFee(String id);

    Result approveWithdraw(String id,String reason,String yibaiAdminToken);
}
