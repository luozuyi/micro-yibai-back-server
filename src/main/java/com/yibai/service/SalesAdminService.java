package com.yibai.service;

import com.yibai.entity.SalesAdmin;
import com.yibai.utils.Result;

public interface SalesAdminService extends BaseService<SalesAdmin,Long>{
    /**
     * 查询组长列表
     * @return
     */
    Result selectLeaderList();

    /**
     * 查询部长列表
     * @return
     */
    Result selectMinisterList();

    /**
     * 批量分配组员给组长
     * @param list 数组集合
     * @param leaderId 组长id
     * @return
     */
    Result addBatch(Long list[], Integer leaderId);

    /**
     * 分配组员给部长
     * @param list 数组集合
     * @param minister 部长id
     * @return
     */
    Result addDepartMember(Long list[], Integer minister);

    /**
     * 调整销售管理客户数量
     * @param id 主键id
     * @param num 数量
     * @param level 等级
     * @param limitnum 最大限制数
     * @return
     */
    Result numUpdate(Long id,Integer num, Integer level, Integer limitnum);

    /**
     * 批量调整个人月目标
     * @param list 数组集合
     * @param mgoal 个人月目标
     * @return
     */
    Result mgoalUpdate(Long list[],Integer mgoal);

    /**
     * 批量调整销售级别
     * @param list 数组集合
     * @param levelChange 等及
     * @return
     */
    Result updateBatchLevelChange(Long list[],Integer levelChange);

    /**
     * 批量更新销售员最大数
     * @param list 数组集合
     * @param levelnum 最大限制数
     * @return
     */
    Result updateBatchLevelnum(Long list[],Integer levelnum);
}
