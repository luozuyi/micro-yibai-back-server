package com.yibai.service;

import com.yibai.entity.ShopAlipay;
import com.yibai.utils.Result;

public interface ShopAlipayService extends BaseService<ShopAlipay,Long> {


    Result selectAllShopAlipay(ShopAlipay record,String yibaiAdminToken, Integer pageNum, Integer pageSize);

    Result approve(String id,String approve,String yibaiAdminToken);

    Result selectShopAlipayById(String id);
}
