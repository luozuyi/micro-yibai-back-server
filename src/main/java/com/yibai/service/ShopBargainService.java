package com.yibai.service;

import com.yibai.entity.ShopBargain;
import com.yibai.utils.Result;

public interface ShopBargainService extends BaseService<ShopBargain,Integer>{
    /**
     * 修改备注
     * @param id 主键id
     * @param remarks 备注信息
     * @return
     */
    Result updateNote(Integer id, String remarks);

    /**
     * 销售员抢信息
     * @param id 主键id
     * @param yibaiAdminToken 流书token
     * @return
     */
    Result impackMyMember(Integer id, String yibaiAdminToken);
}
