package com.yibai.service;

import com.yibai.entity.OrderCouponStatistics;
import com.yibai.utils.Result;

import java.util.Map;

public interface OrderCouponStatisticsService extends BaseService<OrderCouponStatistics,Long>{
    /**
     * 分页查询管理员列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result listpage(Integer pageNum, Integer pageSize,  Map<String, Object> params);
}
