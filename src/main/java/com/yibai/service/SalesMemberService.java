package com.yibai.service;

import com.yibai.entity.SalesMember;
import com.yibai.utils.Result;

import java.util.Map;

public interface SalesMemberService extends BaseService<SalesMember,Long>{
    /**
     * 分页条件查询我的客户
     * @param pageNum 当前页
     * @param pageSize 一页显示多条
     * @param params 参数map
     * @param yibaiAdminToken 凭据
     * @return
     */
    Result selectByBelongSalesmanSelection(Integer pageNum,Integer pageSize,Map<String,Object> params,String yibaiAdminToken);
}
