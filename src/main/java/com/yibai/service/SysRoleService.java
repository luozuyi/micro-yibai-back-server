package com.yibai.service;

import com.yibai.entity.SysRole;
import com.yibai.utils.Result;

public interface SysRoleService extends BaseService<SysRole,String>{
    /**
     *
     * @param roleName 角色名称
     * @param sequence 排序
     * @param sysResIds 资源ids
     * @return
     */
    Result add(String roleName, String sequence, String[] sysResIds);

    /**
     * 主键查询角色 以及拥有的权限
     * @param roleId 角色id
     * @return
     */
    Result findById(String roleId);

    /**
     * 修改角色名称，排序，以及权限
     * @param roleId
     * @param roleName
     * @param sequence
     * @param sysResIds
     * @return
     */
    Result update(String roleId, String roleName, String sequence, String[] sysResIds);

    /**
     * 分页查询角色列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize);

    /**
     * 查询列表
     * @return
     */
    Result list();
}
