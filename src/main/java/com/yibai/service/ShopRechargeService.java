package com.yibai.service;

import com.yibai.entity.ShopRecharge;
import com.yibai.utils.Result;

public interface ShopRechargeService extends BaseService<ShopRecharge,Long>{

    Result selectAllRecharge(ShopRecharge shopRecharge,Integer pageNum,Integer pageSize);
}
