package com.yibai.service;

import com.yibai.entity.ShopInterview;
import com.yibai.utils.Result;

public interface ShopInterviewService {

    Result pageList(Integer pageNum, Integer pageSize, ShopInterview record);

    public Result detail(Long id);

    public Result deleteById(Long id);

    public Result update(ShopInterview record);

    public Result insertSelective(ShopInterview record);
}
