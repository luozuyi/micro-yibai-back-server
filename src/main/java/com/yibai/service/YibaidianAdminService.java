package com.yibai.service;

import com.yibai.entity.YibaidianAdmin;
import com.yibai.utils.Result;

public interface YibaidianAdminService extends BaseService<YibaidianAdmin,String>{
    /**
     * 自定义插入list
     * @return
     */
    Result insertList();

    /**
     * 添加管理员
     * @param adminName 管理员名称
     * @param password 管理员密码
     * @param roleId 角色id
     * @return
     */
    Result addAdmin(String adminName,String password,String roleId,String isDisable,String surePassword);

    /**
     * 分页查询管理员列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageList(Integer pageNum,Integer pageSize);


    /**
     * 修改管理员信息
     * @param adminId 管理员id
     * @param password 管理员密码
     * @param roleId 角色id
     * @param isDisable 是否禁用
     * @param surePassword 确认密码
     * @return
     */
    Result update(String adminId,String password,String roleId,String isDisable,String surePassword);
}
