package com.yibai.service;

import com.yibai.entity.UserUnsuccessreg;
import com.yibai.utils.Result;

public interface UserUnsuccessregService extends BaseService<UserUnsuccessreg,Integer>{
    /**
     * 修改备注
     * @param id 主键id
     * @param note 备注
     * @return
     */
    Result updateNote(Integer id, String note,String yibaiAdminToken);
}
