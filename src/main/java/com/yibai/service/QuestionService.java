package com.yibai.service;

import com.yibai.entity.Question;
import com.yibai.utils.Result;

import java.util.Map;

public interface QuestionService {

    Result pageListQuestion(Integer pageNum, Integer pageSize, Map<String,Object> params);

    public Result detailQuestion(Long questionId);

    public Result updateQuestion(Question record);

    public Result insertSelectiveQuestion(Question record);

    public Result deleteByIdsQuestion(Long[] array);

    Result pageListProposal(Integer pageNum, Integer pageSize, Map<String,Object> params);

    public Result detailProposal(Long questionId);

    public Result deleteByIdProposal(Long questionId);
}
