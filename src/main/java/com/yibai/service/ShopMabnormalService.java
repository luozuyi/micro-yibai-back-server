package com.yibai.service;

import com.yibai.entity.ShopMabnormal;
import com.yibai.utils.Result;

public interface ShopMabnormalService extends BaseService<ShopMabnormal,Long> {

    Result selectAllShopMabnormal(ShopMabnormal shopMabnormal, Integer pageNum, Integer pageSize);
}
