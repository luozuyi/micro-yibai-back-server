package com.yibai.service;

import com.yibai.entity.Artificial;
import com.yibai.utils.Result;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public interface ArtificialService extends BaseService<Artificial,Long>{
    /**
     * 主键修改一些信息
     * @param id
     * @param remarks
     * @param isDeal
     * @param dealShopUrl
     * @return
     */
    Result updateNote(Long id, String remarks, String isDeal, String dealShopUrl);

    /**
     * 销售员抢单
     * @param id
     * @return
     */
    Result impackMyMember(Long id,String yibaiAdminToken);
}
