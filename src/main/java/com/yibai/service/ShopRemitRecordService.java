package com.yibai.service;

import com.yibai.entity.ShopRemitRecord;
import com.yibai.utils.Result;


public interface ShopRemitRecordService extends BaseService<ShopRemitRecord,Long>{

    /**
     * 多条件分页查询线下汇款记录
     * @param shopRemitRecord
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result selectAllRemitRecord(ShopRemitRecord shopRemitRecord,String yibaiAdminToken,Integer pageNum, Integer pageSize);

    /**
     * 审核线下汇款时根据id查询对应信息
     * @param id
     * @return
     */
    Result selectByApprove(String id);

    /**
     * 审核线下汇款
     * @param id
     * @param approve
     * @param yibaiAdminToken
     * @return
     */
    Result approveRemitRecord(String id,String approve,String yibaiAdminToken);
}
