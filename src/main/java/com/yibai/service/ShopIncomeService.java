package com.yibai.service;

import com.yibai.entity.ShopIncome;
import com.yibai.utils.Result;


public interface ShopIncomeService extends BaseService<ShopIncome,Long> {

    Result selectAllShopIncome(ShopIncome shopIncome,Integer pageNum, Integer pageSize);
}
