package com.yibai.service;

import com.yibai.entity.ActivityCoupon;
import com.yibai.utils.Result;

import java.util.Date;
import java.util.Map;

public interface ActivityCouponService  extends BaseService<ActivityCoupon,Long>{

    /**
     * 添加优惠卷活动
     * @param activityCoupon 优惠卷对象
     * @return
     */
    Result addActivityCoupon(ActivityCoupon activityCoupon);

    /**
     * 关闭活动
     * @param
     * @return
     * */
    Result change(Long id,String status);

    /**
     * 分页查询管理员列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result listScreen(Integer pageNum,Integer pageSize,Map<String, Object> params);
}
