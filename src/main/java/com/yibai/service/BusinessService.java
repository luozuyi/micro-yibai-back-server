package com.yibai.service;

import com.yibai.entity.Business;
import com.yibai.utils.Result;

public interface BusinessService extends BaseService<Business,Integer> {
    /**
     * 商务洽谈列表
     * @param business
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result pageList(Business business, Integer pageNum, Integer pageSize);

    /**
     * 通过id查看商务信息
     * @param id
     * @return
     */
    Result findBusinessById(Long id);
}
