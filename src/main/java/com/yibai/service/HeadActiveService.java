package com.yibai.service;

import com.yibai.entity.HeadActive;
import com.yibai.utils.Result;

public interface HeadActiveService extends BaseService<HeadActive,Integer> {
    /**
     * isoy应用打开列表
     * @param headActive
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result pageList(HeadActive headActive, Integer pageNum, Integer pageSize);
}
