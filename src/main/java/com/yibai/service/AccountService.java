package com.yibai.service;

import com.yibai.entity.Account;
import com.yibai.utils.Result;

public interface AccountService extends BaseService<Account,Integer>{
    /**
     * 账号管理分页查询
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result pageList(Account account,Integer pageNum, Integer pageSize);


    /**
     * 添加账号信息
     * @param account
     * @return
     */
     Result addAccount(Account account);

    /**
     * 修改账号信息
     * @param account
     * @return
     */
     Result updateAccount(Integer id,Account account);


}
