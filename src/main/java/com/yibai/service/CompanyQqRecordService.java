package com.yibai.service;

import com.yibai.entity.CompanyQqRecord;
import com.yibai.utils.Result;

public interface CompanyQqRecordService extends BaseService<CompanyQqRecord,Integer> {
    /**
     * 客服使用记录分页列表
     * @param companyQqRecord
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result pageList(CompanyQqRecord companyQqRecord, Integer pageNum, Integer pageSize);
}
