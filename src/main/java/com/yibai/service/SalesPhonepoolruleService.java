package com.yibai.service;

import com.yibai.entity.SalesPhonepoolrule;
import com.yibai.utils.Result;

import java.util.Date;

public interface SalesPhonepoolruleService extends BaseService<SalesPhonepoolrule,Long>{
    /**
     * 修改电话池规则
     * @param id
     * @param type
     * @param num
     * @param dateNum
     * @param time
     * @return
     */
    Result updateById(Long id, Integer type, Integer num, Integer dateNum, Date time);
}
